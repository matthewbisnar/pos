﻿Namespace Api
    Public Class logs
        Private dbConfig As New Api.Configs

        Public Sub loginlog(ByVal parameters As Object)
            Dim uniqueid As String = System.Guid.NewGuid().ToString("N")
            Dim DateData As String = String.Format("{0:dd/MM/yyyy - hh:mm:ss tt}", DateTime.Now)
            Dim DateCreateAt As String = String.Format("{0:dd/MM/yyyy}", DateTime.Now)

            With dbConfig
                .Process_qry("INSERT INTO systemlogs (dateandtime, employeeId,employeeposition, type, description, logid, createdat) VALUES (?,?,?,?,?,?,?)",
                             DateData,
                             parameters.item("LoginId"),
                             parameters.item("position"),
                             parameters.item("type"),
                             parameters.item("description"),
                             uniqueid,
                             DateCreateAt
                             )
            End With
        End Sub

        Public Sub transactionLogs(ByVal paramaters As Object)

        End Sub

    End Class
End Namespace