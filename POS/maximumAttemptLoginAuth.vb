﻿Public Class maximumAttemptLoginAuth
    Private dbConfig As New Api.Configs

    Private Sub maximumAttemptLoginAuth_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        AuthenticateLoginInput.Text = String.Empty
    End Sub

    Private Sub AuthenticateLoginBtn_Click(sender As Object, e As EventArgs) Handles AuthenticateLoginBtn.Click
        If AuthenticateLoginInput.Text <> String.Empty Then
            Dim authprocess = dbConfig.Process_qry("SELECT * FROM manager WHERE ManagerId = ?", AuthenticateLoginInput.Text)

            If authprocess.tables(0).Rows.Count > 0 Then
                If MessageBox.Show("Restarting Application press ok to continue.", "Authenticate", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    Application.Restart()
                    Login.Refresh()
                End If
            Else
                MessageBox.Show("Manager ID is incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                AuthenticateLoginInput.Text = String.Empty
            End If
        Else
            MessageBox.Show("Input Must Not be Empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class