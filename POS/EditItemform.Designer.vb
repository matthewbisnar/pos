﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditItemForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(EditItemForm))
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.ClearQntity = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.minusButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.addButton = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddSubtractQuantity = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.itemQuantity = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Cancel = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ClearInputField = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.EditItemToList = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.descriptionInput = New WindowsFormsControlLibrary1.BunifuCustomTextbox()
        Me.DiscountIInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.itemPriceInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.CategoryComboBox = New System.Windows.Forms.ComboBox()
        Me.ProductBrandInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.ProductNameInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.productid = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.TypecomboBox = New System.Windows.Forms.ComboBox()
        Me.BunifuCards1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.TypecomboBox)
        Me.BunifuCards1.Controls.Add(Me.ClearQntity)
        Me.BunifuCards1.Controls.Add(Me.minusButton)
        Me.BunifuCards1.Controls.Add(Me.addButton)
        Me.BunifuCards1.Controls.Add(Me.AddSubtractQuantity)
        Me.BunifuCards1.Controls.Add(Me.itemQuantity)
        Me.BunifuCards1.Controls.Add(Me.Cancel)
        Me.BunifuCards1.Controls.Add(Me.ClearInputField)
        Me.BunifuCards1.Controls.Add(Me.EditItemToList)
        Me.BunifuCards1.Controls.Add(Me.descriptionInput)
        Me.BunifuCards1.Controls.Add(Me.DiscountIInput)
        Me.BunifuCards1.Controls.Add(Me.itemPriceInput)
        Me.BunifuCards1.Controls.Add(Me.CategoryComboBox)
        Me.BunifuCards1.Controls.Add(Me.ProductBrandInput)
        Me.BunifuCards1.Controls.Add(Me.ProductNameInput)
        Me.BunifuCards1.Controls.Add(Me.productid)
        Me.BunifuCards1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(0, 0)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(586, 590)
        Me.BunifuCards1.TabIndex = 1
        '
        'ClearQntity
        '
        Me.ClearQntity.Active = False
        Me.ClearQntity.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearQntity.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearQntity.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClearQntity.BorderRadius = 0
        Me.ClearQntity.ButtonText = "Clear"
        Me.ClearQntity.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearQntity.DisabledColor = System.Drawing.Color.Gray
        Me.ClearQntity.Iconcolor = System.Drawing.Color.Transparent
        Me.ClearQntity.Iconimage = Nothing
        Me.ClearQntity.Iconimage_right = Nothing
        Me.ClearQntity.Iconimage_right_Selected = Nothing
        Me.ClearQntity.Iconimage_Selected = Nothing
        Me.ClearQntity.IconMarginLeft = 0
        Me.ClearQntity.IconMarginRight = 0
        Me.ClearQntity.IconRightVisible = True
        Me.ClearQntity.IconRightZoom = 0R
        Me.ClearQntity.IconVisible = True
        Me.ClearQntity.IconZoom = 90.0R
        Me.ClearQntity.IsTab = False
        Me.ClearQntity.Location = New System.Drawing.Point(12, 267)
        Me.ClearQntity.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ClearQntity.Name = "ClearQntity"
        Me.ClearQntity.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearQntity.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ClearQntity.OnHoverTextColor = System.Drawing.Color.White
        Me.ClearQntity.selected = False
        Me.ClearQntity.Size = New System.Drawing.Size(103, 44)
        Me.ClearQntity.TabIndex = 16
        Me.ClearQntity.Text = "Clear"
        Me.ClearQntity.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearQntity.Textcolor = System.Drawing.Color.White
        Me.ClearQntity.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'minusButton
        '
        Me.minusButton.Active = False
        Me.minusButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.minusButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.minusButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.minusButton.BorderRadius = 0
        Me.minusButton.ButtonText = "-"
        Me.minusButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.minusButton.DisabledColor = System.Drawing.Color.Gray
        Me.minusButton.Iconcolor = System.Drawing.Color.Transparent
        Me.minusButton.Iconimage = Nothing
        Me.minusButton.Iconimage_right = Nothing
        Me.minusButton.Iconimage_right_Selected = Nothing
        Me.minusButton.Iconimage_Selected = Nothing
        Me.minusButton.IconMarginLeft = 0
        Me.minusButton.IconMarginRight = 0
        Me.minusButton.IconRightVisible = True
        Me.minusButton.IconRightZoom = 0R
        Me.minusButton.IconVisible = True
        Me.minusButton.IconZoom = 90.0R
        Me.minusButton.IsTab = False
        Me.minusButton.Location = New System.Drawing.Point(119, 267)
        Me.minusButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.minusButton.Name = "minusButton"
        Me.minusButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.minusButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.minusButton.OnHoverTextColor = System.Drawing.Color.White
        Me.minusButton.selected = False
        Me.minusButton.Size = New System.Drawing.Size(57, 44)
        Me.minusButton.TabIndex = 15
        Me.minusButton.Text = "-"
        Me.minusButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.minusButton.Textcolor = System.Drawing.Color.White
        Me.minusButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'addButton
        '
        Me.addButton.Active = False
        Me.addButton.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.addButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.addButton.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.addButton.BorderRadius = 0
        Me.addButton.ButtonText = "+"
        Me.addButton.Cursor = System.Windows.Forms.Cursors.Hand
        Me.addButton.DisabledColor = System.Drawing.Color.Gray
        Me.addButton.Iconcolor = System.Drawing.Color.Transparent
        Me.addButton.Iconimage = Nothing
        Me.addButton.Iconimage_right = Nothing
        Me.addButton.Iconimage_right_Selected = Nothing
        Me.addButton.Iconimage_Selected = Nothing
        Me.addButton.IconMarginLeft = 0
        Me.addButton.IconMarginRight = 0
        Me.addButton.IconRightVisible = True
        Me.addButton.IconRightZoom = 0R
        Me.addButton.IconVisible = True
        Me.addButton.IconZoom = 90.0R
        Me.addButton.IsTab = False
        Me.addButton.Location = New System.Drawing.Point(511, 267)
        Me.addButton.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.addButton.Name = "addButton"
        Me.addButton.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.addButton.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.addButton.OnHoverTextColor = System.Drawing.Color.White
        Me.addButton.selected = False
        Me.addButton.Size = New System.Drawing.Size(58, 44)
        Me.addButton.TabIndex = 14
        Me.addButton.Text = "+"
        Me.addButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.addButton.Textcolor = System.Drawing.Color.White
        Me.addButton.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddSubtractQuantity
        '
        Me.AddSubtractQuantity.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.AddSubtractQuantity.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.AddSubtractQuantity.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.AddSubtractQuantity.BorderThickness = 1
        Me.AddSubtractQuantity.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.AddSubtractQuantity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.AddSubtractQuantity.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.AddSubtractQuantity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.AddSubtractQuantity.isPassword = False
        Me.AddSubtractQuantity.Location = New System.Drawing.Point(179, 266)
        Me.AddSubtractQuantity.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.AddSubtractQuantity.MaxLength = 100
        Me.AddSubtractQuantity.Name = "AddSubtractQuantity"
        Me.AddSubtractQuantity.Size = New System.Drawing.Size(327, 44)
        Me.AddSubtractQuantity.TabIndex = 13
        Me.AddSubtractQuantity.Text = "Quantity"
        Me.AddSubtractQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'itemQuantity
        '
        Me.itemQuantity.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.itemQuantity.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemQuantity.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.itemQuantity.BorderThickness = 1
        Me.itemQuantity.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.itemQuantity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.itemQuantity.Enabled = False
        Me.itemQuantity.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.itemQuantity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemQuantity.isPassword = False
        Me.itemQuantity.Location = New System.Drawing.Point(10, 214)
        Me.itemQuantity.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.itemQuantity.MaxLength = 100
        Me.itemQuantity.Name = "itemQuantity"
        Me.itemQuantity.Size = New System.Drawing.Size(163, 44)
        Me.itemQuantity.TabIndex = 12
        Me.itemQuantity.Text = "Quantity"
        Me.itemQuantity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Cancel
        '
        Me.Cancel.Active = False
        Me.Cancel.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cancel.BorderRadius = 0
        Me.Cancel.ButtonText = "Cancel"
        Me.Cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Cancel.DisabledColor = System.Drawing.Color.Gray
        Me.Cancel.Iconcolor = System.Drawing.Color.Transparent
        Me.Cancel.Iconimage = CType(resources.GetObject("Cancel.Iconimage"), System.Drawing.Image)
        Me.Cancel.Iconimage_right = Nothing
        Me.Cancel.Iconimage_right_Selected = Nothing
        Me.Cancel.Iconimage_Selected = Nothing
        Me.Cancel.IconMarginLeft = 0
        Me.Cancel.IconMarginRight = 0
        Me.Cancel.IconRightVisible = True
        Me.Cancel.IconRightZoom = 0R
        Me.Cancel.IconVisible = True
        Me.Cancel.IconZoom = 90.0R
        Me.Cancel.IsTab = False
        Me.Cancel.Location = New System.Drawing.Point(170, 528)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Cancel.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.Cancel.OnHoverTextColor = System.Drawing.Color.White
        Me.Cancel.selected = False
        Me.Cancel.Size = New System.Drawing.Size(123, 44)
        Me.Cancel.TabIndex = 11
        Me.Cancel.Text = "Cancel"
        Me.Cancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Cancel.Textcolor = System.Drawing.Color.White
        Me.Cancel.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ClearInputField
        '
        Me.ClearInputField.Active = False
        Me.ClearInputField.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearInputField.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearInputField.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClearInputField.BorderRadius = 0
        Me.ClearInputField.ButtonText = "Reset"
        Me.ClearInputField.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearInputField.DisabledColor = System.Drawing.Color.Gray
        Me.ClearInputField.Iconcolor = System.Drawing.Color.Transparent
        Me.ClearInputField.Iconimage = CType(resources.GetObject("ClearInputField.Iconimage"), System.Drawing.Image)
        Me.ClearInputField.Iconimage_right = Nothing
        Me.ClearInputField.Iconimage_right_Selected = Nothing
        Me.ClearInputField.Iconimage_Selected = Nothing
        Me.ClearInputField.IconMarginLeft = 0
        Me.ClearInputField.IconMarginRight = 0
        Me.ClearInputField.IconRightVisible = True
        Me.ClearInputField.IconRightZoom = 0R
        Me.ClearInputField.IconVisible = True
        Me.ClearInputField.IconZoom = 90.0R
        Me.ClearInputField.IsTab = False
        Me.ClearInputField.Location = New System.Drawing.Point(300, 528)
        Me.ClearInputField.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ClearInputField.Name = "ClearInputField"
        Me.ClearInputField.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearInputField.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ClearInputField.OnHoverTextColor = System.Drawing.Color.White
        Me.ClearInputField.selected = False
        Me.ClearInputField.Size = New System.Drawing.Size(116, 44)
        Me.ClearInputField.TabIndex = 10
        Me.ClearInputField.Text = "Reset"
        Me.ClearInputField.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearInputField.Textcolor = System.Drawing.Color.White
        Me.ClearInputField.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'EditItemToList
        '
        Me.EditItemToList.Active = False
        Me.EditItemToList.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditItemToList.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditItemToList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.EditItemToList.BorderRadius = 0
        Me.EditItemToList.ButtonText = "Update item"
        Me.EditItemToList.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditItemToList.DisabledColor = System.Drawing.Color.Gray
        Me.EditItemToList.Iconcolor = System.Drawing.Color.Transparent
        Me.EditItemToList.Iconimage = CType(resources.GetObject("EditItemToList.Iconimage"), System.Drawing.Image)
        Me.EditItemToList.Iconimage_right = Nothing
        Me.EditItemToList.Iconimage_right_Selected = Nothing
        Me.EditItemToList.Iconimage_Selected = Nothing
        Me.EditItemToList.IconMarginLeft = 0
        Me.EditItemToList.IconMarginRight = 0
        Me.EditItemToList.IconRightVisible = True
        Me.EditItemToList.IconRightZoom = 0R
        Me.EditItemToList.IconVisible = True
        Me.EditItemToList.IconZoom = 90.0R
        Me.EditItemToList.IsTab = False
        Me.EditItemToList.Location = New System.Drawing.Point(422, 528)
        Me.EditItemToList.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EditItemToList.Name = "EditItemToList"
        Me.EditItemToList.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditItemToList.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.EditItemToList.OnHoverTextColor = System.Drawing.Color.White
        Me.EditItemToList.selected = False
        Me.EditItemToList.Size = New System.Drawing.Size(148, 44)
        Me.EditItemToList.TabIndex = 9
        Me.EditItemToList.Text = "Update item"
        Me.EditItemToList.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.EditItemToList.Textcolor = System.Drawing.Color.White
        Me.EditItemToList.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'descriptionInput
        '
        Me.descriptionInput.BorderColor = System.Drawing.Color.SeaGreen
        Me.descriptionInput.Location = New System.Drawing.Point(12, 320)
        Me.descriptionInput.Multiline = True
        Me.descriptionInput.Name = "descriptionInput"
        Me.descriptionInput.Size = New System.Drawing.Size(558, 197)
        Me.descriptionInput.TabIndex = 8
        Me.descriptionInput.Text = "Specifications"
        '
        'DiscountIInput
        '
        Me.DiscountIInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.DiscountIInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DiscountIInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.DiscountIInput.BorderThickness = 1
        Me.DiscountIInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.DiscountIInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.DiscountIInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.DiscountIInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DiscountIInput.isPassword = False
        Me.DiscountIInput.Location = New System.Drawing.Point(407, 214)
        Me.DiscountIInput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.DiscountIInput.MaxLength = 100
        Me.DiscountIInput.Name = "DiscountIInput"
        Me.DiscountIInput.Size = New System.Drawing.Size(163, 44)
        Me.DiscountIInput.TabIndex = 7
        Me.DiscountIInput.Text = "Discount"
        Me.DiscountIInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'itemPriceInput
        '
        Me.itemPriceInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.itemPriceInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemPriceInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.itemPriceInput.BorderThickness = 1
        Me.itemPriceInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.itemPriceInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.itemPriceInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.itemPriceInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemPriceInput.isPassword = False
        Me.itemPriceInput.Location = New System.Drawing.Point(176, 214)
        Me.itemPriceInput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.itemPriceInput.MaxLength = 32767
        Me.itemPriceInput.Name = "itemPriceInput"
        Me.itemPriceInput.Size = New System.Drawing.Size(227, 44)
        Me.itemPriceInput.TabIndex = 6
        Me.itemPriceInput.Text = "Price"
        Me.itemPriceInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'CategoryComboBox
        '
        Me.CategoryComboBox.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.CategoryComboBox.FormattingEnabled = True
        Me.CategoryComboBox.Items.AddRange(New Object() {"Other Category"})
        Me.CategoryComboBox.Location = New System.Drawing.Point(12, 169)
        Me.CategoryComboBox.Name = "CategoryComboBox"
        Me.CategoryComboBox.Size = New System.Drawing.Size(280, 25)
        Me.CategoryComboBox.TabIndex = 5
        Me.CategoryComboBox.Text = "No Category"
        '
        'ProductBrandInput
        '
        Me.ProductBrandInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.ProductBrandInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductBrandInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.ProductBrandInput.BorderThickness = 1
        Me.ProductBrandInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.ProductBrandInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ProductBrandInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.ProductBrandInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductBrandInput.isPassword = False
        Me.ProductBrandInput.Location = New System.Drawing.Point(300, 102)
        Me.ProductBrandInput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ProductBrandInput.MaxLength = 32767
        Me.ProductBrandInput.Name = "ProductBrandInput"
        Me.ProductBrandInput.Size = New System.Drawing.Size(271, 44)
        Me.ProductBrandInput.TabIndex = 4
        Me.ProductBrandInput.Text = "Product Brand"
        Me.ProductBrandInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'ProductNameInput
        '
        Me.ProductNameInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.ProductNameInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductNameInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.ProductNameInput.BorderThickness = 1
        Me.ProductNameInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.ProductNameInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ProductNameInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.ProductNameInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductNameInput.isPassword = False
        Me.ProductNameInput.Location = New System.Drawing.Point(10, 102)
        Me.ProductNameInput.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.ProductNameInput.MaxLength = 32767
        Me.ProductNameInput.Name = "ProductNameInput"
        Me.ProductNameInput.Size = New System.Drawing.Size(282, 44)
        Me.ProductNameInput.TabIndex = 3
        Me.ProductNameInput.Text = "Product Name"
        Me.ProductNameInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'productid
        '
        Me.productid.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.productid.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.productid.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.productid.BorderThickness = 1
        Me.productid.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.productid.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.productid.Enabled = False
        Me.productid.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.productid.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.productid.isPassword = False
        Me.productid.Location = New System.Drawing.Point(10, 40)
        Me.productid.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.productid.MaxLength = 32767
        Me.productid.Name = "productid"
        Me.productid.Size = New System.Drawing.Size(561, 44)
        Me.productid.TabIndex = 1
        Me.productid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'TypecomboBox
        '
        Me.TypecomboBox.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.TypecomboBox.FormattingEnabled = True
        Me.TypecomboBox.Items.AddRange(New Object() {"Brand New", "Second Hand"})
        Me.TypecomboBox.Location = New System.Drawing.Point(301, 169)
        Me.TypecomboBox.Name = "TypecomboBox"
        Me.TypecomboBox.Size = New System.Drawing.Size(270, 25)
        Me.TypecomboBox.TabIndex = 17
        Me.TypecomboBox.Text = "Brand New"
        '
        'EditItemForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 590)
        Me.Controls.Add(Me.BunifuCards1)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "EditItemForm"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EditItemForm"
        Me.BunifuCards1.ResumeLayout(False)
        Me.BunifuCards1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents Cancel As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ClearInputField As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents EditItemToList As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents descriptionInput As WindowsFormsControlLibrary1.BunifuCustomTextbox
    Friend WithEvents DiscountIInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents itemPriceInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents CategoryComboBox As ComboBox
    Friend WithEvents ProductBrandInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ProductNameInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents productid As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents itemQuantity As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ClearQntity As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents minusButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents addButton As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddSubtractQuantity As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents TypecomboBox As ComboBox
End Class
