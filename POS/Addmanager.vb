﻿Public Class Addmanager
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Private Structure AddManager
        Dim ManagerId As String
        Dim firstname As String
        Dim lastname As String
        Dim managerPassword As String
        Dim managerConfirmPassword As String
        Dim dateCreated As String
    End Structure

    Private addmanagerInstance As New AddManager

    Private Sub Addmanager_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        addmanagerInstance.ManagerId = String.Empty
        addmanagerInstance.firstname = String.Empty
        addmanagerInstance.lastname = String.Empty
        addmanagerInstance.managerPassword = String.Empty
        addmanagerInstance.managerConfirmPassword = String.Empty
        addmanagerInstance.dateCreated = String.Empty
    End Sub

    Private Sub AddMangerBtn_Click(sender As Object, e As EventArgs) Handles AddMangerBtn.Click

        If AddManagerId.Text <> String.Empty Then

            Dim checkExistingManager As Object = dbConfig.Process_qry("SELECT * FROM manager WHERE ManagerId = ? ", AddManagerId.Text)
            Dim checkingExistingCashier As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE CashierId = ? ", AddManagerId.Text)

            If checkExistingManager.tables(0).Rows.Count = 0 And checkingExistingCashier.tables(0).Rows.Count = 0 Then

                If MangerFirstname.Text <> String.Empty And Lastname.Text <> String.Empty Then
                    If ManagerPassword.Text <> String.Empty And Confirmpassword.Text <> String.Empty Then

                        addmanagerInstance.ManagerId = AddManagerId.Text
                        addmanagerInstance.firstname = MangerFirstname.Text
                        addmanagerInstance.lastname = Lastname.Text
                        addmanagerInstance.managerPassword = ManagerPassword.Text
                        addmanagerInstance.managerConfirmPassword = Confirmpassword.Text
                        addmanagerInstance.dateCreated = String.Format("{0:dd/MM/yyyy - hh:mm:ss tt}", DateTime.Now)

                        If addmanagerInstance.ManagerId.Count = 8 Then
                            If ManagerPassword.Text = Confirmpassword.Text Then

                                Dim addManager As Object = dbConfig.Process_qry("INSERT INTO manager (ManagerId, firstname, lastname, managerpassword, createdate) VALUES (?,?,?,?,?)",
                               addmanagerInstance.ManagerId, addmanagerInstance.firstname, addmanagerInstance.lastname, util.setHash(addmanagerInstance.managerPassword), addmanagerInstance.dateCreated)

                                systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", AddManagerId.Text},
                                {"description", "Account Successfully created!"},
                                {"type", "success"},
                                {"position", "manager"}
                             })

                                If MessageBox.Show("Account Successfully added.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                                    addmanagerInstance.ManagerId = String.Empty
                                    AddManagerId.Text = String.Empty
                                    addmanagerInstance.firstname = String.Empty
                                    MangerFirstname.Text = String.Empty
                                    addmanagerInstance.lastname = String.Empty
                                    Lastname.Text = String.Empty
                                    addmanagerInstance.managerPassword = String.Empty
                                    ManagerPassword.Text = String.Empty
                                    addmanagerInstance.managerConfirmPassword = String.Empty
                                    Confirmpassword.Text = String.Empty
                                    MyBase.Close()
                                End If
                            Else
                                MessageBox.Show("Confirm password does not match!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                addmanagerInstance.managerPassword = String.Empty
                                addmanagerInstance.managerConfirmPassword = String.Empty
                                ManagerPassword.Text = String.Empty
                                Confirmpassword.Text = String.Empty
                            End If

                        Else
                            MessageBox.Show("Please Input password!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Else
                        MessageBox.Show("ID must be 8 in length!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Else
                    MessageBox.Show("Please Input firstname and lastname!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("ID is already exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Please Input Employee ID!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CancelBtn_Click(sender As Object, e As EventArgs) Handles CancelBtn.Click
        MyBase.Close()
    End Sub
End Class