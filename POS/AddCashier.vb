﻿Public Class AddCashier
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Private Structure addCashier
        Dim CashierId As String
        Dim firstname As String
        Dim lastname As String
        Dim cashierPassword As String
        Dim cashierConfirmPassword As String
        Dim dateCreated As String
    End Structure

    Private addCashierInstance As New addCashier

    Private Sub AddCashier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        addCashierInstance.CashierId = String.Empty
        addCashierInstance.firstname = String.Empty
        addCashierInstance.lastname = String.Empty
        addCashierInstance.cashierPassword = String.Empty
        addCashierInstance.cashierConfirmPassword = String.Empty
        addCashierInstance.dateCreated = String.Empty
    End Sub
    Private Sub AddCashierBtn_Click(sender As Object, e As EventArgs) Handles AddCashierBtn.Click

        If AddCashierId.Text <> String.Empty Then

            Dim checkExistingManager As Object = dbConfig.Process_qry("SELECT * FROM manager WHERE ManagerId = ? ", AddCashierId.Text)
            Dim checkingExistingCashier As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE CashierId = ? ", AddCashierId.Text)

            If checkExistingManager.tables(0).Rows.Count = 0 And checkingExistingCashier.tables(0).Rows.Count = 0 Then

                If CashierFirstname.Text <> String.Empty And CashierLastname.Text <> String.Empty Then
                    If CashierPassword.Text <> String.Empty And CashierConfirmPassword.Text <> String.Empty Then

                        addCashierInstance.CashierId = AddCashierId.Text
                        addCashierInstance.firstname = CashierFirstname.Text
                        addCashierInstance.lastname = CashierLastname.Text
                        addCashierInstance.cashierPassword = CashierPassword.Text
                        addCashierInstance.cashierConfirmPassword = CashierConfirmPassword.Text
                        addCashierInstance.dateCreated = String.Format("{0:dd/MM/yyyy - hh:mm:ss tt}", DateTime.Now)

                        If addCashierInstance.CashierId.Count = 8 Then
                            If CashierPassword.Text = CashierConfirmPassword.Text Then

                                Dim addManager As Object = dbConfig.Process_qry("INSERT INTO cashier (cashierid, firstname, lastname, cashierpassword, createdate) VALUES (?,?,?,?,?)",
                               addCashierInstance.CashierId, addCashierInstance.firstname, addCashierInstance.lastname, util.setHash(addCashierInstance.cashierPassword), addCashierInstance.dateCreated)

                                systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", AddCashierId.Text},
                                {"description", "Account Successfully created!"},
                                {"type", "success"},
                                {"position", "cashier"}
                             })

                                If MessageBox.Show("Account Successfully added.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                                    addCashierInstance.CashierId = String.Empty
                                    AddCashierId.Text = String.Empty
                                    addCashierInstance.firstname = String.Empty
                                    CashierFirstname.Text = String.Empty
                                    addCashierInstance.lastname = String.Empty
                                    CashierLastname.Text = String.Empty
                                    addCashierInstance.cashierPassword = String.Empty
                                    CashierPassword.Text = String.Empty
                                    addCashierInstance.cashierConfirmPassword = String.Empty
                                    CashierConfirmPassword.Text = String.Empty
                                    MyBase.Close()
                                End If
                            Else
                                MessageBox.Show("Confirm password does not match!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                addCashierInstance.cashierPassword = String.Empty
                                addCashierInstance.cashierConfirmPassword = String.Empty
                                CashierPassword.Text = String.Empty
                                CashierConfirmPassword.Text = String.Empty
                            End If

                        Else
                            MessageBox.Show("ID must be 8 in length!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Else
                        MessageBox.Show("Please Input password!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If

                Else
                    MessageBox.Show("Please Input firstname and lastname!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("ID is already exist!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Please Input Employee ID!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CancelBtn_Click(sender As Object, e As EventArgs) Handles CancelBtn.Click
        MyBase.Close()
    End Sub
End Class