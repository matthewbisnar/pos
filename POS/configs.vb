﻿'Database Dynamic Connect class
'Author: Matthew Bisnar
'Description: For database connection and data query class.
'Version: 1.0.0

Imports System
Imports System.Text
Imports System.Data.OleDb

Namespace Api
    Public Class Configs

        Public datasetCounter As Integer = 0

        Private Function DbconnectionString() As String
            Return My.Settings.posdatabaseConnectionString
        End Function

        Public Function Process_qry(ByVal query As String, ParamArray ByVal bindVal() As Object) As Object

            Dim dbDataset As New DataSet
            Dim queryCommand As New OleDbCommand
            Dim dbDataApdater As New OleDbDataAdapter

            Using connection As New OleDbConnection(Me.DbconnectionString())

                Try

                    With connection
                        .Open()

                        With queryCommand
                            .Connection = connection
                            .CommandText = query
                            .CommandType = CommandType.Text

                            For i As Integer = 0 To UBound(bindVal, 1)
                                .Parameters.Add(New OleDbParameter("?" & (i + Rnd()), bindVal(i)))
                            Next
                        End With

                        dbDataApdater = New OleDbDataAdapter(queryCommand)
                        Me.datasetCounter = dbDataApdater.Fill(dbDataset)

                        Return dbDataset
                        .Close()
                    End With

                Catch ex As Exception
                    Return MessageBox.Show(ex.Message())
                End Try
            End Using
        End Function
    End Class
End Namespace