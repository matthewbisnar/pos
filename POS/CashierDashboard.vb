﻿Public Class SS

    'import transaction controller to share to class.
    Public transaction As New controller.transactionController

    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Public Structure Cashier
        Dim id As String
        Dim firstname As String
        Dim lastname As String
        Dim position As String
    End Structure

    Public CashierInstance As New Cashier

    Private Sub CashierDashboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        CashierDashboard.BringToFront()
        Timer1.Start()
        Me.loadCashierInfo()
    End Sub

    Private Sub loadCashierInfo()
        Dim fetchcashier As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE cashierid = ?", CashierInstance.id)

        If fetchcashier.tables(0).Rows.Count > 0 Then
            For Each fetch As DataRow In fetchcashier.tables(0).rows
                CashierInstance.firstname = fetch("firstname")
                CashierInstance.lastname = fetch("lastname")
                CashierInstance.position = fetch("position")
            Next
        End If

        AccountName.Text = CashierInstance.firstname & " (" & CashierInstance.position & ")"
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        timezone.Text = "Time zone: " & Format(Now, "dd/mm/yyyy hh:mm:ss tt")
    End Sub

    Private Sub DashboardBtn_Click(sender As Object, e As EventArgs) Handles DashboardBtn.Click
        CashierDashboard.BringToFront()
    End Sub

    Private Sub CashierItemsBtn_Click(sender As Object, e As EventArgs) Handles CashierItemsBtn.Click
        CashierProductComponent1.BringToFront()
    End Sub

    Private Sub TransactioLogs_Click(sender As Object, e As EventArgs) Handles TransactioLogs.Click
        CashierTransactionLogsComponent1.BringToFront()
    End Sub

    Private Sub LogoutBtn_Click(sender As Object, e As EventArgs) Handles LogoutBtn.Click
        Login.Show()
        MyBase.Hide()
    End Sub

    Private Sub CashierItemSearch_Click(sender As Object, e As EventArgs)
        SearchItem.ShowDialog()
    End Sub
End Class