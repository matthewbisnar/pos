﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class maximumAttemptLoginAuth
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.AuthenticateLoginBtn = New System.Windows.Forms.Button()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.AuthenticateLoginInput = New System.Windows.Forms.TextBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Panel1.Controls.Add(Me.AuthenticateLoginBtn)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.AuthenticateLoginInput)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(800, 450)
        Me.Panel1.TabIndex = 0
        '
        'AuthenticateLoginBtn
        '
        Me.AuthenticateLoginBtn.BackColor = System.Drawing.Color.Red
        Me.AuthenticateLoginBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.AuthenticateLoginBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.AuthenticateLoginBtn.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AuthenticateLoginBtn.Location = New System.Drawing.Point(174, 327)
        Me.AuthenticateLoginBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.AuthenticateLoginBtn.Name = "AuthenticateLoginBtn"
        Me.AuthenticateLoginBtn.Size = New System.Drawing.Size(447, 54)
        Me.AuthenticateLoginBtn.TabIndex = 6
        Me.AuthenticateLoginBtn.Text = "Authenticate"
        Me.AuthenticateLoginBtn.UseVisualStyleBackColor = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(244, 103)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(325, 29)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Ask Admin For Authentication"
        '
        'AuthenticateLoginInput
        '
        Me.AuthenticateLoginInput.Location = New System.Drawing.Point(174, 169)
        Me.AuthenticateLoginInput.Multiline = True
        Me.AuthenticateLoginInput.Name = "AuthenticateLoginInput"
        Me.AuthenticateLoginInput.Size = New System.Drawing.Size(447, 57)
        Me.AuthenticateLoginInput.TabIndex = 4
        '
        'maximumAttemptLoginAuth
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "maximumAttemptLoginAuth"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "maximumAttemptLoginAuth"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label2 As Label
    Friend WithEvents AuthenticateLoginInput As TextBox
    Friend WithEvents AuthenticateLoginBtn As Button
    Friend WithEvents Timer1 As Timer
End Class
