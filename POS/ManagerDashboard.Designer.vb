﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ManagerDashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManagerDashboard))
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.AccountName = New System.Windows.Forms.Label()
        Me.timezone = New System.Windows.Forms.Label()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.LogoutBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.CashierList = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.DashboardBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ManagerPanelDashboard = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.SplitContainer2 = New System.Windows.Forms.SplitContainer()
        Me.ItemList = New System.Windows.Forms.ListView()
        Me.ProductId = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductNames = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductBrand = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ItemType = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.itemCategory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Specifications = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductPrice = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.itemDiscount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductQuantity = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.RefreshBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuFlatButton5 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.DeleteItem = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.EditItem = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.additem = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Searchitems = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.SearchBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ManagerCashierComponent1 = New POS.ManagerCashierComponent()
        Me.Panel2.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.ManagerPanelDashboard.SuspendLayout()
        Me.BunifuCards1.SuspendLayout()
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer2.Panel1.SuspendLayout()
        Me.SplitContainer2.Panel2.SuspendLayout()
        Me.SplitContainer2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.AccountName)
        Me.Panel2.Controls.Add(Me.timezone)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1567, 69)
        Me.Panel2.TabIndex = 2
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1360, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(39, 32)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'AccountName
        '
        Me.AccountName.AutoSize = True
        Me.AccountName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AccountName.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AccountName.Location = New System.Drawing.Point(1408, 22)
        Me.AccountName.Name = "AccountName"
        Me.AccountName.Size = New System.Drawing.Size(147, 20)
        Me.AccountName.TabIndex = 4
        Me.AccountName.Text = "Matthew (Manager)"
        '
        'timezone
        '
        Me.timezone.AutoSize = True
        Me.timezone.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.timezone.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.timezone.Location = New System.Drawing.Point(251, 22)
        Me.timezone.Name = "timezone"
        Me.timezone.Size = New System.Drawing.Size(0, 20)
        Me.timezone.TabIndex = 3
        '
        'Panel3
        '
        Me.Panel3.Location = New System.Drawing.Point(0, 63)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1327, 602)
        Me.Panel3.TabIndex = 2
        '
        'Timer1
        '
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 69)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.Color.White
        Me.SplitContainer1.Panel1.Controls.Add(Me.LogoutBtn)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CashierList)
        Me.SplitContainer1.Panel1.Controls.Add(Me.DashboardBtn)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.ManagerPanelDashboard)
        Me.SplitContainer1.Panel2.Controls.Add(Me.ManagerCashierComponent1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1567, 697)
        Me.SplitContainer1.SplitterDistance = 220
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 3
        '
        'LogoutBtn
        '
        Me.LogoutBtn.Active = False
        Me.LogoutBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.LogoutBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.LogoutBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.LogoutBtn.BorderRadius = 0
        Me.LogoutBtn.ButtonText = "Logout"
        Me.LogoutBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LogoutBtn.DisabledColor = System.Drawing.Color.Gray
        Me.LogoutBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.LogoutBtn.Iconimage = CType(resources.GetObject("LogoutBtn.Iconimage"), System.Drawing.Image)
        Me.LogoutBtn.Iconimage_right = Nothing
        Me.LogoutBtn.Iconimage_right_Selected = Nothing
        Me.LogoutBtn.Iconimage_Selected = Nothing
        Me.LogoutBtn.IconMarginLeft = 0
        Me.LogoutBtn.IconMarginRight = 0
        Me.LogoutBtn.IconRightVisible = True
        Me.LogoutBtn.IconRightZoom = 0R
        Me.LogoutBtn.IconVisible = True
        Me.LogoutBtn.IconZoom = 90.0R
        Me.LogoutBtn.IsTab = False
        Me.LogoutBtn.Location = New System.Drawing.Point(-2, 135)
        Me.LogoutBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.LogoutBtn.Name = "LogoutBtn"
        Me.LogoutBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.LogoutBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.LogoutBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.LogoutBtn.selected = False
        Me.LogoutBtn.Size = New System.Drawing.Size(235, 67)
        Me.LogoutBtn.TabIndex = 3
        Me.LogoutBtn.Text = "Logout"
        Me.LogoutBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.LogoutBtn.Textcolor = System.Drawing.Color.White
        Me.LogoutBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'CashierList
        '
        Me.CashierList.Active = False
        Me.CashierList.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierList.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierList.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CashierList.BorderRadius = 0
        Me.CashierList.ButtonText = "Cashiers"
        Me.CashierList.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CashierList.DisabledColor = System.Drawing.Color.Gray
        Me.CashierList.Iconcolor = System.Drawing.Color.Transparent
        Me.CashierList.Iconimage = CType(resources.GetObject("CashierList.Iconimage"), System.Drawing.Image)
        Me.CashierList.Iconimage_right = Nothing
        Me.CashierList.Iconimage_right_Selected = Nothing
        Me.CashierList.Iconimage_Selected = Nothing
        Me.CashierList.IconMarginLeft = 0
        Me.CashierList.IconMarginRight = 0
        Me.CashierList.IconRightVisible = True
        Me.CashierList.IconRightZoom = 0R
        Me.CashierList.IconVisible = True
        Me.CashierList.IconZoom = 90.0R
        Me.CashierList.IsTab = False
        Me.CashierList.Location = New System.Drawing.Point(-2, 68)
        Me.CashierList.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.CashierList.Name = "CashierList"
        Me.CashierList.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierList.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.CashierList.OnHoverTextColor = System.Drawing.Color.White
        Me.CashierList.selected = False
        Me.CashierList.Size = New System.Drawing.Size(235, 67)
        Me.CashierList.TabIndex = 2
        Me.CashierList.Text = "Cashiers"
        Me.CashierList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.CashierList.Textcolor = System.Drawing.Color.White
        Me.CashierList.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'DashboardBtn
        '
        Me.DashboardBtn.Active = False
        Me.DashboardBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DashboardBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DashboardBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DashboardBtn.BorderRadius = 0
        Me.DashboardBtn.ButtonText = "Dashboard"
        Me.DashboardBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DashboardBtn.DisabledColor = System.Drawing.Color.Gray
        Me.DashboardBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.DashboardBtn.Iconimage = CType(resources.GetObject("DashboardBtn.Iconimage"), System.Drawing.Image)
        Me.DashboardBtn.Iconimage_right = Nothing
        Me.DashboardBtn.Iconimage_right_Selected = Nothing
        Me.DashboardBtn.Iconimage_Selected = Nothing
        Me.DashboardBtn.IconMarginLeft = 0
        Me.DashboardBtn.IconMarginRight = 0
        Me.DashboardBtn.IconRightVisible = True
        Me.DashboardBtn.IconRightZoom = 0R
        Me.DashboardBtn.IconVisible = True
        Me.DashboardBtn.IconZoom = 90.0R
        Me.DashboardBtn.IsTab = False
        Me.DashboardBtn.Location = New System.Drawing.Point(0, 1)
        Me.DashboardBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.DashboardBtn.Name = "DashboardBtn"
        Me.DashboardBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DashboardBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.DashboardBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.DashboardBtn.selected = False
        Me.DashboardBtn.Size = New System.Drawing.Size(235, 67)
        Me.DashboardBtn.TabIndex = 1
        Me.DashboardBtn.Text = "Dashboard"
        Me.DashboardBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DashboardBtn.Textcolor = System.Drawing.Color.White
        Me.DashboardBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ManagerPanelDashboard
        '
        Me.ManagerPanelDashboard.Controls.Add(Me.Panel5)
        Me.ManagerPanelDashboard.Controls.Add(Me.BunifuCards1)
        Me.ManagerPanelDashboard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ManagerPanelDashboard.Location = New System.Drawing.Point(0, 0)
        Me.ManagerPanelDashboard.Name = "ManagerPanelDashboard"
        Me.ManagerPanelDashboard.Size = New System.Drawing.Size(1344, 697)
        Me.ManagerPanelDashboard.TabIndex = 2
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 644)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1344, 53)
        Me.Panel5.TabIndex = 3
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.SplitContainer2)
        Me.BunifuCards1.Controls.Add(Me.Panel4)
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(14, 17)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(1316, 614)
        Me.BunifuCards1.TabIndex = 0
        '
        'SplitContainer2
        '
        Me.SplitContainer2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer2.Location = New System.Drawing.Point(0, 51)
        Me.SplitContainer2.Name = "SplitContainer2"
        '
        'SplitContainer2.Panel1
        '
        Me.SplitContainer2.Panel1.Controls.Add(Me.ItemList)
        '
        'SplitContainer2.Panel2
        '
        Me.SplitContainer2.Panel2.Controls.Add(Me.RefreshBtn)
        Me.SplitContainer2.Panel2.Controls.Add(Me.BunifuFlatButton5)
        Me.SplitContainer2.Panel2.Controls.Add(Me.DeleteItem)
        Me.SplitContainer2.Panel2.Controls.Add(Me.EditItem)
        Me.SplitContainer2.Panel2.Controls.Add(Me.additem)
        Me.SplitContainer2.Size = New System.Drawing.Size(1316, 563)
        Me.SplitContainer2.SplitterDistance = 1116
        Me.SplitContainer2.TabIndex = 2
        '
        'ItemList
        '
        Me.ItemList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ProductId, Me.ProductNames, Me.ProductBrand, Me.ItemType, Me.itemCategory, Me.Specifications, Me.ProductPrice, Me.itemDiscount, Me.ProductQuantity})
        Me.ItemList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ItemList.FullRowSelect = True
        Me.ItemList.GridLines = True
        Me.ItemList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ItemList.HideSelection = False
        Me.ItemList.Location = New System.Drawing.Point(0, 0)
        Me.ItemList.Margin = New System.Windows.Forms.Padding(0)
        Me.ItemList.Name = "ItemList"
        Me.ItemList.Size = New System.Drawing.Size(1116, 563)
        Me.ItemList.TabIndex = 3
        Me.ItemList.UseCompatibleStateImageBehavior = False
        Me.ItemList.View = System.Windows.Forms.View.Details
        '
        'ProductId
        '
        Me.ProductId.Text = "Product ID"
        Me.ProductId.Width = 215
        '
        'ProductNames
        '
        Me.ProductNames.Text = "Product Name"
        Me.ProductNames.Width = 281
        '
        'ProductBrand
        '
        Me.ProductBrand.Text = "Brand"
        Me.ProductBrand.Width = 228
        '
        'ItemType
        '
        Me.ItemType.Text = "Item type"
        Me.ItemType.Width = 182
        '
        'itemCategory
        '
        Me.itemCategory.Text = "Category"
        Me.itemCategory.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.itemCategory.Width = 229
        '
        'Specifications
        '
        Me.Specifications.Text = "Specifications"
        Me.Specifications.Width = 162
        '
        'ProductPrice
        '
        Me.ProductPrice.Text = "Price"
        Me.ProductPrice.Width = 127
        '
        'itemDiscount
        '
        Me.itemDiscount.Text = "Discount"
        Me.itemDiscount.Width = 124
        '
        'ProductQuantity
        '
        Me.ProductQuantity.Text = "Quantity"
        Me.ProductQuantity.Width = 246
        '
        'RefreshBtn
        '
        Me.RefreshBtn.Active = False
        Me.RefreshBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RefreshBtn.BorderRadius = 0
        Me.RefreshBtn.ButtonText = "Refresh "
        Me.RefreshBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshBtn.DisabledColor = System.Drawing.Color.Gray
        Me.RefreshBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.RefreshBtn.Iconimage = CType(resources.GetObject("RefreshBtn.Iconimage"), System.Drawing.Image)
        Me.RefreshBtn.Iconimage_right = Nothing
        Me.RefreshBtn.Iconimage_right_Selected = Nothing
        Me.RefreshBtn.Iconimage_Selected = Nothing
        Me.RefreshBtn.IconMarginLeft = 0
        Me.RefreshBtn.IconMarginRight = 0
        Me.RefreshBtn.IconRightVisible = True
        Me.RefreshBtn.IconRightZoom = 0R
        Me.RefreshBtn.IconVisible = True
        Me.RefreshBtn.IconZoom = 90.0R
        Me.RefreshBtn.IsTab = False
        Me.RefreshBtn.Location = New System.Drawing.Point(0, 4)
        Me.RefreshBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.RefreshBtn.Name = "RefreshBtn"
        Me.RefreshBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.RefreshBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.RefreshBtn.selected = False
        Me.RefreshBtn.Size = New System.Drawing.Size(195, 60)
        Me.RefreshBtn.TabIndex = 7
        Me.RefreshBtn.Text = "Refresh "
        Me.RefreshBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RefreshBtn.Textcolor = System.Drawing.Color.White
        Me.RefreshBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuFlatButton5
        '
        Me.BunifuFlatButton5.Active = False
        Me.BunifuFlatButton5.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.BunifuFlatButton5.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.BunifuFlatButton5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton5.BorderRadius = 0
        Me.BunifuFlatButton5.ButtonText = "Export"
        Me.BunifuFlatButton5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuFlatButton5.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton5.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton5.Iconimage = CType(resources.GetObject("BunifuFlatButton5.Iconimage"), System.Drawing.Image)
        Me.BunifuFlatButton5.Iconimage_right = Nothing
        Me.BunifuFlatButton5.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton5.Iconimage_Selected = Nothing
        Me.BunifuFlatButton5.IconMarginLeft = 0
        Me.BunifuFlatButton5.IconMarginRight = 0
        Me.BunifuFlatButton5.IconRightVisible = True
        Me.BunifuFlatButton5.IconRightZoom = 0R
        Me.BunifuFlatButton5.IconVisible = True
        Me.BunifuFlatButton5.IconZoom = 90.0R
        Me.BunifuFlatButton5.IsTab = False
        Me.BunifuFlatButton5.Location = New System.Drawing.Point(0, 244)
        Me.BunifuFlatButton5.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.BunifuFlatButton5.Name = "BunifuFlatButton5"
        Me.BunifuFlatButton5.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.BunifuFlatButton5.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.BunifuFlatButton5.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton5.selected = False
        Me.BunifuFlatButton5.Size = New System.Drawing.Size(195, 60)
        Me.BunifuFlatButton5.TabIndex = 6
        Me.BunifuFlatButton5.Text = "Export"
        Me.BunifuFlatButton5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuFlatButton5.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton5.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'DeleteItem
        '
        Me.DeleteItem.Active = False
        Me.DeleteItem.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DeleteItem.BorderRadius = 0
        Me.DeleteItem.ButtonText = "Delete Item"
        Me.DeleteItem.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteItem.DisabledColor = System.Drawing.Color.Gray
        Me.DeleteItem.Iconcolor = System.Drawing.Color.Transparent
        Me.DeleteItem.Iconimage = CType(resources.GetObject("DeleteItem.Iconimage"), System.Drawing.Image)
        Me.DeleteItem.Iconimage_right = Nothing
        Me.DeleteItem.Iconimage_right_Selected = Nothing
        Me.DeleteItem.Iconimage_Selected = Nothing
        Me.DeleteItem.IconMarginLeft = 0
        Me.DeleteItem.IconMarginRight = 0
        Me.DeleteItem.IconRightVisible = True
        Me.DeleteItem.IconRightZoom = 0R
        Me.DeleteItem.IconVisible = True
        Me.DeleteItem.IconZoom = 90.0R
        Me.DeleteItem.IsTab = False
        Me.DeleteItem.Location = New System.Drawing.Point(0, 184)
        Me.DeleteItem.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.DeleteItem.Name = "DeleteItem"
        Me.DeleteItem.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteItem.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.DeleteItem.OnHoverTextColor = System.Drawing.Color.White
        Me.DeleteItem.selected = False
        Me.DeleteItem.Size = New System.Drawing.Size(195, 60)
        Me.DeleteItem.TabIndex = 5
        Me.DeleteItem.Text = "Delete Item"
        Me.DeleteItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DeleteItem.Textcolor = System.Drawing.Color.White
        Me.DeleteItem.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'EditItem
        '
        Me.EditItem.Active = False
        Me.EditItem.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditItem.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.EditItem.BorderRadius = 0
        Me.EditItem.ButtonText = "Edit Item"
        Me.EditItem.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditItem.DisabledColor = System.Drawing.Color.Gray
        Me.EditItem.Iconcolor = System.Drawing.Color.Transparent
        Me.EditItem.Iconimage = CType(resources.GetObject("EditItem.Iconimage"), System.Drawing.Image)
        Me.EditItem.Iconimage_right = Nothing
        Me.EditItem.Iconimage_right_Selected = Nothing
        Me.EditItem.Iconimage_Selected = Nothing
        Me.EditItem.IconMarginLeft = 0
        Me.EditItem.IconMarginRight = 0
        Me.EditItem.IconRightVisible = True
        Me.EditItem.IconRightZoom = 0R
        Me.EditItem.IconVisible = True
        Me.EditItem.IconZoom = 90.0R
        Me.EditItem.IsTab = False
        Me.EditItem.Location = New System.Drawing.Point(0, 124)
        Me.EditItem.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.EditItem.Name = "EditItem"
        Me.EditItem.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditItem.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.EditItem.OnHoverTextColor = System.Drawing.Color.White
        Me.EditItem.selected = False
        Me.EditItem.Size = New System.Drawing.Size(195, 60)
        Me.EditItem.TabIndex = 4
        Me.EditItem.Text = "Edit Item"
        Me.EditItem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.EditItem.Textcolor = System.Drawing.Color.White
        Me.EditItem.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'additem
        '
        Me.additem.Active = False
        Me.additem.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.additem.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.additem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.additem.BorderRadius = 0
        Me.additem.ButtonText = "Add Item"
        Me.additem.Cursor = System.Windows.Forms.Cursors.Hand
        Me.additem.DisabledColor = System.Drawing.Color.Gray
        Me.additem.Iconcolor = System.Drawing.Color.Transparent
        Me.additem.Iconimage = CType(resources.GetObject("additem.Iconimage"), System.Drawing.Image)
        Me.additem.Iconimage_right = Nothing
        Me.additem.Iconimage_right_Selected = Nothing
        Me.additem.Iconimage_Selected = Nothing
        Me.additem.IconMarginLeft = 0
        Me.additem.IconMarginRight = 0
        Me.additem.IconRightVisible = True
        Me.additem.IconRightZoom = 0R
        Me.additem.IconVisible = True
        Me.additem.IconZoom = 90.0R
        Me.additem.IsTab = False
        Me.additem.Location = New System.Drawing.Point(0, 64)
        Me.additem.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.additem.Name = "additem"
        Me.additem.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.additem.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.additem.OnHoverTextColor = System.Drawing.Color.White
        Me.additem.selected = False
        Me.additem.Size = New System.Drawing.Size(195, 60)
        Me.additem.TabIndex = 3
        Me.additem.Text = "Add Item"
        Me.additem.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.additem.Textcolor = System.Drawing.Color.White
        Me.additem.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Searchitems)
        Me.Panel4.Controls.Add(Me.SearchBtn)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel4.Location = New System.Drawing.Point(0, 0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1316, 51)
        Me.Panel4.TabIndex = 1
        '
        'Searchitems
        '
        Me.Searchitems.BorderColorFocused = System.Drawing.Color.Blue
        Me.Searchitems.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Searchitems.BorderColorMouseHover = System.Drawing.Color.Blue
        Me.Searchitems.BorderThickness = 1
        Me.Searchitems.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.Searchitems.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.Searchitems.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.Searchitems.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.Searchitems.isPassword = False
        Me.Searchitems.Location = New System.Drawing.Point(411, 1)
        Me.Searchitems.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Searchitems.MaxLength = 32767
        Me.Searchitems.Name = "Searchitems"
        Me.Searchitems.Size = New System.Drawing.Size(706, 50)
        Me.Searchitems.TabIndex = 3
        Me.Searchitems.Text = "Search Items"
        Me.Searchitems.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SearchBtn
        '
        Me.SearchBtn.Active = False
        Me.SearchBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SearchBtn.BorderRadius = 0
        Me.SearchBtn.ButtonText = "Search"
        Me.SearchBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SearchBtn.DisabledColor = System.Drawing.Color.Gray
        Me.SearchBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.SearchBtn.Iconimage = CType(resources.GetObject("SearchBtn.Iconimage"), System.Drawing.Image)
        Me.SearchBtn.Iconimage_right = Nothing
        Me.SearchBtn.Iconimage_right_Selected = Nothing
        Me.SearchBtn.Iconimage_Selected = Nothing
        Me.SearchBtn.IconMarginLeft = 0
        Me.SearchBtn.IconMarginRight = 0
        Me.SearchBtn.IconRightVisible = True
        Me.SearchBtn.IconRightZoom = 0R
        Me.SearchBtn.IconVisible = True
        Me.SearchBtn.IconZoom = 90.0R
        Me.SearchBtn.IsTab = False
        Me.SearchBtn.Location = New System.Drawing.Point(1119, 0)
        Me.SearchBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.SearchBtn.Name = "SearchBtn"
        Me.SearchBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SearchBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.SearchBtn.selected = False
        Me.SearchBtn.Size = New System.Drawing.Size(196, 51)
        Me.SearchBtn.TabIndex = 2
        Me.SearchBtn.Text = "Search"
        Me.SearchBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SearchBtn.Textcolor = System.Drawing.Color.White
        Me.SearchBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ManagerCashierComponent1
        '
        Me.ManagerCashierComponent1.BackColor = System.Drawing.Color.Lavender
        Me.ManagerCashierComponent1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ManagerCashierComponent1.Location = New System.Drawing.Point(0, 0)
        Me.ManagerCashierComponent1.Margin = New System.Windows.Forms.Padding(1, 1, 1, 1)
        Me.ManagerCashierComponent1.Name = "ManagerCashierComponent1"
        Me.ManagerCashierComponent1.Size = New System.Drawing.Size(1344, 697)
        Me.ManagerCashierComponent1.TabIndex = 0
        '
        'ManagerDashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.ClientSize = New System.Drawing.Size(1567, 766)
        Me.Controls.Add(Me.SplitContainer1)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "ManagerDashboard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ManagerDashboard"
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ManagerPanelDashboard.ResumeLayout(False)
        Me.BunifuCards1.ResumeLayout(False)
        Me.SplitContainer2.Panel1.ResumeLayout(False)
        Me.SplitContainer2.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer2.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents AccountName As Label
    Friend WithEvents timezone As Label
    Friend WithEvents Panel3 As Panel
    Friend WithEvents Timer1 As Timer
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents DashboardBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents LogoutBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents CashierList As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ManagerCashierComponent1 As ManagerCashierComponent
    Friend WithEvents Panel2 As Panel
    Friend WithEvents ManagerPanelDashboard As Panel
    Friend WithEvents Panel5 As Panel
    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents SplitContainer2 As SplitContainer
    Friend WithEvents ItemList As ListView
    Friend WithEvents ProductId As ColumnHeader
    Friend WithEvents ProductNames As ColumnHeader
    Friend WithEvents ProductBrand As ColumnHeader
    Friend WithEvents itemCategory As ColumnHeader
    Friend WithEvents Specifications As ColumnHeader
    Friend WithEvents ProductPrice As ColumnHeader
    Friend WithEvents itemDiscount As ColumnHeader
    Friend WithEvents RefreshBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents BunifuFlatButton5 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents DeleteItem As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents EditItem As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents additem As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Searchitems As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents SearchBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ProductQuantity As ColumnHeader
    Friend WithEvents ItemType As ColumnHeader
End Class
