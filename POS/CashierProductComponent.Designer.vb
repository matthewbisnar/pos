﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CashierProductComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CashierProductComponent))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ItemList = New System.Windows.Forms.ListView()
        Me.ProductId = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductNames = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductBrand = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ItemType = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.itemCategory = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Specifications = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductPrice = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.itemDiscount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductQuantity = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.searchBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.searchiteminputs = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.refreshBtn = New Bunifu.Framework.UI.BunifuImageButton()
        Me.Panel1.SuspendLayout()
        Me.BunifuCards1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        CType(Me.refreshBtn, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Panel5)
        Me.Panel1.Controls.Add(Me.BunifuCards1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1351, 729)
        Me.Panel1.TabIndex = 0
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 676)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1351, 53)
        Me.Panel5.TabIndex = 3
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.SplitContainer1)
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(17, 19)
        Me.BunifuCards1.Margin = New System.Windows.Forms.Padding(2)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(1314, 637)
        Me.BunifuCards1.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(2)
        Me.SplitContainer1.Name = "SplitContainer1"
        Me.SplitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.refreshBtn)
        Me.SplitContainer1.Panel1.Controls.Add(Me.searchiteminputs)
        Me.SplitContainer1.Panel1.Controls.Add(Me.searchBtn)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.ItemList)
        Me.SplitContainer1.Size = New System.Drawing.Size(1314, 637)
        Me.SplitContainer1.SplitterDistance = 51
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 1
        '
        'ItemList
        '
        Me.ItemList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ProductId, Me.ProductNames, Me.ProductBrand, Me.ItemType, Me.itemCategory, Me.Specifications, Me.ProductPrice, Me.itemDiscount, Me.ProductQuantity})
        Me.ItemList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ItemList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ItemList.FullRowSelect = True
        Me.ItemList.GridLines = True
        Me.ItemList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.ItemList.HideSelection = False
        Me.ItemList.Location = New System.Drawing.Point(0, 0)
        Me.ItemList.Margin = New System.Windows.Forms.Padding(0)
        Me.ItemList.Name = "ItemList"
        Me.ItemList.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ItemList.RightToLeftLayout = True
        Me.ItemList.Size = New System.Drawing.Size(1314, 583)
        Me.ItemList.TabIndex = 4
        Me.ItemList.UseCompatibleStateImageBehavior = False
        Me.ItemList.View = System.Windows.Forms.View.Details
        '
        'ProductId
        '
        Me.ProductId.Text = "Product ID"
        Me.ProductId.Width = 215
        '
        'ProductNames
        '
        Me.ProductNames.Text = "Product Name"
        Me.ProductNames.Width = 281
        '
        'ProductBrand
        '
        Me.ProductBrand.Text = "Brand"
        Me.ProductBrand.Width = 228
        '
        'ItemType
        '
        Me.ItemType.Text = "Item type"
        Me.ItemType.Width = 182
        '
        'itemCategory
        '
        Me.itemCategory.Text = "Category"
        Me.itemCategory.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.itemCategory.Width = 229
        '
        'Specifications
        '
        Me.Specifications.Text = "Specifications"
        Me.Specifications.Width = 162
        '
        'ProductPrice
        '
        Me.ProductPrice.Text = "Price"
        Me.ProductPrice.Width = 127
        '
        'itemDiscount
        '
        Me.itemDiscount.Text = "Discount"
        Me.itemDiscount.Width = 124
        '
        'ProductQuantity
        '
        Me.ProductQuantity.Text = "Quantity"
        Me.ProductQuantity.Width = 246
        '
        'searchBtn
        '
        Me.searchBtn.Active = False
        Me.searchBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.searchBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.searchBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.searchBtn.BorderRadius = 0
        Me.searchBtn.ButtonText = "Search"
        Me.searchBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.searchBtn.DisabledColor = System.Drawing.Color.Gray
        Me.searchBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.searchBtn.Iconimage = CType(resources.GetObject("searchBtn.Iconimage"), System.Drawing.Image)
        Me.searchBtn.Iconimage_right = Nothing
        Me.searchBtn.Iconimage_right_Selected = Nothing
        Me.searchBtn.Iconimage_Selected = Nothing
        Me.searchBtn.IconMarginLeft = 0
        Me.searchBtn.IconMarginRight = 0
        Me.searchBtn.IconRightVisible = True
        Me.searchBtn.IconRightZoom = 0R
        Me.searchBtn.IconVisible = True
        Me.searchBtn.IconZoom = 90.0R
        Me.searchBtn.IsTab = False
        Me.searchBtn.Location = New System.Drawing.Point(1076, 3)
        Me.searchBtn.Name = "searchBtn"
        Me.searchBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.searchBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.searchBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.searchBtn.selected = False
        Me.searchBtn.Size = New System.Drawing.Size(238, 48)
        Me.searchBtn.TabIndex = 0
        Me.searchBtn.Text = "Search"
        Me.searchBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.searchBtn.Textcolor = System.Drawing.Color.White
        Me.searchBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'searchiteminputs
        '
        Me.searchiteminputs.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.searchiteminputs.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.searchiteminputs.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.searchiteminputs.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.searchiteminputs.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.searchiteminputs.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.searchiteminputs.HintForeColor = System.Drawing.Color.Empty
        Me.searchiteminputs.HintText = ""
        Me.searchiteminputs.isPassword = False
        Me.searchiteminputs.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.searchiteminputs.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.searchiteminputs.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.searchiteminputs.LineThickness = 2
        Me.searchiteminputs.Location = New System.Drawing.Point(292, 4)
        Me.searchiteminputs.Margin = New System.Windows.Forms.Padding(4)
        Me.searchiteminputs.MaxLength = 32767
        Me.searchiteminputs.Name = "searchiteminputs"
        Me.searchiteminputs.Size = New System.Drawing.Size(780, 47)
        Me.searchiteminputs.TabIndex = 1
        Me.searchiteminputs.Text = "Search items.."
        Me.searchiteminputs.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'refreshBtn
        '
        Me.refreshBtn.BackColor = System.Drawing.Color.SeaGreen
        Me.refreshBtn.ErrorImage = Nothing
        Me.refreshBtn.Image = CType(resources.GetObject("refreshBtn.Image"), System.Drawing.Image)
        Me.refreshBtn.ImageActive = Nothing
        Me.refreshBtn.InitialImage = Nothing
        Me.refreshBtn.Location = New System.Drawing.Point(239, 5)
        Me.refreshBtn.Name = "refreshBtn"
        Me.refreshBtn.Size = New System.Drawing.Size(46, 46)
        Me.refreshBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage
        Me.refreshBtn.TabIndex = 2
        Me.refreshBtn.TabStop = False
        Me.refreshBtn.Zoom = 10
        '
        'CashierProductComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.Controls.Add(Me.Panel1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "CashierProductComponent"
        Me.Size = New System.Drawing.Size(1351, 729)
        Me.Panel1.ResumeLayout(False)
        Me.BunifuCards1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        CType(Me.refreshBtn, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents Panel1 As Panel
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents ItemList As ListView
    Friend WithEvents ProductId As ColumnHeader
    Friend WithEvents ProductNames As ColumnHeader
    Friend WithEvents ProductBrand As ColumnHeader
    Friend WithEvents ItemType As ColumnHeader
    Friend WithEvents itemCategory As ColumnHeader
    Friend WithEvents Specifications As ColumnHeader
    Friend WithEvents ProductPrice As ColumnHeader
    Friend WithEvents itemDiscount As ColumnHeader
    Friend WithEvents ProductQuantity As ColumnHeader
    Friend WithEvents Panel5 As Panel
    Friend WithEvents searchiteminputs As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents searchBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents refreshBtn As Bunifu.Framework.UI.BunifuImageButton
End Class
