﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class CashierLoginComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(CashierLoginComponent))
        Me.CashierLogin = New System.Windows.Forms.Panel()
        Me.CashierLoginBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.CashierUsernameInput = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.CashierPaswordInput = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.LoginAttemptNotif = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.CC = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.CashierLogin.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CashierLogin
        '
        Me.CashierLogin.BackColor = System.Drawing.Color.Lavender
        Me.CashierLogin.Controls.Add(Me.CashierLoginBtn)
        Me.CashierLogin.Controls.Add(Me.CashierUsernameInput)
        Me.CashierLogin.Controls.Add(Me.CashierPaswordInput)
        Me.CashierLogin.Controls.Add(Me.LoginAttemptNotif)
        Me.CashierLogin.Controls.Add(Me.PictureBox1)
        Me.CashierLogin.Controls.Add(Me.CC)
        Me.CashierLogin.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierLogin.Location = New System.Drawing.Point(0, 0)
        Me.CashierLogin.Name = "CashierLogin"
        Me.CashierLogin.Size = New System.Drawing.Size(592, 926)
        Me.CashierLogin.TabIndex = 3
        '
        'CashierLoginBtn
        '
        Me.CashierLoginBtn.Active = False
        Me.CashierLoginBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierLoginBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierLoginBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CashierLoginBtn.BorderRadius = 0
        Me.CashierLoginBtn.ButtonText = "Cashier Login"
        Me.CashierLoginBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CashierLoginBtn.DisabledColor = System.Drawing.Color.Gray
        Me.CashierLoginBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.CashierLoginBtn.Iconimage = CType(resources.GetObject("CashierLoginBtn.Iconimage"), System.Drawing.Image)
        Me.CashierLoginBtn.Iconimage_right = Nothing
        Me.CashierLoginBtn.Iconimage_right_Selected = Nothing
        Me.CashierLoginBtn.Iconimage_Selected = Nothing
        Me.CashierLoginBtn.IconMarginLeft = 0
        Me.CashierLoginBtn.IconMarginRight = 0
        Me.CashierLoginBtn.IconRightVisible = True
        Me.CashierLoginBtn.IconRightZoom = 0R
        Me.CashierLoginBtn.IconVisible = True
        Me.CashierLoginBtn.IconZoom = 90.0R
        Me.CashierLoginBtn.IsTab = False
        Me.CashierLoginBtn.Location = New System.Drawing.Point(88, 725)
        Me.CashierLoginBtn.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.CashierLoginBtn.Name = "CashierLoginBtn"
        Me.CashierLoginBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierLoginBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.CashierLoginBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.CashierLoginBtn.selected = False
        Me.CashierLoginBtn.Size = New System.Drawing.Size(412, 74)
        Me.CashierLoginBtn.TabIndex = 10
        Me.CashierLoginBtn.Text = "Cashier Login"
        Me.CashierLoginBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.CashierLoginBtn.Textcolor = System.Drawing.Color.White
        Me.CashierLoginBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'CashierUsernameInput
        '
        Me.CashierUsernameInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.CashierUsernameInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.CashierUsernameInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.CashierUsernameInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.CashierUsernameInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.CashierUsernameInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CashierUsernameInput.HintForeColor = System.Drawing.Color.Empty
        Me.CashierUsernameInput.HintText = ""
        Me.CashierUsernameInput.isPassword = False
        Me.CashierUsernameInput.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.CashierUsernameInput.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.CashierUsernameInput.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.CashierUsernameInput.LineThickness = 2
        Me.CashierUsernameInput.Location = New System.Drawing.Point(88, 422)
        Me.CashierUsernameInput.Margin = New System.Windows.Forms.Padding(6)
        Me.CashierUsernameInput.MaxLength = 32767
        Me.CashierUsernameInput.Name = "CashierUsernameInput"
        Me.CashierUsernameInput.Size = New System.Drawing.Size(412, 58)
        Me.CashierUsernameInput.TabIndex = 9
        Me.CashierUsernameInput.Text = "Cashier Login"
        Me.CashierUsernameInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'CashierPaswordInput
        '
        Me.CashierPaswordInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.CashierPaswordInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.CashierPaswordInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.CashierPaswordInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.CashierPaswordInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.CashierPaswordInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CashierPaswordInput.HintForeColor = System.Drawing.Color.Empty
        Me.CashierPaswordInput.HintText = ""
        Me.CashierPaswordInput.isPassword = True
        Me.CashierPaswordInput.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.CashierPaswordInput.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.CashierPaswordInput.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.CashierPaswordInput.LineThickness = 2
        Me.CashierPaswordInput.Location = New System.Drawing.Point(88, 568)
        Me.CashierPaswordInput.Margin = New System.Windows.Forms.Padding(6)
        Me.CashierPaswordInput.MaxLength = 32767
        Me.CashierPaswordInput.Name = "CashierPaswordInput"
        Me.CashierPaswordInput.Size = New System.Drawing.Size(412, 58)
        Me.CashierPaswordInput.TabIndex = 8
        Me.CashierPaswordInput.Text = "BunifuMaterialTextbox1"
        Me.CashierPaswordInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'LoginAttemptNotif
        '
        Me.LoginAttemptNotif.AutoSize = True
        Me.LoginAttemptNotif.ForeColor = System.Drawing.Color.Red
        Me.LoginAttemptNotif.Location = New System.Drawing.Point(93, 828)
        Me.LoginAttemptNotif.Name = "LoginAttemptNotif"
        Me.LoginAttemptNotif.Size = New System.Drawing.Size(0, 20)
        Me.LoginAttemptNotif.TabIndex = 7
        '
        'PictureBox1
        '
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(213, 75)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(176, 169)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'CC
        '
        Me.CC.AutoSize = True
        Me.CC.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CC.Location = New System.Drawing.Point(182, 297)
        Me.CC.Name = "CC"
        Me.CC.Size = New System.Drawing.Size(233, 32)
        Me.CC.TabIndex = 0
        Me.CC.Text = "LOGIN CASHIER"
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'CashierLoginComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.CashierLogin)
        Me.Name = "CashierLoginComponent"
        Me.Size = New System.Drawing.Size(592, 926)
        Me.CashierLogin.ResumeLayout(False)
        Me.CashierLogin.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents CashierLogin As Panel
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents CC As Label
    Friend WithEvents LoginAttemptNotif As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents CashierUsernameInput As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents CashierPaswordInput As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents CashierLoginBtn As Bunifu.Framework.UI.BunifuFlatButton
End Class
