﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ManagerLoginComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManagerLoginComponent))
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.ManagerLoginBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ManagerUsernameInput = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.ManagerPasswordInput = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.LoginAttemptNotif = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Lavender
        Me.Panel1.Controls.Add(Me.ManagerLoginBtn)
        Me.Panel1.Controls.Add(Me.ManagerUsernameInput)
        Me.Panel1.Controls.Add(Me.ManagerPasswordInput)
        Me.Panel1.Controls.Add(Me.LoginAttemptNotif)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(592, 966)
        Me.Panel1.TabIndex = 12
        '
        'ManagerLoginBtn
        '
        Me.ManagerLoginBtn.Active = False
        Me.ManagerLoginBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ManagerLoginBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ManagerLoginBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ManagerLoginBtn.BorderRadius = 0
        Me.ManagerLoginBtn.ButtonText = "Manager Login"
        Me.ManagerLoginBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ManagerLoginBtn.DisabledColor = System.Drawing.Color.Gray
        Me.ManagerLoginBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.ManagerLoginBtn.Iconimage = CType(resources.GetObject("ManagerLoginBtn.Iconimage"), System.Drawing.Image)
        Me.ManagerLoginBtn.Iconimage_right = Nothing
        Me.ManagerLoginBtn.Iconimage_right_Selected = Nothing
        Me.ManagerLoginBtn.Iconimage_Selected = Nothing
        Me.ManagerLoginBtn.IconMarginLeft = 0
        Me.ManagerLoginBtn.IconMarginRight = 0
        Me.ManagerLoginBtn.IconRightVisible = True
        Me.ManagerLoginBtn.IconRightZoom = 0R
        Me.ManagerLoginBtn.IconVisible = True
        Me.ManagerLoginBtn.IconZoom = 90.0R
        Me.ManagerLoginBtn.IsTab = False
        Me.ManagerLoginBtn.Location = New System.Drawing.Point(88, 725)
        Me.ManagerLoginBtn.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.ManagerLoginBtn.Name = "ManagerLoginBtn"
        Me.ManagerLoginBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ManagerLoginBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ManagerLoginBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.ManagerLoginBtn.selected = False
        Me.ManagerLoginBtn.Size = New System.Drawing.Size(412, 74)
        Me.ManagerLoginBtn.TabIndex = 10
        Me.ManagerLoginBtn.Text = "Manager Login"
        Me.ManagerLoginBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ManagerLoginBtn.Textcolor = System.Drawing.Color.White
        Me.ManagerLoginBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ManagerUsernameInput
        '
        Me.ManagerUsernameInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.ManagerUsernameInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.ManagerUsernameInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.ManagerUsernameInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ManagerUsernameInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.ManagerUsernameInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ManagerUsernameInput.HintForeColor = System.Drawing.Color.Empty
        Me.ManagerUsernameInput.HintText = ""
        Me.ManagerUsernameInput.isPassword = False
        Me.ManagerUsernameInput.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.ManagerUsernameInput.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.ManagerUsernameInput.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.ManagerUsernameInput.LineThickness = 2
        Me.ManagerUsernameInput.Location = New System.Drawing.Point(88, 422)
        Me.ManagerUsernameInput.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.ManagerUsernameInput.MaxLength = 32767
        Me.ManagerUsernameInput.Name = "ManagerUsernameInput"
        Me.ManagerUsernameInput.Size = New System.Drawing.Size(412, 58)
        Me.ManagerUsernameInput.TabIndex = 9
        Me.ManagerUsernameInput.Text = "Manager Login"
        Me.ManagerUsernameInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'ManagerPasswordInput
        '
        Me.ManagerPasswordInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.ManagerPasswordInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.ManagerPasswordInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.ManagerPasswordInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ManagerPasswordInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.ManagerPasswordInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ManagerPasswordInput.HintForeColor = System.Drawing.Color.Empty
        Me.ManagerPasswordInput.HintText = ""
        Me.ManagerPasswordInput.isPassword = True
        Me.ManagerPasswordInput.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.ManagerPasswordInput.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.ManagerPasswordInput.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.ManagerPasswordInput.LineThickness = 2
        Me.ManagerPasswordInput.Location = New System.Drawing.Point(88, 568)
        Me.ManagerPasswordInput.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.ManagerPasswordInput.MaxLength = 32767
        Me.ManagerPasswordInput.Name = "ManagerPasswordInput"
        Me.ManagerPasswordInput.Size = New System.Drawing.Size(412, 58)
        Me.ManagerPasswordInput.TabIndex = 8
        Me.ManagerPasswordInput.Text = "BunifuMaterialTextbox1"
        Me.ManagerPasswordInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'LoginAttemptNotif
        '
        Me.LoginAttemptNotif.AutoSize = True
        Me.LoginAttemptNotif.ForeColor = System.Drawing.Color.Red
        Me.LoginAttemptNotif.Location = New System.Drawing.Point(93, 828)
        Me.LoginAttemptNotif.Name = "LoginAttemptNotif"
        Me.LoginAttemptNotif.Size = New System.Drawing.Size(0, 20)
        Me.LoginAttemptNotif.TabIndex = 7
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(213, 75)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(176, 169)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 6
        Me.PictureBox2.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(152, 297)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(288, 32)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "MANAGER CASHIER"
        '
        'ManagerLoginComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Name = "ManagerLoginComponent"
        Me.Size = New System.Drawing.Size(592, 966)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Panel1 As Panel
    Friend WithEvents ManagerLoginBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ManagerUsernameInput As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents ManagerPasswordInput As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents LoginAttemptNotif As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label4 As Label
End Class
