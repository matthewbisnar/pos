﻿

Public Class additemPopup
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private modules As New Api.utilities

    Private Sub additemPopup_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        LoadCategories()
    End Sub

    Private Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click
        Me.clearAllInputs()
        MyBase.Close()
    End Sub

    Private Sub AddItemtolist_Click(sender As Object, e As EventArgs) Handles AddItemtolist.Click
        Dim itemprice As Double
        Dim discountitemprice As Double
        Dim qntity As Double
        Dim DateData As String = String.Format("{0:dd/MM/yyyy - hh:mm:ss tt}", DateTime.Now)
        Dim selectedComboItemType As String

        If productid.Text <> String.Empty Then
            If (ProductNameInput.Text <> String.Empty Or ProductNameInput.Text = "Product Name") And
                (ProductBrandInput.Text <> String.Empty Or ProductBrandInput.Text = "Product Brand") And
                (descriptionInput.Text <> String.Empty Or descriptionInput.Text = "Specifications") Then

                If CategoryComboBox.Text <> String.Empty Then

                    If itemQntity.Text = String.Empty Then
                        itemQntity.Text = 0
                    End If

                    If itemPriceInput.Text = String.Empty Or itemPriceInput.Text = "Price" Then
                        itemPriceInput.Text = 0
                    End If

                    If DiscountIInput.Text = String.Empty Or DiscountIInput.Text = "Discount" Then
                        DiscountIInput.Text = 0
                    End If

                    If TypecomboBox.Text = String.Empty Then
                        selectedComboItemType = "Brand New"
                    Else
                        selectedComboItemType = If(TypecomboBox.SelectedIndex = 1, "Second Hand", "Brand New")
                    End If

                    If Double.TryParse(itemPriceInput.Text, itemprice) And Double.TryParse(DiscountIInput.Text, discountitemprice) And Double.TryParse(itemQntity.Text, qntity) Then

                        If CType(DiscountIInput.Text, Integer) <= 100 Then

                            With dbConfig
                                .Process_qry("INSERT INTO items (itemid, itemname, description, itemType, itemcategory, brand, price, discount, createdat) VALUES (?,?,?,?,?,?,?,?,?)",
                           productid.Text, ProductNameInput.Text, descriptionInput.Text, selectedComboItemType, CategoryComboBox.Text, ProductBrandInput.Text, itemPriceInput.Text, DiscountIInput.Text, DateData)
                                .Process_qry("INSERT INTO itemQuantity (itemIdForeign, Qnty) VALUES (?,?)", productid.Text, itemQntity.Text)
                            End With

                            systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", ManagerDashboard.manageridinstance.managerid},
                                {"description", productid.Text & " Item Successfully added."},
                                {"type", "success"},
                                {"position", "manager"}
                            })
                            If MessageBox.Show("Item Successfully added!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                                Me.clearAllInputs()
                                ManagerDashboard.loadItemList()
                                MyBase.Close()
                            End If
                        Else
                            MessageBox.Show("Exceeded maximum discount input!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        End If
                    Else
                        MessageBox.Show("Quantity, Price and Discount Inputs must be a number!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        itemPriceInput.Text = 0
                        DiscountIInput.Text = 0
                    End If
                Else
                    MessageBox.Show("Please Select Category", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("Input fields must not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Product id must not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub GenerateId_Click(sender As Object, e As EventArgs) Handles GenerateId.Click
        productid.Text = modules.characterGenerator()
    End Sub

    Private Sub ProductNameInput_MouseEnter(sender As Object, e As EventArgs) Handles ProductNameInput.MouseEnter
        If ProductNameInput.Text.ToLower() = "product name" Then
            ProductNameInput.Text = String.Empty
        End If
    End Sub

    Private Sub ProductNameInput_MouseLeave(sender As Object, e As EventArgs) Handles ProductNameInput.MouseLeave
        If ProductNameInput.Text = String.Empty Then
            ProductNameInput.Text = "Product Name"
        End If
    End Sub

    Private Sub ProductBrandInput_MouseEnter(sender As Object, e As EventArgs) Handles ProductBrandInput.MouseEnter
        If ProductBrandInput.Text.ToLower() = "product brand" Then
            ProductBrandInput.Text = String.Empty
        End If
    End Sub

    Private Sub ProductBrandInput_MouseLeave(sender As Object, e As EventArgs) Handles ProductBrandInput.MouseLeave
        If ProductBrandInput.Text = String.Empty Then
            ProductBrandInput.Text = "Product Brand"
        End If
    End Sub

    Private Sub itemPriceInput_MouseEnter(sender As Object, e As EventArgs) Handles itemPriceInput.MouseEnter
        If itemPriceInput.Text.ToLower() = "price" Then
            itemPriceInput.Text = String.Empty
        End If
    End Sub

    Private Sub itemPriceInput_MouseLeave(sender As Object, e As EventArgs) Handles itemPriceInput.MouseLeave
        If itemPriceInput.Text = String.Empty Then
            itemPriceInput.Text = "Price"
        End If
    End Sub

    Private Sub DiscountIInput_MouseEnter(sender As Object, e As EventArgs) Handles DiscountIInput.MouseEnter
        If DiscountIInput.Text.ToLower() = "discount" Then
            DiscountIInput.Text = String.Empty
        End If
    End Sub

    Private Sub DiscountIInput_MouseLeave(sender As Object, e As EventArgs) Handles DiscountIInput.MouseLeave
        If DiscountIInput.Text = String.Empty Then
            DiscountIInput.Text = "Discount"
        End If
    End Sub

    Private Sub descriptionInput_MouseEnter(sender As Object, e As EventArgs) Handles descriptionInput.MouseEnter
        If descriptionInput.Text.ToLower() = "specifications" Then
            descriptionInput.Text = String.Empty
        End If
    End Sub

    Private Sub descriptionInput_MouseLeave(sender As Object, e As EventArgs) Handles descriptionInput.MouseLeave
        If descriptionInput.Text = String.Empty Then
            descriptionInput.Text = "Specifications"
        End If
    End Sub

    Private Sub itemQntity_MouseEnter(sender As Object, e As EventArgs) Handles itemQntity.MouseEnter
        If itemQntity.Text.ToLower() = "quantity" Then
            itemQntity.Text = String.Empty
        End If
    End Sub

    Private Sub itemQntity_MouseLeave(sender As Object, e As EventArgs) Handles itemQntity.MouseLeave
        If itemQntity.Text = String.Empty Then
            itemQntity.Text = "Quantity"
        End If
    End Sub

    Private Sub clearAllInputs()
        productid.Text = String.Empty
        ProductNameInput.Text = "Product Name"
        ProductBrandInput.Text = "Product Brand"
        itemPriceInput.Text = "price"
        DiscountIInput.Text = "Discount"
        descriptionInput.Text = "Specifications"
        itemQntity.Text = "Quantity"
    End Sub

    Private Sub ClearInputField_Click(sender As Object, e As EventArgs) Handles ClearInputField.Click
        Me.clearAllInputs()
    End Sub

    Private Sub LoadCategories()
        Dim loadCategories As Object = dbConfig.Process_qry("SELECT * FROM itemcategory")

        If loadCategories.tables(0).Rows.Count > 0 Then
            For Each categories As DataRow In loadCategories.tables(0).Rows
                CategoryComboBox.Items.Add(categories("category"))
            Next
        End If
    End Sub
End Class