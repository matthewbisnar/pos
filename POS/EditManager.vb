﻿Public Class EditManager

    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Public Structure EditManager
        Dim ManagerId As String
        Dim firstname As String
        Dim lastname As String
        Dim position As String
        Dim managerPassword As String
        Dim manaegerConfirmPassword As String
    End Structure

    Public EditManagerInstance As New EditManager

    Private Sub EditManager_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim loadManager As Object = dbConfig.Process_qry("SELECT * FROM manager WHERE managerid = ?", EditManagerInstance.ManagerId)

        If loadManager.tables(0).rows.count > 0 Then
            For Each fetchManager As DataRow In loadManager.tables(0).rows
                EditManagerInstance.ManagerId = fetchManager("managerid")
                EditManagerInstance.firstname = fetchManager("firstname")
                EditManagerInstance.lastname = fetchManager("lastname")
                EditManagerInstance.position = fetchManager("position")
            Next

            EditCashierId.ReadOnly = True
            EditCashierPosition.ReadOnly = True
            EditCashierId.Text = EditManagerInstance.ManagerId
            EditCashierFirstname.Text = EditManagerInstance.firstname
            EditCashierLastname.Text = EditManagerInstance.lastname
            EditCashierPosition.Text = EditManagerInstance.position
        End If
    End Sub

    Private Sub CancelBtn_Click(sender As Object, e As EventArgs) Handles CancelBtn.Click
        MyBase.Close()
    End Sub

    Private Sub EditManagerBtn_Click(sender As Object, e As EventArgs) Handles EditCashierBtn.Click
        If EditCashierFirstname.Text <> String.Empty And EditCashierLastname.Text <> String.Empty Then

            EditManagerInstance.firstname = EditCashierFirstname.Text
            EditManagerInstance.lastname = EditCashierLastname.Text

            With dbConfig
                .Process_qry("UPDATE manager SET firstname = ?, lastname = ? WHERE managerid = ?",
                EditManagerInstance.firstname, EditManagerInstance.lastname, EditManagerInstance.ManagerId
            )
            End With

            systemlogs.loginlog(New Dictionary(Of String, String) From {
                {"LoginId", EditManagerInstance.ManagerId},
                {"description", "Account Successfully updated!"},
                {"type", "success"},
                {"position", "manager"}
                })

            If MessageBox.Show("Account Successfully Edited.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                MyBase.Close()
            End If
        Else
            MessageBox.Show("Please Input firstname and lastname!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CancelBtn1_Click(sender As Object, e As EventArgs) Handles CancelBtn1.Click
        MyBase.Show()
    End Sub

    Private Sub Changepassword_Click(sender As Object, e As EventArgs) Handles ChangeCashierpassword.Click
        If ManagerChangePassword.Text <> String.Empty And ConfirmChangePassword.Text <> String.Empty Then
            If ManagerChangePassword.Text = ConfirmChangePassword.Text Then

                With dbConfig
                    .Process_qry("UPDATE manager SET managerpassword = ? WHERE managerid = ?", util.setHash(ManagerChangePassword.Text), EditManagerInstance.ManagerId)
                End With

                ManagerChangePassword.Text = String.Empty
                ConfirmChangePassword.Text = String.Empty

                systemlogs.loginlog(New Dictionary(Of String, String) From {
                    {"LoginId", EditManagerInstance.ManagerId},
                    {"description", "Password Successfully updated!"},
                    {"type", "success"},
                    {"position", "manager"}
                })

                If MessageBox.Show("Password Successfully Updated.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    MyBase.Close()
                End If
            Else
                MessageBox.Show("Change password and Confirm password does not match.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Please Input Change password and Confirm password", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

End Class