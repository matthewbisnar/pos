﻿Public Class SoftwareAdminSettingsComponent

    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private modules As New Api.utilities

    Private Sub SoftwareAdminSettingsComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim adminid As Object = dbConfig.Process_qry("SELECT * FROM softwareadmin WHERE uniqueid = ?", My.Settings.softwareid)

        If adminid.tables(0).rows.count > 0 Then
            For Each admin As DataRow In adminid.tables(0).rows
                softwareAdminIdInput.Text = admin("adminid")
            Next
        Else
            If MessageBox.Show("Database does not load, press ok to restart the application.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error) = DialogResult.OK Then
                Application.Restart()
            End If
        End If
        softwareAdminIdInput.ReadOnly = True
    End Sub

    Private Sub ChangePassword_Click(sender As Object, e As EventArgs) Handles ChangePassword.Click
        If softwareAdminIdInput.Text <> String.Empty Then
            If SoftwareAdminPasswordChangeInput.Text <> String.Empty Or SoftwareAdminConfirmPassword.Text <> String.Empty Then
                If SoftwareAdminPasswordChangeInput.Text = SoftwareAdminConfirmPassword.Text Then

                    With dbConfig
                        .Process_qry("UPDATE softwareadmin SET adminid = ?, adminpassword = ? WHERE uniqueid = ? ", softwareAdminIdInput.Text, util.setHash(SoftwareAdminPasswordChangeInput.Text), My.Settings.softwareid)
                        If MessageBox.Show("Account successfully updated. Press ok to restart the application. ", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                            Application.Restart()
                        End If
                    End With

                Else
                    MessageBox.Show("Confirm Password does not match.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End If
            Else
                MessageBox.Show("Password input and confirm password input must not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("ID input must not be empty.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub ResetPasswordDefault_Click(sender As Object, e As EventArgs) Handles ResetPasswordDefault.Click
        With dbConfig
            .Process_qry("UPDATE softwareadmin SET adminid = ?, adminname = ?, adminpassword = ? WHERE uniqueid = ?", "20162762", My.Settings.softwareadminname, util.setHash("root"), My.Settings.softwareid)

            If MessageBox.Show("Account successfully Reset to Default. Press ok to restart the application. ", "Succes", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                Application.Restart()
            End If
        End With
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles EnableChangeIdBtn.Click
        softwareAdminIdInput.ReadOnly = False
    End Sub

    Private Sub BuckupDatabaseBtn_Click(sender As Object, e As EventArgs) Handles BuckupDatabaseBtn.Click
        modules.backupDatabase()
    End Sub
End Class
