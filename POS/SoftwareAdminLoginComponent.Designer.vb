﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SoftwareAdminLoginComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SoftwareAdminLoginComponent))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SoftwareAdminBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.SoftwareAdminInput = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.SoftwareAdminPassword = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.LoginAttemptNotif = New System.Windows.Forms.Label()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.Color.Lavender
        Me.Panel1.Controls.Add(Me.SoftwareAdminBtn)
        Me.Panel1.Controls.Add(Me.SoftwareAdminInput)
        Me.Panel1.Controls.Add(Me.SoftwareAdminPassword)
        Me.Panel1.Controls.Add(Me.LoginAttemptNotif)
        Me.Panel1.Controls.Add(Me.PictureBox2)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(592, 966)
        Me.Panel1.TabIndex = 10
        '
        'SoftwareAdminBtn
        '
        Me.SoftwareAdminBtn.Active = False
        Me.SoftwareAdminBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SoftwareAdminBtn.BorderRadius = 0
        Me.SoftwareAdminBtn.ButtonText = "Admin Login"
        Me.SoftwareAdminBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SoftwareAdminBtn.DisabledColor = System.Drawing.Color.Gray
        Me.SoftwareAdminBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.SoftwareAdminBtn.Iconimage = CType(resources.GetObject("SoftwareAdminBtn.Iconimage"), System.Drawing.Image)
        Me.SoftwareAdminBtn.Iconimage_right = Nothing
        Me.SoftwareAdminBtn.Iconimage_right_Selected = Nothing
        Me.SoftwareAdminBtn.Iconimage_Selected = Nothing
        Me.SoftwareAdminBtn.IconMarginLeft = 0
        Me.SoftwareAdminBtn.IconMarginRight = 0
        Me.SoftwareAdminBtn.IconRightVisible = True
        Me.SoftwareAdminBtn.IconRightZoom = 0R
        Me.SoftwareAdminBtn.IconVisible = True
        Me.SoftwareAdminBtn.IconZoom = 90.0R
        Me.SoftwareAdminBtn.IsTab = False
        Me.SoftwareAdminBtn.Location = New System.Drawing.Point(88, 725)
        Me.SoftwareAdminBtn.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.SoftwareAdminBtn.Name = "SoftwareAdminBtn"
        Me.SoftwareAdminBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SoftwareAdminBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.SoftwareAdminBtn.selected = False
        Me.SoftwareAdminBtn.Size = New System.Drawing.Size(412, 74)
        Me.SoftwareAdminBtn.TabIndex = 10
        Me.SoftwareAdminBtn.Text = "Admin Login"
        Me.SoftwareAdminBtn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.SoftwareAdminBtn.Textcolor = System.Drawing.Color.White
        Me.SoftwareAdminBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'SoftwareAdminInput
        '
        Me.SoftwareAdminInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.SoftwareAdminInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.SoftwareAdminInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.SoftwareAdminInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SoftwareAdminInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.SoftwareAdminInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SoftwareAdminInput.HintForeColor = System.Drawing.Color.Empty
        Me.SoftwareAdminInput.HintText = ""
        Me.SoftwareAdminInput.isPassword = False
        Me.SoftwareAdminInput.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.SoftwareAdminInput.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.SoftwareAdminInput.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.SoftwareAdminInput.LineThickness = 2
        Me.SoftwareAdminInput.Location = New System.Drawing.Point(88, 422)
        Me.SoftwareAdminInput.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.SoftwareAdminInput.MaxLength = 32767
        Me.SoftwareAdminInput.Name = "SoftwareAdminInput"
        Me.SoftwareAdminInput.Size = New System.Drawing.Size(412, 58)
        Me.SoftwareAdminInput.TabIndex = 9
        Me.SoftwareAdminInput.Text = "Admin Login"
        Me.SoftwareAdminInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'SoftwareAdminPassword
        '
        Me.SoftwareAdminPassword.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.SoftwareAdminPassword.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.SoftwareAdminPassword.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.SoftwareAdminPassword.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.SoftwareAdminPassword.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.SoftwareAdminPassword.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.SoftwareAdminPassword.HintForeColor = System.Drawing.Color.Empty
        Me.SoftwareAdminPassword.HintText = ""
        Me.SoftwareAdminPassword.isPassword = True
        Me.SoftwareAdminPassword.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.SoftwareAdminPassword.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.SoftwareAdminPassword.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.SoftwareAdminPassword.LineThickness = 2
        Me.SoftwareAdminPassword.Location = New System.Drawing.Point(88, 568)
        Me.SoftwareAdminPassword.Margin = New System.Windows.Forms.Padding(6, 6, 6, 6)
        Me.SoftwareAdminPassword.MaxLength = 32767
        Me.SoftwareAdminPassword.Name = "SoftwareAdminPassword"
        Me.SoftwareAdminPassword.Size = New System.Drawing.Size(412, 58)
        Me.SoftwareAdminPassword.TabIndex = 8
        Me.SoftwareAdminPassword.Text = "BunifuMaterialTextbox1"
        Me.SoftwareAdminPassword.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'LoginAttemptNotif
        '
        Me.LoginAttemptNotif.AutoSize = True
        Me.LoginAttemptNotif.ForeColor = System.Drawing.Color.Red
        Me.LoginAttemptNotif.Location = New System.Drawing.Point(93, 828)
        Me.LoginAttemptNotif.Name = "LoginAttemptNotif"
        Me.LoginAttemptNotif.Size = New System.Drawing.Size(0, 20)
        Me.LoginAttemptNotif.TabIndex = 7
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(213, 75)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(176, 169)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 6
        Me.PictureBox2.TabStop = False
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(182, 297)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(235, 32)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "ADMIN CASHIER"
        '
        'SoftwareAdminLoginComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.Controls.Add(Me.Panel1)
        Me.Name = "SoftwareAdminLoginComponent"
        Me.Size = New System.Drawing.Size(592, 966)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents SoftwareAdminBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SoftwareAdminInput As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents SoftwareAdminPassword As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents LoginAttemptNotif As Label
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents Label4 As Label
End Class
