﻿'Title : Dynamic utiliity and encryption class
'Author : Matthew Bisnar
'Version : 1.0

Imports System
Imports System.IO
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Security.Cryptography
Imports System.Drawing


Namespace Api

    'ONE WAY HASHING CLASS.
    Public Class md5Encryption

        Private md5Hash As MD5 = MD5.Create()
        Private EncryptData As String = Nothing

        Public Function setHash(ByVal dataHash As String) As String

            Dim dataByte As Byte() = md5Hash.ComputeHash(UTF8Encoding.UTF8.GetBytes(dataHash))
            Dim sBuilder As New StringBuilder

            For i As Integer = 0 To dataByte.Length - 1
                sBuilder.Append(dataByte(i).ToString("x2"))
            Next

            Me.EncryptData = sBuilder.ToString()
            Return Me.EncryptData
        End Function

        Public Function verifyHash(ByVal inputHash As String) As Boolean

            If EncryptData IsNot Nothing Then
                Dim compareData As StringComparer = StringComparer.OrdinalIgnoreCase

                If 0 = compareData.Compare(EncryptData, inputHash) Then
                    Return True
                Else
                    Return False
                End If
            Else
                Return False
            End If
        End Function
    End Class

    Public Class utilities

        'OVAL SHAPE PICTUREBOX
        Public Function ovalImage(ByVal pics As PictureBox) As Object

            Dim gp As Drawing2D.GraphicsPath = New Drawing2D.GraphicsPath()
            gp.AddEllipse(0, 0, pics.Width - 3, pics.Height - 3)

            Dim rg As Region = New Region(gp)
            pics.Region = rg

            Return pics.Region
        End Function

        Public Sub exportcsv(ByVal listViewItems As ListView, ByVal filenameparam As String, ByVal subpath As String)

            Dim directoryPath As String = My.Application.Info.DirectoryPath & "\exports\" & subpath
            Dim strDate As String = "-" & String.Format("{0:dd-MM-yyyy - hhmmss}", DateTime.Now)
            Dim Exportfilename As String = filenameparam & strDate
            Dim csvfile As String = String.Empty

            If Not Directory.Exists(directoryPath) Then
                Directory.CreateDirectory(directoryPath)
            End If

            If listViewItems.Items.Count > 0 Then

                'Column Header
                For i As Integer = 0 To listViewItems.Columns.Count - 1
                    csvfile += listViewItems.Columns(i).Text.ToString() & ","
                Next

                csvfile += vbCr & vbLf

                'Sub Items..
                For Each listviewExport As ListViewItem In listViewItems.Items
                    For i As Integer = 0 To listViewItems.Columns.Count - 1
                        csvfile += listviewExport.SubItems(i).Text.ToString() & ","
                    Next

                    csvfile += vbCr & vbLf
                Next

                Try
                    If Not File.Exists(directoryPath & Exportfilename & ".csv") Then
                        File.AppendAllText(directoryPath & Exportfilename & ".csv", csvfile)
                        MessageBox.Show("File Successfully Exported to " & Exportfilename, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else

                        Dim openfile As String = File.ReadAllText(directoryPath & Exportfilename & ".csv")
                        Dim openfileArray() As String = openfile.Split(New String() {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
                        Dim ExistingCsvFileArray() As String = csvfile.Split(New String() {Environment.NewLine}, StringSplitOptions.RemoveEmptyEntries)
                        Dim result = openfileArray.Union(ExistingCsvFileArray).Distinct().ToArray()

                        Dim appendNewCsv As String = String.Empty

                        For i As Integer = 0 To result.Length - 1
                            appendNewCsv += result(i) & vbCr & vbLf
                        Next

                        File.WriteAllText(directoryPath & Exportfilename & ".csv", "")
                        File.WriteAllText(directoryPath & Exportfilename & ".csv", appendNewCsv.ToString())
                        MessageBox.Show("File Successfully Exported to " & Exportfilename, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

                    End If
                Catch ex As Exception
                    MessageBox.Show(ex.Message())
                End Try
            Else
                MessageBox.Show("List items must not be empty", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        End Sub

        Public Sub backupDatabase()

            Dim strDate As String = Date.Now().ToString("ddMMMyyyy")
            Dim DateData As String = String.Format("{0:dd-MM-yyyy - hhmmss}", DateTime.Now)
            Dim directoryPath As String = My.Application.Info.DirectoryPath & "\exports\backup\database\" & DateData & "\"

            If Not Directory.Exists(directoryPath) Then

                Directory.CreateDirectory(directoryPath)

                Try
                    File.Copy(
                    My.Application.Info.DirectoryPath & "\posdatabase.accdb",
                    directoryPath & "\posdatabase.accdb", True)

                    MessageBox.Show("Database Successfully backup to " & directoryPath, "Success", MessageBoxButtons.OK, MessageBoxIcon.Information)

                Catch ex As Exception
                    MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

            End If

        End Sub

        Public Function characterGenerator() As String
            Return String.Format("{0:ddMMyyyy hhmmss}", DateTime.Now).Replace(" ", "")
        End Function

        Public Function characterGeneratorBackup() As String
            Dim charsData As String = "abcdefghijklmnopqrstuvwxyz1234567890"
            Dim rand As New Random
            Dim output As String = String.Empty

            For i As Integer = 0 To 8
                output += charsData.ToArray(rand.Next(0, charsData.Length - 1))
            Next

            Return output.ToString().ToUpper()
        End Function

        Public Sub getPathBulkUpload()

            Dim openFileDialog1 As New OpenFileDialog()
            Dim imageFormat As String = "Jpg (*.jpg)|*.jpg| Jpeg (*.jpeg)|*jpeg |Bitmap files (*.bmp)|*.bmp"
            Dim textFormat As String = "txt files (*.txt)|*.txt|All files (*.*)|*.*"

            Try

                openFileDialog1.InitialDirectory = My.Computer.FileSystem.SpecialDirectories.MyDocuments
                openFileDialog1.Title = "File Upload"
                openFileDialog1.Filter = textFormat
                openFileDialog1.FilterIndex = 1
                openFileDialog1.RestoreDirectory = True

                If openFileDialog1.ShowDialog() = Windows.Forms.DialogResult.OK Then
                    Dim readAsString As String = File.ReadAllText(openFileDialog1.FileName)
                    MessageBox.Show(readAsString)
                End If
            Catch ex As Exception
                MessageBox.Show(ex.Message, "error")
            End Try

        End Sub
    End Class
End Namespace
