﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Addmanager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Confirmpassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ManagerPassword = New System.Windows.Forms.TextBox()
        Me.CancelBtn = New System.Windows.Forms.Button()
        Me.AddMangerBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Lastname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MangerFirstname = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.AddManagerId = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.Confirmpassword)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.ManagerPassword)
        Me.Panel1.Controls.Add(Me.CancelBtn)
        Me.Panel1.Controls.Add(Me.AddMangerBtn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.Lastname)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.MangerFirstname)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.AddManagerId)
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(693, 966)
        Me.Panel1.TabIndex = 0
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(92, 664)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(210, 29)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Confirm Password"
        '
        'Confirmpassword
        '
        Me.Confirmpassword.Location = New System.Drawing.Point(97, 716)
        Me.Confirmpassword.Multiline = True
        Me.Confirmpassword.Name = "Confirmpassword"
        Me.Confirmpassword.Size = New System.Drawing.Size(503, 56)
        Me.Confirmpassword.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(92, 536)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 29)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Password"
        '
        'ManagerPassword
        '
        Me.ManagerPassword.Location = New System.Drawing.Point(97, 588)
        Me.ManagerPassword.Multiline = True
        Me.ManagerPassword.Name = "ManagerPassword"
        Me.ManagerPassword.Size = New System.Drawing.Size(503, 56)
        Me.ManagerPassword.TabIndex = 12
        '
        'CancelBtn
        '
        Me.CancelBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelBtn.Location = New System.Drawing.Point(215, 816)
        Me.CancelBtn.Name = "CancelBtn"
        Me.CancelBtn.Size = New System.Drawing.Size(188, 65)
        Me.CancelBtn.TabIndex = 11
        Me.CancelBtn.Text = "Cancel"
        Me.CancelBtn.UseVisualStyleBackColor = True
        '
        'AddMangerBtn
        '
        Me.AddMangerBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddMangerBtn.Location = New System.Drawing.Point(409, 816)
        Me.AddMangerBtn.Name = "AddMangerBtn"
        Me.AddMangerBtn.Size = New System.Drawing.Size(188, 65)
        Me.AddMangerBtn.TabIndex = 10
        Me.AddMangerBtn.Text = "Create"
        Me.AddMangerBtn.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(90, 402)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 29)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Lastname"
        '
        'Lastname
        '
        Me.Lastname.Location = New System.Drawing.Point(95, 454)
        Me.Lastname.Multiline = True
        Me.Lastname.Name = "Lastname"
        Me.Lastname.Size = New System.Drawing.Size(503, 56)
        Me.Lastname.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(90, 274)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 29)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Firstname"
        '
        'MangerFirstname
        '
        Me.MangerFirstname.Location = New System.Drawing.Point(95, 326)
        Me.MangerFirstname.Multiline = True
        Me.MangerFirstname.Name = "MangerFirstname"
        Me.MangerFirstname.Size = New System.Drawing.Size(503, 56)
        Me.MangerFirstname.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(86, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 29)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Admin ID"
        '
        'AddManagerId
        '
        Me.AddManagerId.Location = New System.Drawing.Point(92, 179)
        Me.AddManagerId.MaxLength = 8
        Me.AddManagerId.Multiline = True
        Me.AddManagerId.Name = "AddManagerId"
        Me.AddManagerId.Size = New System.Drawing.Size(503, 56)
        Me.AddManagerId.TabIndex = 4
        '
        'Addmanager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 966)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Addmanager"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Addmanager"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label3 As Label
    Friend WithEvents Lastname As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents MangerFirstname As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents AddManagerId As TextBox
    Friend WithEvents CancelBtn As Button
    Friend WithEvents AddMangerBtn As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents ManagerPassword As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Confirmpassword As TextBox
End Class
