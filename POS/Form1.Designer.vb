﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SoftwareAdmin = New System.Windows.Forms.Button()
        Me.ManagerBtn = New System.Windows.Forms.Button()
        Me.CashierBtn = New System.Windows.Forms.Button()
        Me.CashierLoginComponent1 = New POS.CashierLoginComponent()
        Me.SoftwareAdminLoginComponent1 = New POS.SoftwareAdminLoginComponent()
        Me.ManagerLoginComponent1 = New POS.ManagerLoginComponent()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.SuspendLayout()
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.BackColor = System.Drawing.SystemColors.HotTrack
        Me.SplitContainer1.Panel1.Controls.Add(Me.Label1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.SoftwareAdmin)
        Me.SplitContainer1.Panel1.Controls.Add(Me.ManagerBtn)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CashierBtn)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.ManagerLoginComponent1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.SoftwareAdminLoginComponent1)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CashierLoginComponent1)
        Me.SplitContainer1.Size = New System.Drawing.Size(1446, 969)
        Me.SplitContainer1.SplitterDistance = 852
        Me.SplitContainer1.TabIndex = 0
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label1.Location = New System.Drawing.Point(33, 920)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(89, 20)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Version 1.0"
        '
        'SoftwareAdmin
        '
        Me.SoftwareAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SoftwareAdmin.Location = New System.Drawing.Point(114, 579)
        Me.SoftwareAdmin.Name = "SoftwareAdmin"
        Me.SoftwareAdmin.Size = New System.Drawing.Size(492, 96)
        Me.SoftwareAdmin.TabIndex = 2
        Me.SoftwareAdmin.Text = "Software Admin"
        Me.SoftwareAdmin.UseVisualStyleBackColor = True
        '
        'ManagerBtn
        '
        Me.ManagerBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ManagerBtn.Location = New System.Drawing.Point(114, 427)
        Me.ManagerBtn.Name = "ManagerBtn"
        Me.ManagerBtn.Size = New System.Drawing.Size(492, 96)
        Me.ManagerBtn.TabIndex = 1
        Me.ManagerBtn.Text = "Login Manager"
        Me.ManagerBtn.UseVisualStyleBackColor = True
        '
        'CashierBtn
        '
        Me.CashierBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CashierBtn.Location = New System.Drawing.Point(114, 274)
        Me.CashierBtn.Name = "CashierBtn"
        Me.CashierBtn.Size = New System.Drawing.Size(492, 96)
        Me.CashierBtn.TabIndex = 0
        Me.CashierBtn.Text = "Login Cashier"
        Me.CashierBtn.UseVisualStyleBackColor = True
        '
        'CashierLoginComponent1
        '
        Me.CashierLoginComponent1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierLoginComponent1.Location = New System.Drawing.Point(0, 0)
        Me.CashierLoginComponent1.Name = "CashierLoginComponent1"
        Me.CashierLoginComponent1.Size = New System.Drawing.Size(590, 969)
        Me.CashierLoginComponent1.TabIndex = 0
        '
        'SoftwareAdminLoginComponent1
        '
        Me.SoftwareAdminLoginComponent1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SoftwareAdminLoginComponent1.Location = New System.Drawing.Point(0, 0)
        Me.SoftwareAdminLoginComponent1.Name = "SoftwareAdminLoginComponent1"
        Me.SoftwareAdminLoginComponent1.Size = New System.Drawing.Size(590, 969)
        Me.SoftwareAdminLoginComponent1.TabIndex = 1
        '
        'ManagerLoginComponent1
        '
        Me.ManagerLoginComponent1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.ManagerLoginComponent1.Location = New System.Drawing.Point(0, 0)
        Me.ManagerLoginComponent1.Name = "ManagerLoginComponent1"
        Me.ManagerLoginComponent1.Size = New System.Drawing.Size(590, 969)
        Me.ManagerLoginComponent1.TabIndex = 2
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1446, 969)
        Me.Controls.Add(Me.SplitContainer1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "Login"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Form1"
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.PerformLayout()
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents SoftwareAdmin As Button
    Friend WithEvents ManagerBtn As Button
    Friend WithEvents CashierBtn As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents CashierLoginComponent1 As CashierLoginComponent
    Friend WithEvents SoftwareAdminLoginComponent1 As SoftwareAdminLoginComponent
    Friend WithEvents ManagerLoginComponent1 As ManagerLoginComponent
End Class
