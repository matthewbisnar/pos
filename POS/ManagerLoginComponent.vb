﻿Public Class ManagerLoginComponent
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Private Structure LoginAttempt
        Dim LoginAttempt As Integer
        Dim LoginCounterTimer As Integer
    End Structure

    Dim attempt As New LoginAttempt

    Private Sub ManagerLoginComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        attempt.LoginAttempt = 0
        attempt.LoginCounterTimer = 60
    End Sub

    Private Sub disableInputs()
        ManagerUsernameInput.Text = String.Empty
        ManagerPasswordInput.Text = String.Empty

        ManagerUsernameInput.Enabled = False
        ManagerPasswordInput.Enabled = False
        ManagerLoginBtn.Enabled = False
    End Sub

    Public Sub enabledInput()
        ManagerUsernameInput.Enabled = True
        ManagerPasswordInput.Enabled = True
        ManagerLoginBtn.Enabled = True
        attempt.LoginAttempt = 0
        LoginAttemptNotif.Text = String.Empty
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        attempt.LoginCounterTimer = attempt.LoginCounterTimer - 1
        LoginAttemptNotif.Text = "Login Maximum " & attempt.LoginAttempt & " Attempt Exceeded! " & vbCrLf & "Wait " & attempt.LoginCounterTimer & " seconds to be able to login."

        If attempt.LoginCounterTimer <= 0 Then
            Timer1.Stop()
            Me.enabledInput()
        End If
    End Sub

    Private Sub ManagerLoginBtn_Click_1(sender As Object, e As EventArgs) Handles ManagerLoginBtn.Click
        If (ManagerUsernameInput.Text <> String.Empty Or ManagerUsernameInput.Text.ToLower() = "Manager Login".ToLower()) Or (ManagerPasswordInput.Text <> String.Empty Or ManagerPasswordInput.Text.ToLower() = "BunifuMaterialTextbox1".ToLower()) Then
            With dbConfig
                Dim loginManager As Object = .Process_qry("SELECT * FROM manager WHERE ManagerId = ? AND managerpassword = ?", ManagerUsernameInput.Text, util.setHash(ManagerPasswordInput.Text))

                If loginManager.tables(0).Rows.Count > 0 Then
                    For Each managerPassword As DataRow In loginManager.tables(0).Rows
                        If util.verifyHash(util.setHash(managerPassword("managerpassword"))) = True Then
                            systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", ManagerUsernameInput.Text},
                                {"description", "Account Successfully Login!"},
                                {"type", "success"},
                                {"position", "manager"}
                            })

                            attempt.LoginAttempt = 0
                            attempt.LoginCounterTimer = 30
                            ManagerDashboard.manageridinstance.managerid = ManagerUsernameInput.Text
                            ManagerUsernameInput.Text = String.Empty
                            ManagerPasswordInput.Text = String.Empty
                            Login.Hide()
                            ManagerDashboard.ShowDialog()
                        End If
                    Next
                Else
                    If attempt.LoginAttempt < 3 Then
                        MessageBox.Show("Username and/or Password you Entered is Incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        systemlogs.loginlog(New Dictionary(Of String, String) From {
                            {"LoginId", ManagerUsernameInput.Text},
                            {"description", "Username and/or Password you Entered is Incorrect!"},
                            {"type", "error"},
                            {"position", "manager"}
                        })

                        attempt.LoginAttempt = attempt.LoginAttempt + 1
                        LoginAttemptNotif.Text = "Login Failed " & attempt.LoginAttempt & " attempt."
                        ManagerUsernameInput.Text = String.Empty
                        ManagerPasswordInput.Text = String.Empty
                    Else
                        MessageBox.Show("Login Maximum 3 Attempt Exceeded!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        LoginAttemptNotif.Text = "Login Maximum " & attempt.LoginAttempt & " Attempt Exceeded!"
                        Me.disableInputs()
                        ManagerAuthenticate.ShowDialog()
                        Timer1.Start()
                    End If
                End If
            End With
        Else
            MessageBox.Show("Username and/or Password Must Not be Empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub ManagerUsernameInput_MouseEnter(sender As Object, e As EventArgs) Handles ManagerUsernameInput.MouseEnter
        If ManagerUsernameInput.Text.ToLower() = "Manager Login".ToLower() Then
            ManagerUsernameInput.Text = String.Empty
        End If
    End Sub

    Private Sub ManagerUsernameInput_MouseLeave(sender As Object, e As EventArgs) Handles ManagerUsernameInput.MouseLeave
        If ManagerUsernameInput.Text = String.Empty Then
            ManagerUsernameInput.Text = "Manager Login"
        End If
    End Sub

    Private Sub ManagerPasswordInput_MouseEnter(sender As Object, e As EventArgs) Handles ManagerPasswordInput.MouseEnter
        If ManagerPasswordInput.Text.ToLower() = "BunifuMaterialTextbox1".ToLower() Then
            ManagerPasswordInput.Text = String.Empty
        End If
    End Sub

    Private Sub ManagerPasswordInput_MouseLeave(sender As Object, e As EventArgs) Handles ManagerPasswordInput.MouseLeave
        If ManagerPasswordInput.Text = String.Empty Then
            ManagerPasswordInput.Text = "BunifuMaterialTextbox1"
        End If
    End Sub
End Class
