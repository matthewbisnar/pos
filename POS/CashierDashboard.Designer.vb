﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SS
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SS))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.timezone = New System.Windows.Forms.Label()
        Me.AccountName = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.CashierDashboard = New System.Windows.Forms.Panel()
        Me.BunifuCards3 = New Bunifu.Framework.UI.BunifuCards()
        Me.CompleteTransaction = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ServiceFeeBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Void = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.CashierUsernameInput = New Bunifu.Framework.UI.BunifuMaterialTextbox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BunifuCards2 = New Bunifu.Framework.UI.BunifuCards()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.TotalitemCount = New System.Windows.Forms.Label()
        Me.Totalitms = New System.Windows.Forms.Label()
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.CashierProductList = New System.Windows.Forms.ListView()
        Me.ProductId = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductNames = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductBrand = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductPrice = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.itemDiscount = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductQnty = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ProductSubtotal = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.RemoveItems = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.CashierItemSearch = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.LogoutBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.TransactioLogs = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.CashierItemsBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.DashboardBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.CashierProductComponent1 = New POS.CashierProductComponent()
        Me.CashierTransactionLogsComponent1 = New POS.CashierTransactionLogsComponent()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.CashierDashboard.SuspendLayout()
        Me.BunifuCards3.SuspendLayout()
        Me.BunifuCards2.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.BunifuCards1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(238, 746)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(192, 13)
        Me.Label2.TabIndex = 6
        Me.Label2.Text = "Copyright 2019 POS. All rights reserved"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Timer1
        '
        Me.Timer1.Interval = 1000
        '
        'timezone
        '
        Me.timezone.AutoSize = True
        Me.timezone.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.timezone.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.timezone.Location = New System.Drawing.Point(248, 22)
        Me.timezone.Name = "timezone"
        Me.timezone.Size = New System.Drawing.Size(0, 20)
        Me.timezone.TabIndex = 3
        '
        'AccountName
        '
        Me.AccountName.AutoSize = True
        Me.AccountName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AccountName.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AccountName.Location = New System.Drawing.Point(1429, 22)
        Me.AccountName.Name = "AccountName"
        Me.AccountName.Size = New System.Drawing.Size(138, 20)
        Me.AccountName.TabIndex = 4
        Me.AccountName.Text = "Matthew (Cashier)"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1382, 16)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(39, 33)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.AccountName)
        Me.Panel2.Controls.Add(Me.timezone)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1590, 69)
        Me.Panel2.TabIndex = 1
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 0)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(2)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.CashierDashboard)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CashierProductComponent1)
        Me.SplitContainer1.Panel1.Controls.Add(Me.CashierTransactionLogsComponent1)
        Me.SplitContainer1.Panel1.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.BackColor = System.Drawing.SystemColors.HighlightText
        Me.SplitContainer1.Panel2.Controls.Add(Me.LogoutBtn)
        Me.SplitContainer1.Panel2.Controls.Add(Me.TransactioLogs)
        Me.SplitContainer1.Panel2.Controls.Add(Me.CashierItemsBtn)
        Me.SplitContainer1.Panel2.Controls.Add(Me.DashboardBtn)
        Me.SplitContainer1.Panel2.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.SplitContainer1.Size = New System.Drawing.Size(1590, 728)
        Me.SplitContainer1.SplitterDistance = 1354
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 0
        '
        'CashierDashboard
        '
        Me.CashierDashboard.Controls.Add(Me.BunifuCards3)
        Me.CashierDashboard.Controls.Add(Me.Panel5)
        Me.CashierDashboard.Controls.Add(Me.BunifuCards2)
        Me.CashierDashboard.Controls.Add(Me.BunifuCards1)
        Me.CashierDashboard.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierDashboard.Location = New System.Drawing.Point(0, 0)
        Me.CashierDashboard.Margin = New System.Windows.Forms.Padding(0)
        Me.CashierDashboard.Name = "CashierDashboard"
        Me.CashierDashboard.Size = New System.Drawing.Size(1354, 728)
        Me.CashierDashboard.TabIndex = 4
        '
        'BunifuCards3
        '
        Me.BunifuCards3.BackColor = System.Drawing.Color.White
        Me.BunifuCards3.BorderRadius = 0
        Me.BunifuCards3.BottomSahddow = True
        Me.BunifuCards3.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards3.Controls.Add(Me.CompleteTransaction)
        Me.BunifuCards3.Controls.Add(Me.ServiceFeeBtn)
        Me.BunifuCards3.Controls.Add(Me.Void)
        Me.BunifuCards3.Controls.Add(Me.Label9)
        Me.BunifuCards3.Controls.Add(Me.Label10)
        Me.BunifuCards3.Controls.Add(Me.Label11)
        Me.BunifuCards3.Controls.Add(Me.Label12)
        Me.BunifuCards3.Controls.Add(Me.CashierUsernameInput)
        Me.BunifuCards3.LeftSahddow = False
        Me.BunifuCards3.Location = New System.Drawing.Point(1011, 294)
        Me.BunifuCards3.Name = "BunifuCards3"
        Me.BunifuCards3.RightSahddow = True
        Me.BunifuCards3.ShadowDepth = 20
        Me.BunifuCards3.Size = New System.Drawing.Size(323, 362)
        Me.BunifuCards3.TabIndex = 3
        '
        'CompleteTransaction
        '
        Me.CompleteTransaction.Active = False
        Me.CompleteTransaction.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CompleteTransaction.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CompleteTransaction.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CompleteTransaction.BorderRadius = 0
        Me.CompleteTransaction.ButtonText = "Complete"
        Me.CompleteTransaction.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CompleteTransaction.DisabledColor = System.Drawing.Color.Gray
        Me.CompleteTransaction.Iconcolor = System.Drawing.Color.Transparent
        Me.CompleteTransaction.Iconimage = CType(resources.GetObject("CompleteTransaction.Iconimage"), System.Drawing.Image)
        Me.CompleteTransaction.Iconimage_right = Nothing
        Me.CompleteTransaction.Iconimage_right_Selected = Nothing
        Me.CompleteTransaction.Iconimage_Selected = Nothing
        Me.CompleteTransaction.IconMarginLeft = 0
        Me.CompleteTransaction.IconMarginRight = 0
        Me.CompleteTransaction.IconRightVisible = True
        Me.CompleteTransaction.IconRightZoom = 0R
        Me.CompleteTransaction.IconVisible = True
        Me.CompleteTransaction.IconZoom = 90.0R
        Me.CompleteTransaction.IsTab = False
        Me.CompleteTransaction.Location = New System.Drawing.Point(2, 305)
        Me.CompleteTransaction.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.CompleteTransaction.Name = "CompleteTransaction"
        Me.CompleteTransaction.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CompleteTransaction.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.CompleteTransaction.OnHoverTextColor = System.Drawing.Color.White
        Me.CompleteTransaction.selected = False
        Me.CompleteTransaction.Size = New System.Drawing.Size(319, 54)
        Me.CompleteTransaction.TabIndex = 17
        Me.CompleteTransaction.Text = "Complete"
        Me.CompleteTransaction.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CompleteTransaction.Textcolor = System.Drawing.Color.White
        Me.CompleteTransaction.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ServiceFeeBtn
        '
        Me.ServiceFeeBtn.Active = False
        Me.ServiceFeeBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ServiceFeeBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ServiceFeeBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ServiceFeeBtn.BorderRadius = 0
        Me.ServiceFeeBtn.ButtonText = "Service Fee"
        Me.ServiceFeeBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ServiceFeeBtn.DisabledColor = System.Drawing.Color.Gray
        Me.ServiceFeeBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.ServiceFeeBtn.Iconimage = CType(resources.GetObject("ServiceFeeBtn.Iconimage"), System.Drawing.Image)
        Me.ServiceFeeBtn.Iconimage_right = Nothing
        Me.ServiceFeeBtn.Iconimage_right_Selected = Nothing
        Me.ServiceFeeBtn.Iconimage_Selected = Nothing
        Me.ServiceFeeBtn.IconMarginLeft = 0
        Me.ServiceFeeBtn.IconMarginRight = 0
        Me.ServiceFeeBtn.IconRightVisible = True
        Me.ServiceFeeBtn.IconRightZoom = 0R
        Me.ServiceFeeBtn.IconVisible = True
        Me.ServiceFeeBtn.IconZoom = 90.0R
        Me.ServiceFeeBtn.IsTab = False
        Me.ServiceFeeBtn.Location = New System.Drawing.Point(162, 251)
        Me.ServiceFeeBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ServiceFeeBtn.Name = "ServiceFeeBtn"
        Me.ServiceFeeBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ServiceFeeBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ServiceFeeBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.ServiceFeeBtn.selected = False
        Me.ServiceFeeBtn.Size = New System.Drawing.Size(159, 54)
        Me.ServiceFeeBtn.TabIndex = 16
        Me.ServiceFeeBtn.Text = "Service Fee"
        Me.ServiceFeeBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.ServiceFeeBtn.Textcolor = System.Drawing.Color.White
        Me.ServiceFeeBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Void
        '
        Me.Void.Active = False
        Me.Void.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Void.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Void.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Void.BorderRadius = 0
        Me.Void.ButtonText = "Void"
        Me.Void.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Void.DisabledColor = System.Drawing.Color.Gray
        Me.Void.Iconcolor = System.Drawing.Color.Transparent
        Me.Void.Iconimage = CType(resources.GetObject("Void.Iconimage"), System.Drawing.Image)
        Me.Void.Iconimage_right = Nothing
        Me.Void.Iconimage_right_Selected = Nothing
        Me.Void.Iconimage_Selected = Nothing
        Me.Void.IconMarginLeft = 0
        Me.Void.IconMarginRight = 0
        Me.Void.IconRightVisible = True
        Me.Void.IconRightZoom = 0R
        Me.Void.IconVisible = True
        Me.Void.IconZoom = 90.0R
        Me.Void.IsTab = False
        Me.Void.Location = New System.Drawing.Point(2, 251)
        Me.Void.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Void.Name = "Void"
        Me.Void.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Void.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.Void.OnHoverTextColor = System.Drawing.Color.White
        Me.Void.selected = False
        Me.Void.Size = New System.Drawing.Size(160, 54)
        Me.Void.TabIndex = 15
        Me.Void.Text = "Void"
        Me.Void.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.Void.Textcolor = System.Drawing.Color.White
        Me.Void.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.Location = New System.Drawing.Point(268, 183)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(32, 20)
        Me.Label9.TabIndex = 14
        Me.Label9.Text = "P 0"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label10.Location = New System.Drawing.Point(19, 181)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(65, 20)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Change"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(268, 117)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(32, 20)
        Me.Label11.TabIndex = 12
        Me.Label11.Text = "P 0"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.Location = New System.Drawing.Point(19, 115)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(107, 20)
        Me.Label12.TabIndex = 11
        Me.Label12.Text = "Received Fee"
        '
        'CashierUsernameInput
        '
        Me.CashierUsernameInput.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None
        Me.CashierUsernameInput.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None
        Me.CashierUsernameInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.CashierUsernameInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.CashierUsernameInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.CashierUsernameInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.CashierUsernameInput.HintForeColor = System.Drawing.Color.Empty
        Me.CashierUsernameInput.HintText = ""
        Me.CashierUsernameInput.isPassword = False
        Me.CashierUsernameInput.LineFocusedColor = System.Drawing.Color.SeaGreen
        Me.CashierUsernameInput.LineIdleColor = System.Drawing.Color.SeaGreen
        Me.CashierUsernameInput.LineMouseHoverColor = System.Drawing.Color.SeaGreen
        Me.CashierUsernameInput.LineThickness = 2
        Me.CashierUsernameInput.Location = New System.Drawing.Point(22, 29)
        Me.CashierUsernameInput.Margin = New System.Windows.Forms.Padding(4)
        Me.CashierUsernameInput.MaxLength = 32767
        Me.CashierUsernameInput.Name = "CashierUsernameInput"
        Me.CashierUsernameInput.Size = New System.Drawing.Size(275, 38)
        Me.CashierUsernameInput.TabIndex = 10
        Me.CashierUsernameInput.Text = "Cashier Login"
        Me.CashierUsernameInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 675)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1354, 53)
        Me.Panel5.TabIndex = 2
        '
        'BunifuCards2
        '
        Me.BunifuCards2.BackColor = System.Drawing.Color.White
        Me.BunifuCards2.BorderRadius = 0
        Me.BunifuCards2.BottomSahddow = True
        Me.BunifuCards2.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards2.Controls.Add(Me.Label5)
        Me.BunifuCards2.Controls.Add(Me.Panel6)
        Me.BunifuCards2.Controls.Add(Me.Label3)
        Me.BunifuCards2.Controls.Add(Me.Label4)
        Me.BunifuCards2.Controls.Add(Me.Label1)
        Me.BunifuCards2.Controls.Add(Me.Label7)
        Me.BunifuCards2.Controls.Add(Me.TotalitemCount)
        Me.BunifuCards2.Controls.Add(Me.Totalitms)
        Me.BunifuCards2.LeftSahddow = False
        Me.BunifuCards2.Location = New System.Drawing.Point(1011, 17)
        Me.BunifuCards2.Name = "BunifuCards2"
        Me.BunifuCards2.RightSahddow = True
        Me.BunifuCards2.ShadowDepth = 20
        Me.BunifuCards2.Size = New System.Drawing.Size(323, 271)
        Me.BunifuCards2.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(269, 225)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(32, 20)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "P 0"
        '
        'Panel6
        '
        Me.Panel6.BackColor = System.Drawing.SystemColors.Window
        Me.Panel6.Controls.Add(Me.Label6)
        Me.Panel6.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel6.Location = New System.Drawing.Point(0, 201)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(323, 70)
        Me.Panel6.TabIndex = 16
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(22, 27)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(104, 20)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Total Payable"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(269, 156)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 20)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "P 0"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(20, 154)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(93, 20)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Service Fee"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(269, 90)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(32, 20)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "P 0"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.Location = New System.Drawing.Point(20, 88)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(83, 20)
        Me.Label7.TabIndex = 11
        Me.Label7.Text = "Total Price"
        '
        'TotalitemCount
        '
        Me.TotalitemCount.AutoSize = True
        Me.TotalitemCount.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TotalitemCount.Location = New System.Drawing.Point(269, 26)
        Me.TotalitemCount.Name = "TotalitemCount"
        Me.TotalitemCount.Size = New System.Drawing.Size(32, 20)
        Me.TotalitemCount.TabIndex = 10
        Me.TotalitemCount.Text = "P 0"
        '
        'Totalitms
        '
        Me.Totalitms.AutoSize = True
        Me.Totalitms.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Totalitms.Location = New System.Drawing.Point(20, 24)
        Me.Totalitms.Name = "Totalitms"
        Me.Totalitms.Size = New System.Drawing.Size(88, 20)
        Me.Totalitms.TabIndex = 9
        Me.Totalitms.Text = "Total Items"
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.TableLayoutPanel1)
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(14, 17)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(978, 642)
        Me.BunifuCards1.TabIndex = 0
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 15.26639!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 84.7336!))
        Me.TableLayoutPanel1.Controls.Add(Me.CashierProductList, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel4, 0, 0)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(978, 642)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'CashierProductList
        '
        Me.CashierProductList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ProductId, Me.ProductNames, Me.ProductBrand, Me.ProductPrice, Me.itemDiscount, Me.ProductQnty, Me.ProductSubtotal})
        Me.CashierProductList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierProductList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CashierProductList.FullRowSelect = True
        Me.CashierProductList.GridLines = True
        Me.CashierProductList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.CashierProductList.HideSelection = False
        Me.CashierProductList.Location = New System.Drawing.Point(0, 0)
        Me.CashierProductList.Margin = New System.Windows.Forms.Padding(0)
        Me.CashierProductList.Name = "CashierProductList"
        Me.CashierProductList.Size = New System.Drawing.Size(829, 642)
        Me.CashierProductList.TabIndex = 2
        Me.CashierProductList.UseCompatibleStateImageBehavior = False
        Me.CashierProductList.View = System.Windows.Forms.View.Details
        '
        'ProductId
        '
        Me.ProductId.Text = "Product ID"
        Me.ProductId.Width = 215
        '
        'ProductNames
        '
        Me.ProductNames.Text = "Product Name"
        Me.ProductNames.Width = 281
        '
        'ProductBrand
        '
        Me.ProductBrand.Text = "Brand"
        Me.ProductBrand.Width = 228
        '
        'ProductPrice
        '
        Me.ProductPrice.Text = "Price"
        Me.ProductPrice.Width = 190
        '
        'itemDiscount
        '
        Me.itemDiscount.Text = "Discount"
        Me.itemDiscount.Width = 124
        '
        'ProductQnty
        '
        Me.ProductQnty.Text = "Qnty"
        Me.ProductQnty.Width = 104
        '
        'ProductSubtotal
        '
        Me.ProductSubtotal.Text = "Subtotal"
        Me.ProductSubtotal.Width = 182
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.RemoveItems)
        Me.Panel4.Controls.Add(Me.CashierItemSearch)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(829, 0)
        Me.Panel4.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(149, 642)
        Me.Panel4.TabIndex = 3
        '
        'RemoveItems
        '
        Me.RemoveItems.Active = False
        Me.RemoveItems.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RemoveItems.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RemoveItems.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RemoveItems.BorderRadius = 0
        Me.RemoveItems.ButtonText = "Remove"
        Me.RemoveItems.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RemoveItems.DisabledColor = System.Drawing.Color.Gray
        Me.RemoveItems.Iconcolor = System.Drawing.Color.Transparent
        Me.RemoveItems.Iconimage = CType(resources.GetObject("RemoveItems.Iconimage"), System.Drawing.Image)
        Me.RemoveItems.Iconimage_right = Nothing
        Me.RemoveItems.Iconimage_right_Selected = Nothing
        Me.RemoveItems.Iconimage_Selected = Nothing
        Me.RemoveItems.IconMarginLeft = 0
        Me.RemoveItems.IconMarginRight = 0
        Me.RemoveItems.IconRightVisible = True
        Me.RemoveItems.IconRightZoom = 0R
        Me.RemoveItems.IconVisible = True
        Me.RemoveItems.IconZoom = 90.0R
        Me.RemoveItems.IsTab = False
        Me.RemoveItems.Location = New System.Drawing.Point(-1, 72)
        Me.RemoveItems.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.RemoveItems.Name = "RemoveItems"
        Me.RemoveItems.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RemoveItems.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.RemoveItems.OnHoverTextColor = System.Drawing.Color.White
        Me.RemoveItems.selected = False
        Me.RemoveItems.Size = New System.Drawing.Size(150, 67)
        Me.RemoveItems.TabIndex = 2
        Me.RemoveItems.Text = "Remove"
        Me.RemoveItems.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.RemoveItems.Textcolor = System.Drawing.Color.White
        Me.RemoveItems.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'CashierItemSearch
        '
        Me.CashierItemSearch.Active = False
        Me.CashierItemSearch.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierItemSearch.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierItemSearch.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CashierItemSearch.BorderRadius = 0
        Me.CashierItemSearch.ButtonText = "Search"
        Me.CashierItemSearch.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CashierItemSearch.DisabledColor = System.Drawing.Color.Gray
        Me.CashierItemSearch.Iconcolor = System.Drawing.Color.Transparent
        Me.CashierItemSearch.Iconimage = CType(resources.GetObject("CashierItemSearch.Iconimage"), System.Drawing.Image)
        Me.CashierItemSearch.Iconimage_right = Nothing
        Me.CashierItemSearch.Iconimage_right_Selected = Nothing
        Me.CashierItemSearch.Iconimage_Selected = Nothing
        Me.CashierItemSearch.IconMarginLeft = 0
        Me.CashierItemSearch.IconMarginRight = 0
        Me.CashierItemSearch.IconRightVisible = True
        Me.CashierItemSearch.IconRightZoom = 0R
        Me.CashierItemSearch.IconVisible = True
        Me.CashierItemSearch.IconZoom = 90.0R
        Me.CashierItemSearch.IsTab = False
        Me.CashierItemSearch.Location = New System.Drawing.Point(-1, 5)
        Me.CashierItemSearch.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.CashierItemSearch.Name = "CashierItemSearch"
        Me.CashierItemSearch.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierItemSearch.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.CashierItemSearch.OnHoverTextColor = System.Drawing.Color.White
        Me.CashierItemSearch.selected = False
        Me.CashierItemSearch.Size = New System.Drawing.Size(150, 67)
        Me.CashierItemSearch.TabIndex = 1
        Me.CashierItemSearch.Text = "Search"
        Me.CashierItemSearch.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CashierItemSearch.Textcolor = System.Drawing.Color.White
        Me.CashierItemSearch.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'LogoutBtn
        '
        Me.LogoutBtn.Active = False
        Me.LogoutBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.LogoutBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.LogoutBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.LogoutBtn.BorderRadius = 0
        Me.LogoutBtn.ButtonText = "Logout"
        Me.LogoutBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.LogoutBtn.DisabledColor = System.Drawing.Color.Gray
        Me.LogoutBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.LogoutBtn.Iconimage = CType(resources.GetObject("LogoutBtn.Iconimage"), System.Drawing.Image)
        Me.LogoutBtn.Iconimage_right = Nothing
        Me.LogoutBtn.Iconimage_right_Selected = Nothing
        Me.LogoutBtn.Iconimage_Selected = Nothing
        Me.LogoutBtn.IconMarginLeft = 0
        Me.LogoutBtn.IconMarginRight = 0
        Me.LogoutBtn.IconRightVisible = True
        Me.LogoutBtn.IconRightZoom = 0R
        Me.LogoutBtn.IconVisible = True
        Me.LogoutBtn.IconZoom = 90.0R
        Me.LogoutBtn.IsTab = False
        Me.LogoutBtn.Location = New System.Drawing.Point(-3, 202)
        Me.LogoutBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.LogoutBtn.Name = "LogoutBtn"
        Me.LogoutBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.LogoutBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.LogoutBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.LogoutBtn.selected = False
        Me.LogoutBtn.Size = New System.Drawing.Size(237, 67)
        Me.LogoutBtn.TabIndex = 3
        Me.LogoutBtn.Text = "Logout"
        Me.LogoutBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.LogoutBtn.Textcolor = System.Drawing.Color.White
        Me.LogoutBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'TransactioLogs
        '
        Me.TransactioLogs.Active = False
        Me.TransactioLogs.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.TransactioLogs.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.TransactioLogs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TransactioLogs.BorderRadius = 0
        Me.TransactioLogs.ButtonText = "Transaction History"
        Me.TransactioLogs.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TransactioLogs.DisabledColor = System.Drawing.Color.Gray
        Me.TransactioLogs.Iconcolor = System.Drawing.Color.Transparent
        Me.TransactioLogs.Iconimage = CType(resources.GetObject("TransactioLogs.Iconimage"), System.Drawing.Image)
        Me.TransactioLogs.Iconimage_right = Nothing
        Me.TransactioLogs.Iconimage_right_Selected = Nothing
        Me.TransactioLogs.Iconimage_Selected = Nothing
        Me.TransactioLogs.IconMarginLeft = 0
        Me.TransactioLogs.IconMarginRight = 0
        Me.TransactioLogs.IconRightVisible = True
        Me.TransactioLogs.IconRightZoom = 0R
        Me.TransactioLogs.IconVisible = True
        Me.TransactioLogs.IconZoom = 90.0R
        Me.TransactioLogs.IsTab = False
        Me.TransactioLogs.Location = New System.Drawing.Point(-3, 135)
        Me.TransactioLogs.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TransactioLogs.Name = "TransactioLogs"
        Me.TransactioLogs.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.TransactioLogs.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.TransactioLogs.OnHoverTextColor = System.Drawing.Color.White
        Me.TransactioLogs.selected = False
        Me.TransactioLogs.Size = New System.Drawing.Size(237, 67)
        Me.TransactioLogs.TabIndex = 2
        Me.TransactioLogs.Text = "Transaction History"
        Me.TransactioLogs.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.TransactioLogs.Textcolor = System.Drawing.Color.White
        Me.TransactioLogs.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'CashierItemsBtn
        '
        Me.CashierItemsBtn.Active = False
        Me.CashierItemsBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierItemsBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierItemsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.CashierItemsBtn.BorderRadius = 0
        Me.CashierItemsBtn.ButtonText = "Items"
        Me.CashierItemsBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.CashierItemsBtn.DisabledColor = System.Drawing.Color.Gray
        Me.CashierItemsBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.CashierItemsBtn.Iconimage = CType(resources.GetObject("CashierItemsBtn.Iconimage"), System.Drawing.Image)
        Me.CashierItemsBtn.Iconimage_right = Nothing
        Me.CashierItemsBtn.Iconimage_right_Selected = Nothing
        Me.CashierItemsBtn.Iconimage_Selected = Nothing
        Me.CashierItemsBtn.IconMarginLeft = 0
        Me.CashierItemsBtn.IconMarginRight = 0
        Me.CashierItemsBtn.IconRightVisible = True
        Me.CashierItemsBtn.IconRightZoom = 0R
        Me.CashierItemsBtn.IconVisible = True
        Me.CashierItemsBtn.IconZoom = 90.0R
        Me.CashierItemsBtn.IsTab = False
        Me.CashierItemsBtn.Location = New System.Drawing.Point(-3, 68)
        Me.CashierItemsBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.CashierItemsBtn.Name = "CashierItemsBtn"
        Me.CashierItemsBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.CashierItemsBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.CashierItemsBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.CashierItemsBtn.selected = False
        Me.CashierItemsBtn.Size = New System.Drawing.Size(237, 67)
        Me.CashierItemsBtn.TabIndex = 1
        Me.CashierItemsBtn.Text = "Items"
        Me.CashierItemsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.CashierItemsBtn.Textcolor = System.Drawing.Color.White
        Me.CashierItemsBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'DashboardBtn
        '
        Me.DashboardBtn.Active = False
        Me.DashboardBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DashboardBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DashboardBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DashboardBtn.BorderRadius = 0
        Me.DashboardBtn.ButtonText = "Dashboard"
        Me.DashboardBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DashboardBtn.DisabledColor = System.Drawing.Color.Gray
        Me.DashboardBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.DashboardBtn.Iconimage = CType(resources.GetObject("DashboardBtn.Iconimage"), System.Drawing.Image)
        Me.DashboardBtn.Iconimage_right = Nothing
        Me.DashboardBtn.Iconimage_right_Selected = Nothing
        Me.DashboardBtn.Iconimage_Selected = Nothing
        Me.DashboardBtn.IconMarginLeft = 0
        Me.DashboardBtn.IconMarginRight = 0
        Me.DashboardBtn.IconRightVisible = True
        Me.DashboardBtn.IconRightZoom = 0R
        Me.DashboardBtn.IconVisible = True
        Me.DashboardBtn.IconZoom = 90.0R
        Me.DashboardBtn.IsTab = False
        Me.DashboardBtn.Location = New System.Drawing.Point(0, 1)
        Me.DashboardBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.DashboardBtn.Name = "DashboardBtn"
        Me.DashboardBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DashboardBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.DashboardBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.DashboardBtn.selected = False
        Me.DashboardBtn.Size = New System.Drawing.Size(234, 67)
        Me.DashboardBtn.TabIndex = 0
        Me.DashboardBtn.Text = "Dashboard"
        Me.DashboardBtn.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.DashboardBtn.Textcolor = System.Drawing.Color.White
        Me.DashboardBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SplitContainer1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 69)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1590, 728)
        Me.Panel1.TabIndex = 7
        '
        'CashierProductComponent1
        '
        Me.CashierProductComponent1.BackColor = System.Drawing.Color.Lavender
        Me.CashierProductComponent1.Location = New System.Drawing.Point(-2, 0)
        Me.CashierProductComponent1.Margin = New System.Windows.Forms.Padding(2)
        Me.CashierProductComponent1.Name = "CashierProductComponent1"
        Me.CashierProductComponent1.Size = New System.Drawing.Size(1351, 729)
        Me.CashierProductComponent1.TabIndex = 1
        '
        'CashierTransactionLogsComponent1
        '
        Me.CashierTransactionLogsComponent1.BackColor = System.Drawing.SystemColors.Control
        Me.CashierTransactionLogsComponent1.Location = New System.Drawing.Point(-2, -3)
        Me.CashierTransactionLogsComponent1.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.CashierTransactionLogsComponent1.Name = "CashierTransactionLogsComponent1"
        Me.CashierTransactionLogsComponent1.Size = New System.Drawing.Size(1351, 729)
        Me.CashierTransactionLogsComponent1.TabIndex = 0
        '
        'SS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.ClientSize = New System.Drawing.Size(1590, 797)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel2)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "SS"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "CashierDashboard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.CashierDashboard.ResumeLayout(False)
        Me.BunifuCards3.ResumeLayout(False)
        Me.BunifuCards3.PerformLayout()
        Me.BunifuCards2.ResumeLayout(False)
        Me.BunifuCards2.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.BunifuCards1.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel4.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents timezone As Label
    Friend WithEvents AccountName As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents Panel1 As Panel
    Friend WithEvents CashierItemsBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents DashboardBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents TransactioLogs As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents LogoutBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents CashierTransactionLogsComponent1 As CashierTransactionLogsComponent
    Friend WithEvents CashierDashboard As Panel
    Friend WithEvents BunifuCards3 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents CompleteTransaction As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ServiceFeeBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Void As Bunifu.Framework.UI.BunifuFlatButton
    Private WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Private WithEvents Label11 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents CashierUsernameInput As Bunifu.Framework.UI.BunifuMaterialTextbox
    Friend WithEvents Panel5 As Panel
    Friend WithEvents BunifuCards2 As Bunifu.Framework.UI.BunifuCards
    Private WithEvents Label5 As Label
    Friend WithEvents Panel6 As Panel
    Friend WithEvents Label6 As Label
    Private WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Private WithEvents Label1 As Label
    Friend WithEvents Label7 As Label
    Private WithEvents TotalitemCount As Label
    Friend WithEvents Totalitms As Label
    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents CashierProductList As ListView
    Friend WithEvents ProductId As ColumnHeader
    Friend WithEvents ProductNames As ColumnHeader
    Friend WithEvents ProductBrand As ColumnHeader
    Friend WithEvents ProductPrice As ColumnHeader
    Friend WithEvents itemDiscount As ColumnHeader
    Friend WithEvents ProductQnty As ColumnHeader
    Friend WithEvents ProductSubtotal As ColumnHeader
    Friend WithEvents Panel4 As Panel
    Friend WithEvents RemoveItems As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents CashierItemSearch As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents CashierProductComponent1 As CashierProductComponent
End Class
