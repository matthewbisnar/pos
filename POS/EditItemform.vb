﻿Public Class EditItemForm
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private modules As New Api.utilities

    Public Structure edititemid
        Dim id As String
    End Structure

    Public edititeminstance As New edititemid
    Private Sub EditItemForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.loadSelectedItem()
        Me.LoadCategories()
    End Sub

    Private Sub loadSelectedItem()
        Dim fetchSelectedItem As Object = dbConfig.Process_qry("SELECT * FROM items INNER JOIN itemquantity ON items.itemid = itemquantity.itemIdForeign WHERE itemid = ?", edititeminstance.id)
        If fetchSelectedItem.tables(0).Rows.Count > 0 Then
            For Each items In fetchSelectedItem.tables(0).Rows
                productid.Text = items("itemid")
                ProductNameInput.Text = items("itemname")
                ProductBrandInput.Text = items("brand")
                TypecomboBox.Text = items("itemtype")
                CategoryComboBox.Text = items("itemcategory")
                itemPriceInput.Text = items("price")
                DiscountIInput.Text = items("discount")
                descriptionInput.Text = items("description")
                itemQuantity.Text = items("qnty")
            Next
        End If

        AddSubtractQuantity.Text = String.Empty
    End Sub

    Private Sub EditItemToList_Click(sender As Object, e As EventArgs) Handles EditItemToList.Click
        Dim itemprice As Double
        Dim discountitemprice As Double
        Dim DateData As String = String.Format("{0:dd/MM/yyyy - hh:mm:ss tt}", DateTime.Now)
        Dim selectedComboItemType As String

        If ProductNameInput.Text <> String.Empty And ProductBrandInput.Text <> String.Empty And descriptionInput.Text <> String.Empty Then

            If CategoryComboBox.Text <> String.Empty Then

                If itemPriceInput.Text = String.Empty Then
                    itemPriceInput.Text = 0
                End If

                If DiscountIInput.Text = String.Empty Then
                    DiscountIInput.Text = 0
                End If

                If TypecomboBox.Text = String.Empty Then
                    selectedComboItemType = "Brand New"
                Else
                    selectedComboItemType = If(TypecomboBox.SelectedIndex = 1, "Second Hand", "Brand New")
                End If

                If Double.TryParse(itemPriceInput.Text, itemprice) And Double.TryParse(DiscountIInput.Text, discountitemprice) Then

                    If CType(DiscountIInput.Text, Integer) <= 100 Then


                        With dbConfig
                            .Process_qry("UPDATE items SET itemname = ?, itemtype =?, brand = ?, description = ? , itemcategory = ?, price = ?, discount = ?, updatedat = ? WHERE itemid = ?",
                                     ProductNameInput.Text, selectedComboItemType, ProductBrandInput.Text, descriptionInput.Text, CategoryComboBox.Text, itemPriceInput.Text, DiscountIInput.Text, DateData, productid.Text)
                            .Process_qry("UPDATE itemquantity SET qnty = ? WHERE itemIdForeign = ? ", CType(itemQuantity.Text, Integer), productid.Text)
                        End With
                        systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", ManagerDashboard.manageridinstance.managerid},
                                {"description", productid.Text & " Item Successfully updated."},
                                {"type", "success"},
                                {"position", "manager"}
                            })

                        If MessageBox.Show("Item Successfully updated!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                            ManagerDashboard.loadItemList()
                            MyBase.Close()
                        End If
                    Else
                        MessageBox.Show("Exceeded maximum discount input!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    End If
                Else
                    MessageBox.Show("Price and Discount Inputs must be a number!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                    itemPriceInput.Text = 0
                    DiscountIInput.Text = 0
                End If
            Else
                MessageBox.Show("Please Select Category", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        Else
                MessageBox.Show("Input fields must not be empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If

    End Sub

    Private Sub ClearInputField_Click(sender As Object, e As EventArgs) Handles ClearInputField.Click
        Me.loadSelectedItem()
    End Sub

    Private Sub Cancel_Click(sender As Object, e As EventArgs) Handles Cancel.Click
        Me.loadSelectedItem()
        MyBase.Close()
    End Sub

    Private Sub LoadCategories()
        Dim loadCategories As Object = dbConfig.Process_qry("SELECT * FROM itemcategory")

        If loadCategories.tables(0).Rows.Count > 0 Then
            For Each categories As DataRow In loadCategories.tables(0).Rows
                CategoryComboBox.Items.Add(categories("category"))
            Next
        End If
    End Sub

    Private Sub addButton_Click(sender As Object, e As EventArgs) Handles addButton.Click
        Dim justint As Integer

        If Integer.TryParse(AddSubtractQuantity.Text, justint) Then
            itemQuantity.Text += CType(AddSubtractQuantity.Text, Integer)
        Else
            MessageBox.Show("Input must be a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub minusButton_Click(sender As Object, e As EventArgs) Handles minusButton.Click
        Dim justint As Integer

        If Integer.TryParse(AddSubtractQuantity.Text, justint) Then
            If CType(itemQuantity.Text, Integer) > 0 Then
                itemQuantity.Text -= CType(AddSubtractQuantity.Text, Integer)
            Else
                itemQuantity.Text = 0
            End If
        Else
            MessageBox.Show("Input must be a number", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub ClearQntity_Click(sender As Object, e As EventArgs) Handles ClearQntity.Click
        itemQuantity.Text = 0
    End Sub

    Private Sub AddSubtractQuantity_MouseEnter(sender As Object, e As EventArgs) Handles AddSubtractQuantity.MouseEnter
        If AddSubtractQuantity.Text.ToLower() = "quantity" Then
            AddSubtractQuantity.Text = String.Empty
        End If
    End Sub

    Private Sub AddSubtractQuantity_MouseLeave(sender As Object, e As EventArgs) Handles AddSubtractQuantity.MouseLeave
        If AddSubtractQuantity.Text = String.Empty Then
            AddSubtractQuantity.Text = "Quantity"
        End If
    End Sub
End Class