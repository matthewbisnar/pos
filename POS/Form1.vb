﻿Public Class Login
    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        CashierLoginComponent1.BringToFront()
    End Sub

    Private Sub CashierBtn_Click(sender As Object, e As EventArgs) Handles CashierBtn.Click
        CashierLoginComponent1.BringToFront()
    End Sub

    Private Sub ManagerBtn_Click(sender As Object, e As EventArgs) Handles ManagerBtn.Click
        ManagerLoginComponent1.BringToFront()
    End Sub

    Private Sub SoftwareAdmin_Click(sender As Object, e As EventArgs) Handles SoftwareAdmin.Click
        SoftwareAdminLoginComponent1.BringToFront()
    End Sub

End Class