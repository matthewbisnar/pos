﻿Public Class CashierProductComponent

    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private modules As New Api.utilities

    Public Sub loadItemList()
        Dim loaditemlist As Object = dbConfig.Process_qry("SELECT * FROM items INNER JOIN itemquantity ON itemquantity.itemIdForeign = items.itemid")

        If loaditemlist.tables(0).Rows.Count > 0 Then
            ItemList.Items.Clear()
            For Each items As DataRow In loaditemlist.tables(0).Rows
                With ItemList.Items.Add(items("itemid"))
                    .subitems.add(items("itemname").ToString.ToUpper())
                    .subitems.add(items("brand").ToString.ToUpper())
                    .subitems.add(items("itemType"))
                    .subitems.add(items("itemcategory"))
                    .subitems.add(items("description"))
                    .subitems.add(FormatCurrency(items("price"), , , TriState.True, TriState.True))
                    .subitems.add(items("discount") & "%")
                    .subitems.add(items("qnty"))
                End With
            Next
        Else
            ItemList.Items.Clear()
        End If
    End Sub

    Private Sub CashierProductComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        loadItemList()
    End Sub

    Private Sub searchBtn_Click(sender As Object, e As EventArgs) Handles searchBtn.Click
        search()
    End Sub

    Private Sub search()
        Dim SearchedItems = {searchiteminputs.Text & "%", searchiteminputs.Text & "%", searchiteminputs.Text & "%", searchiteminputs.Text & "%", searchiteminputs.Text & "%"}

        Dim fetchSearchItems As Object = dbConfig.Process_qry("SELECT * FROM items INNER JOIN itemquantity ON itemquantity.itemIdForeign = items.itemid WHERE items.itemname LIKE ? OR 
        items.description LIKE ? OR items.itemcategory LIKE ? OR items.brand LIKE ? OR items.itemid LIKE ? ", SearchedItems)

        If fetchSearchItems.tables(0).Rows.Count > 0 Then
            ItemList.Items.Clear()
            For Each items As DataRow In fetchSearchItems.tables(0).Rows
                With ItemList.Items.Add(items("itemid"))
                    .subitems.add(items("itemname").ToString.ToUpper())
                    .subitems.add(items("brand"))
                    .subitems.add(items("itemType"))
                    .subitems.add(items("itemcategory"))
                    .subitems.add(items("description"))
                    .subitems.add(FormatCurrency(items("price"), , , TriState.True, TriState.True))
                    .subitems.add(items("discount") & "%")
                    .subitems.add(items("qnty"))
                End With
            Next
        Else
            ItemList.Items.Clear()
        End If
    End Sub

    Private Sub searchiteminputs_MouseEnter(sender As Object, e As EventArgs) Handles searchiteminputs.MouseEnter
        If searchiteminputs.Text.ToLower() = "search items.." Then
            searchiteminputs.Text = String.Empty
        End If
    End Sub

    Private Sub searchiteminputs_MouseLeave(sender As Object, e As EventArgs) Handles searchiteminputs.MouseLeave
        If searchiteminputs.Text.ToLower() = String.Empty Then
            searchiteminputs.Text = "search items.."
        End If
    End Sub

    Private Sub refreshBtn_Click(sender As Object, e As EventArgs) Handles refreshBtn.Click
        loadItemList()
    End Sub
End Class
