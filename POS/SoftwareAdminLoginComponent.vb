﻿Public Class SoftwareAdminLoginComponent
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Private Structure LoginAttempt
        Dim LoginAttempt As Integer
        Dim LoginCounterTimer As Integer
    End Structure

    Dim attempt As New LoginAttempt

    Private Sub SoftwareAdminLoginComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim defaultadmin As Object = dbConfig.Process_qry("SELECT * FROM softwareadmin WHERE uniqueid = ?", My.Settings.softwareid)

        If defaultadmin.tables(0).Rows.Count > 0 Then
            For Each admin As DataRow In defaultadmin.tables(0).Rows
                If IsDBNull(admin("adminid")) And IsDBNull(admin("adminpassword")) And IsDBNull(admin("adminname")) Then
                    With dbConfig
                        .Process_qry("UPDATE softwareadmin SET adminid = ?, adminname = ?,adminpassword = ? WHERE uniqueid = ?", "20162762", My.Settings.softwareadminname, util.setHash("root"), My.Settings.softwareid)
                    End With
                End If
            Next
        Else
            With dbConfig
                .Process_qry("INSERT INTO softwareadmin (uniqueid) VALUES (?)", My.Settings.softwareid)
                .Process_qry("UPDATE softwareadmin SET adminid = ?, adminname = ?, adminpassword = ? WHERE uniqueid = ?", "20162762", My.Settings.softwareadminname, util.setHash("root"), My.Settings.softwareid)
            End With

        End If

        attempt.LoginAttempt = 0
        attempt.LoginCounterTimer = 30

    End Sub

    Private Sub disableInputs()
        SoftwareAdminInput.Text = String.Empty
        SoftwareAdminPassword.Text = String.Empty

        SoftwareAdminInput.Enabled = False
        SoftwareAdminPassword.Enabled = False
        SoftwareAdminBtn.Enabled = False

    End Sub

    Public Sub enabledInput()
        SoftwareAdminInput.Text = String.Empty
        SoftwareAdminPassword.Text = String.Empty

        SoftwareAdminInput.Enabled = True
        SoftwareAdminPassword.Enabled = True
        SoftwareAdminBtn.Enabled = True
        attempt.LoginAttempt = 0
        LoginAttemptNotif.Text = String.Empty
    End Sub

    Private Sub SoftwareAdminBtn_Click_1(sender As Object, e As EventArgs) Handles SoftwareAdminBtn.Click
        If (SoftwareAdminInput.Text <> String.Empty Or SoftwareAdminInput.Text.ToLower() = "Admin Login".ToLower()) Or (SoftwareAdminPassword.Text <> String.Empty Or SoftwareAdminPassword.Text.ToLower() = "BunifuMaterialTextbox1".ToLower()) Then
            With dbConfig
                Dim process_qry As Object = dbConfig.Process_qry("SELECT * FROM softwareadmin WHERE adminid = ? AND adminpassword = ?", SoftwareAdminInput.Text, util.setHash(SoftwareAdminPassword.Text))

                If process_qry.tables(0).Rows.Count > 0 Then
                    For Each password As DataRow In process_qry.tables(0).Rows
                        If util.verifyHash(util.setHash(password("adminpassword"))) = True Then
                            systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", SoftwareAdminInput.Text},
                                {"description", "Account Successfully Login!"},
                                {"type", "success"},
                                {"position", "admin"}
                            })
                            attempt.LoginAttempt = 0
                            attempt.LoginCounterTimer = 30
                            SoftwareAdminDashboard.SoftwareAdminInstance.id = SoftwareAdminInput.Text
                            SoftwareAdminInput.Text = String.Empty
                            SoftwareAdminPassword.Text = String.Empty
                            LoginAttemptNotif.Hide()
                            Login.Hide()
                            SoftwareAdminDashboard.ShowDialog()
                        End If
                    Next
                Else
                    If attempt.LoginAttempt < 3 Then
                        MessageBox.Show("Username and/or Password you Entered is Incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        systemlogs.loginlog(New Dictionary(Of String, String) From {
                            {"LoginId", SoftwareAdminInput.Text},
                            {"description", "Username and/or Password you Entered is Incorrect!"},
                            {"type", "error"},
                            {"position", "admin"}
                        })

                        attempt.LoginAttempt = attempt.LoginAttempt + 1
                        LoginAttemptNotif.Text = "Login Failed " & attempt.LoginAttempt & " attempt."
                        SoftwareAdminInput.Text = String.Empty
                        SoftwareAdminPassword.Text = String.Empty
                    Else
                        MessageBox.Show("Login Maximum 3 Attempt Exceeded!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        LoginAttemptNotif.Text = "Login Maximum " & attempt.LoginAttempt & " Attempt Exceeded!"
                        Me.disableInputs()
                    End If
                End If
            End With
        Else
            MessageBox.Show("Username and/or Password Must Not be Empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub SoftwareAdminInput_MouseEnter(sender As Object, e As EventArgs) Handles SoftwareAdminInput.MouseEnter
        If SoftwareAdminInput.Text.ToLower() = "Admin Login".ToLower() Then
            SoftwareAdminInput.Text = String.Empty
        End If
    End Sub

    Private Sub SoftwareAdminPassword_MouseEnter(sender As Object, e As EventArgs) Handles SoftwareAdminPassword.MouseEnter
        If SoftwareAdminPassword.Text.ToLower() = "BunifuMaterialTextbox1".ToLower() Then
            SoftwareAdminPassword.Text = String.Empty
        End If
    End Sub

    Private Sub SoftwareAdminInput_MouseLeave(sender As Object, e As EventArgs) Handles SoftwareAdminInput.MouseLeave
        If SoftwareAdminInput.Text = String.Empty Then
            SoftwareAdminInput.Text = "Admin Login"
        End If
    End Sub

    Private Sub SoftwareAdminPassword_MouseLeave(sender As Object, e As EventArgs) Handles SoftwareAdminPassword.MouseLeave
        If SoftwareAdminPassword.Text = String.Empty Then
            SoftwareAdminPassword.isPassword = True
            SoftwareAdminPassword.Text = "BunifuMaterialTextbox1"
        End If
    End Sub
End Class
