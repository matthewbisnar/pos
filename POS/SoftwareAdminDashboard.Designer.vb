﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SoftwareAdminDashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SoftwareAdminDashboard))
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.timezone = New System.Windows.Forms.Label()
        Me.AccountName = New System.Windows.Forms.Label()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SoftwareAdminLogout = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.SoftwareAdminSettings = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.TransasctionLogsBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ActivityLogs = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.SoftwareDashboard = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Splitter1 = New System.Windows.Forms.Splitter()
        Me.SoftwareLogComponents1 = New POS.SoftwareLogComponents()
        Me.SoftwareDashboardComponent1 = New POS.SoftwareDashboardComponent()
        Me.SoftwareAdminSettingsComponent1 = New POS.SoftwareAdminSettingsComponent()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel2.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(239, 691)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(192, 13)
        Me.Label2.TabIndex = 7
        Me.Label2.Text = "Copyright 2019 POS. All rights reserved"
        Me.Label2.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'Timer1
        '
        '
        'Panel3
        '
        Me.Panel3.Location = New System.Drawing.Point(0, 63)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(1327, 602)
        Me.Panel3.TabIndex = 2
        '
        'timezone
        '
        Me.timezone.AutoSize = True
        Me.timezone.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.timezone.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.timezone.Location = New System.Drawing.Point(245, 23)
        Me.timezone.Name = "timezone"
        Me.timezone.Size = New System.Drawing.Size(0, 20)
        Me.timezone.TabIndex = 3
        '
        'AccountName
        '
        Me.AccountName.AutoSize = True
        Me.AccountName.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AccountName.ForeColor = System.Drawing.SystemColors.ButtonHighlight
        Me.AccountName.Location = New System.Drawing.Point(1438, 23)
        Me.AccountName.Name = "AccountName"
        Me.AccountName.Size = New System.Drawing.Size(129, 20)
        Me.AccountName.TabIndex = 4
        Me.AccountName.Text = "Matthew (Admin)"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(1390, 17)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(39, 33)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.Color.RoyalBlue
        Me.Panel2.Controls.Add(Me.PictureBox1)
        Me.Panel2.Controls.Add(Me.AccountName)
        Me.Panel2.Controls.Add(Me.timezone)
        Me.Panel2.Controls.Add(Me.Panel3)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(1583, 69)
        Me.Panel2.TabIndex = 2
        '
        'Panel4
        '
        Me.Panel4.BackColor = System.Drawing.Color.Lavender
        Me.Panel4.Controls.Add(Me.Panel1)
        Me.Panel4.Controls.Add(Me.SoftwareAdminLogout)
        Me.Panel4.Controls.Add(Me.SoftwareAdminSettings)
        Me.Panel4.Controls.Add(Me.TransasctionLogsBtn)
        Me.Panel4.Controls.Add(Me.ActivityLogs)
        Me.Panel4.Controls.Add(Me.SoftwareDashboard)
        Me.Panel4.Controls.Add(Me.Splitter1)
        Me.Panel4.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel4.Location = New System.Drawing.Point(0, 69)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(1583, 730)
        Me.Panel4.TabIndex = 8
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SoftwareAdminSettingsComponent1)
        Me.Panel1.Controls.Add(Me.SoftwareLogComponents1)
        Me.Panel1.Controls.Add(Me.SoftwareDashboardComponent1)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(233, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1350, 730)
        Me.Panel1.TabIndex = 8
        '
        'SoftwareAdminLogout
        '
        Me.SoftwareAdminLogout.Active = False
        Me.SoftwareAdminLogout.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminLogout.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminLogout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SoftwareAdminLogout.BorderRadius = 0
        Me.SoftwareAdminLogout.ButtonText = "Logout"
        Me.SoftwareAdminLogout.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SoftwareAdminLogout.DisabledColor = System.Drawing.Color.Gray
        Me.SoftwareAdminLogout.Iconcolor = System.Drawing.Color.Transparent
        Me.SoftwareAdminLogout.Iconimage = CType(resources.GetObject("SoftwareAdminLogout.Iconimage"), System.Drawing.Image)
        Me.SoftwareAdminLogout.Iconimage_right = Nothing
        Me.SoftwareAdminLogout.Iconimage_right_Selected = Nothing
        Me.SoftwareAdminLogout.Iconimage_Selected = Nothing
        Me.SoftwareAdminLogout.IconMarginLeft = 0
        Me.SoftwareAdminLogout.IconMarginRight = 0
        Me.SoftwareAdminLogout.IconRightVisible = True
        Me.SoftwareAdminLogout.IconRightZoom = 0R
        Me.SoftwareAdminLogout.IconVisible = True
        Me.SoftwareAdminLogout.IconZoom = 90.0R
        Me.SoftwareAdminLogout.IsTab = False
        Me.SoftwareAdminLogout.Location = New System.Drawing.Point(1, 269)
        Me.SoftwareAdminLogout.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.SoftwareAdminLogout.Name = "SoftwareAdminLogout"
        Me.SoftwareAdminLogout.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminLogout.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SoftwareAdminLogout.OnHoverTextColor = System.Drawing.Color.White
        Me.SoftwareAdminLogout.selected = False
        Me.SoftwareAdminLogout.Size = New System.Drawing.Size(232, 67)
        Me.SoftwareAdminLogout.TabIndex = 7
        Me.SoftwareAdminLogout.Text = "Logout"
        Me.SoftwareAdminLogout.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SoftwareAdminLogout.Textcolor = System.Drawing.Color.White
        Me.SoftwareAdminLogout.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'SoftwareAdminSettings
        '
        Me.SoftwareAdminSettings.Active = False
        Me.SoftwareAdminSettings.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminSettings.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminSettings.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SoftwareAdminSettings.BorderRadius = 0
        Me.SoftwareAdminSettings.ButtonText = "Settings"
        Me.SoftwareAdminSettings.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SoftwareAdminSettings.DisabledColor = System.Drawing.Color.Gray
        Me.SoftwareAdminSettings.Iconcolor = System.Drawing.Color.Transparent
        Me.SoftwareAdminSettings.Iconimage = CType(resources.GetObject("SoftwareAdminSettings.Iconimage"), System.Drawing.Image)
        Me.SoftwareAdminSettings.Iconimage_right = Nothing
        Me.SoftwareAdminSettings.Iconimage_right_Selected = Nothing
        Me.SoftwareAdminSettings.Iconimage_Selected = Nothing
        Me.SoftwareAdminSettings.IconMarginLeft = 0
        Me.SoftwareAdminSettings.IconMarginRight = 0
        Me.SoftwareAdminSettings.IconRightVisible = True
        Me.SoftwareAdminSettings.IconRightZoom = 0R
        Me.SoftwareAdminSettings.IconVisible = True
        Me.SoftwareAdminSettings.IconZoom = 90.0R
        Me.SoftwareAdminSettings.IsTab = False
        Me.SoftwareAdminSettings.Location = New System.Drawing.Point(1, 202)
        Me.SoftwareAdminSettings.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.SoftwareAdminSettings.Name = "SoftwareAdminSettings"
        Me.SoftwareAdminSettings.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareAdminSettings.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SoftwareAdminSettings.OnHoverTextColor = System.Drawing.Color.White
        Me.SoftwareAdminSettings.selected = False
        Me.SoftwareAdminSettings.Size = New System.Drawing.Size(232, 67)
        Me.SoftwareAdminSettings.TabIndex = 6
        Me.SoftwareAdminSettings.Text = "Settings"
        Me.SoftwareAdminSettings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SoftwareAdminSettings.Textcolor = System.Drawing.Color.White
        Me.SoftwareAdminSettings.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'TransasctionLogsBtn
        '
        Me.TransasctionLogsBtn.Active = False
        Me.TransasctionLogsBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.TransasctionLogsBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.TransasctionLogsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.TransasctionLogsBtn.BorderRadius = 0
        Me.TransasctionLogsBtn.ButtonText = "Transaction Logs"
        Me.TransasctionLogsBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.TransasctionLogsBtn.DisabledColor = System.Drawing.Color.Gray
        Me.TransasctionLogsBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.TransasctionLogsBtn.Iconimage = CType(resources.GetObject("TransasctionLogsBtn.Iconimage"), System.Drawing.Image)
        Me.TransasctionLogsBtn.Iconimage_right = Nothing
        Me.TransasctionLogsBtn.Iconimage_right_Selected = Nothing
        Me.TransasctionLogsBtn.Iconimage_Selected = Nothing
        Me.TransasctionLogsBtn.IconMarginLeft = 0
        Me.TransasctionLogsBtn.IconMarginRight = 0
        Me.TransasctionLogsBtn.IconRightVisible = True
        Me.TransasctionLogsBtn.IconRightZoom = 0R
        Me.TransasctionLogsBtn.IconVisible = True
        Me.TransasctionLogsBtn.IconZoom = 90.0R
        Me.TransasctionLogsBtn.IsTab = False
        Me.TransasctionLogsBtn.Location = New System.Drawing.Point(1, 135)
        Me.TransasctionLogsBtn.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TransasctionLogsBtn.Name = "TransasctionLogsBtn"
        Me.TransasctionLogsBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.TransasctionLogsBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.TransasctionLogsBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.TransasctionLogsBtn.selected = False
        Me.TransasctionLogsBtn.Size = New System.Drawing.Size(232, 67)
        Me.TransasctionLogsBtn.TabIndex = 5
        Me.TransasctionLogsBtn.Text = "Transaction Logs"
        Me.TransasctionLogsBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.TransasctionLogsBtn.Textcolor = System.Drawing.Color.White
        Me.TransasctionLogsBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ActivityLogs
        '
        Me.ActivityLogs.Active = False
        Me.ActivityLogs.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ActivityLogs.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ActivityLogs.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ActivityLogs.BorderRadius = 0
        Me.ActivityLogs.ButtonText = "Activity Logs"
        Me.ActivityLogs.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ActivityLogs.DisabledColor = System.Drawing.Color.Gray
        Me.ActivityLogs.Iconcolor = System.Drawing.Color.Transparent
        Me.ActivityLogs.Iconimage = CType(resources.GetObject("ActivityLogs.Iconimage"), System.Drawing.Image)
        Me.ActivityLogs.Iconimage_right = Nothing
        Me.ActivityLogs.Iconimage_right_Selected = Nothing
        Me.ActivityLogs.Iconimage_Selected = Nothing
        Me.ActivityLogs.IconMarginLeft = 0
        Me.ActivityLogs.IconMarginRight = 0
        Me.ActivityLogs.IconRightVisible = True
        Me.ActivityLogs.IconRightZoom = 0R
        Me.ActivityLogs.IconVisible = True
        Me.ActivityLogs.IconZoom = 90.0R
        Me.ActivityLogs.IsTab = False
        Me.ActivityLogs.Location = New System.Drawing.Point(1, 68)
        Me.ActivityLogs.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.ActivityLogs.Name = "ActivityLogs"
        Me.ActivityLogs.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ActivityLogs.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ActivityLogs.OnHoverTextColor = System.Drawing.Color.White
        Me.ActivityLogs.selected = False
        Me.ActivityLogs.Size = New System.Drawing.Size(232, 67)
        Me.ActivityLogs.TabIndex = 4
        Me.ActivityLogs.Text = "Activity Logs"
        Me.ActivityLogs.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ActivityLogs.Textcolor = System.Drawing.Color.White
        Me.ActivityLogs.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'SoftwareDashboard
        '
        Me.SoftwareDashboard.Active = False
        Me.SoftwareDashboard.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareDashboard.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareDashboard.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SoftwareDashboard.BorderRadius = 0
        Me.SoftwareDashboard.ButtonText = "Dashboard"
        Me.SoftwareDashboard.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SoftwareDashboard.DisabledColor = System.Drawing.Color.Gray
        Me.SoftwareDashboard.Iconcolor = System.Drawing.Color.Transparent
        Me.SoftwareDashboard.Iconimage = CType(resources.GetObject("SoftwareDashboard.Iconimage"), System.Drawing.Image)
        Me.SoftwareDashboard.Iconimage_right = Nothing
        Me.SoftwareDashboard.Iconimage_right_Selected = Nothing
        Me.SoftwareDashboard.Iconimage_Selected = Nothing
        Me.SoftwareDashboard.IconMarginLeft = 0
        Me.SoftwareDashboard.IconMarginRight = 0
        Me.SoftwareDashboard.IconRightVisible = True
        Me.SoftwareDashboard.IconRightZoom = 0R
        Me.SoftwareDashboard.IconVisible = True
        Me.SoftwareDashboard.IconZoom = 90.0R
        Me.SoftwareDashboard.IsTab = False
        Me.SoftwareDashboard.Location = New System.Drawing.Point(1, 1)
        Me.SoftwareDashboard.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.SoftwareDashboard.Name = "SoftwareDashboard"
        Me.SoftwareDashboard.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SoftwareDashboard.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SoftwareDashboard.OnHoverTextColor = System.Drawing.Color.White
        Me.SoftwareDashboard.selected = False
        Me.SoftwareDashboard.Size = New System.Drawing.Size(232, 67)
        Me.SoftwareDashboard.TabIndex = 3
        Me.SoftwareDashboard.Text = "Dashboard"
        Me.SoftwareDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SoftwareDashboard.Textcolor = System.Drawing.Color.White
        Me.SoftwareDashboard.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Splitter1
        '
        Me.Splitter1.BackColor = System.Drawing.Color.White
        Me.Splitter1.Location = New System.Drawing.Point(0, 0)
        Me.Splitter1.Name = "Splitter1"
        Me.Splitter1.Size = New System.Drawing.Size(233, 730)
        Me.Splitter1.TabIndex = 2
        Me.Splitter1.TabStop = False
        '
        'SoftwareLogComponents1
        '
        Me.SoftwareLogComponents1.BackColor = System.Drawing.Color.Lavender
        Me.SoftwareLogComponents1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SoftwareLogComponents1.Location = New System.Drawing.Point(0, 0)
        Me.SoftwareLogComponents1.Margin = New System.Windows.Forms.Padding(1)
        Me.SoftwareLogComponents1.Name = "SoftwareLogComponents1"
        Me.SoftwareLogComponents1.Size = New System.Drawing.Size(1350, 730)
        Me.SoftwareLogComponents1.TabIndex = 0
        '
        'SoftwareDashboardComponent1
        '
        Me.SoftwareDashboardComponent1.BackColor = System.Drawing.Color.Lavender
        Me.SoftwareDashboardComponent1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SoftwareDashboardComponent1.Location = New System.Drawing.Point(0, 0)
        Me.SoftwareDashboardComponent1.Margin = New System.Windows.Forms.Padding(1)
        Me.SoftwareDashboardComponent1.Name = "SoftwareDashboardComponent1"
        Me.SoftwareDashboardComponent1.Size = New System.Drawing.Size(1350, 730)
        Me.SoftwareDashboardComponent1.TabIndex = 0
        '
        'SoftwareAdminSettingsComponent1
        '
        Me.SoftwareAdminSettingsComponent1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SoftwareAdminSettingsComponent1.Location = New System.Drawing.Point(0, 0)
        Me.SoftwareAdminSettingsComponent1.Name = "SoftwareAdminSettingsComponent1"
        Me.SoftwareAdminSettingsComponent1.Size = New System.Drawing.Size(1350, 730)
        Me.SoftwareAdminSettingsComponent1.TabIndex = 1
        '
        'SoftwareAdminDashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1583, 799)
        Me.Controls.Add(Me.Panel4)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel2)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "SoftwareAdminDashboard"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SoftwareAdminDashboard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents Timer1 As Timer
    Friend WithEvents Panel3 As Panel
    Friend WithEvents timezone As Label
    Friend WithEvents AccountName As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents Panel4 As Panel
    Friend WithEvents Splitter1 As Splitter
    Friend WithEvents SoftwareDashboard As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SoftwareAdminLogout As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SoftwareAdminSettings As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents TransasctionLogsBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ActivityLogs As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents SoftwareDashboardComponent1 As SoftwareDashboardComponent
    Friend WithEvents SoftwareLogComponents1 As SoftwareLogComponents
    Friend WithEvents SoftwareAdminSettingsComponent1 As SoftwareAdminSettingsComponent
End Class
