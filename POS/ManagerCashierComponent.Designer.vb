﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ManagerCashierComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(ManagerCashierComponent))
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.CashierList = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.EditCashierBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.RefreshCashier1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.DeleteCashierBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddCashierbtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SearchCashier = New System.Windows.Forms.TextBox()
        Me.SearchCashierBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BunifuCards1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.SplitContainer1)
        Me.BunifuCards1.Controls.Add(Me.Panel1)
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(19, 19)
        Me.BunifuCards1.Margin = New System.Windows.Forms.Padding(2)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(1309, 604)
        Me.BunifuCards1.TabIndex = 0
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 55)
        Me.SplitContainer1.Margin = New System.Windows.Forms.Padding(2)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.CashierList)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.Panel2)
        Me.SplitContainer1.Size = New System.Drawing.Size(1309, 549)
        Me.SplitContainer1.SplitterDistance = 1112
        Me.SplitContainer1.SplitterWidth = 3
        Me.SplitContainer1.TabIndex = 2
        '
        'CashierList
        '
        Me.CashierList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.CashierList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CashierList.FullRowSelect = True
        Me.CashierList.GridLines = True
        Me.CashierList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.CashierList.HideSelection = False
        Me.CashierList.Location = New System.Drawing.Point(0, 0)
        Me.CashierList.Margin = New System.Windows.Forms.Padding(0)
        Me.CashierList.Name = "CashierList"
        Me.CashierList.Size = New System.Drawing.Size(1112, 549)
        Me.CashierList.TabIndex = 15
        Me.CashierList.UseCompatibleStateImageBehavior = False
        Me.CashierList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Employee ID"
        Me.ColumnHeader1.Width = 281
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Firstname"
        Me.ColumnHeader2.Width = 228
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Lastname"
        Me.ColumnHeader3.Width = 434
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Position"
        Me.ColumnHeader4.Width = 444
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Date Created"
        Me.ColumnHeader5.Width = 656
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.EditCashierBtn1)
        Me.Panel2.Controls.Add(Me.RefreshCashier1)
        Me.Panel2.Controls.Add(Me.DeleteCashierBtn1)
        Me.Panel2.Controls.Add(Me.AddCashierbtn1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(0, 0)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(194, 549)
        Me.Panel2.TabIndex = 0
        '
        'EditCashierBtn1
        '
        Me.EditCashierBtn1.Active = False
        Me.EditCashierBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditCashierBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditCashierBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.EditCashierBtn1.BorderRadius = 0
        Me.EditCashierBtn1.ButtonText = "Edit Cashier"
        Me.EditCashierBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditCashierBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.EditCashierBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.EditCashierBtn1.Iconimage = CType(resources.GetObject("EditCashierBtn1.Iconimage"), System.Drawing.Image)
        Me.EditCashierBtn1.Iconimage_right = Nothing
        Me.EditCashierBtn1.Iconimage_right_Selected = Nothing
        Me.EditCashierBtn1.Iconimage_Selected = Nothing
        Me.EditCashierBtn1.IconMarginLeft = 0
        Me.EditCashierBtn1.IconMarginRight = 0
        Me.EditCashierBtn1.IconRightVisible = False
        Me.EditCashierBtn1.IconRightZoom = 0R
        Me.EditCashierBtn1.IconVisible = False
        Me.EditCashierBtn1.IconZoom = 90.0R
        Me.EditCashierBtn1.IsTab = False
        Me.EditCashierBtn1.Location = New System.Drawing.Point(1, 73)
        Me.EditCashierBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.EditCashierBtn1.Name = "EditCashierBtn1"
        Me.EditCashierBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditCashierBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.EditCashierBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.EditCashierBtn1.selected = False
        Me.EditCashierBtn1.Size = New System.Drawing.Size(192, 67)
        Me.EditCashierBtn1.TabIndex = 30
        Me.EditCashierBtn1.Text = "Edit Cashier"
        Me.EditCashierBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.EditCashierBtn1.Textcolor = System.Drawing.Color.White
        Me.EditCashierBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'RefreshCashier1
        '
        Me.RefreshCashier1.Active = False
        Me.RefreshCashier1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshCashier1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshCashier1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RefreshCashier1.BorderRadius = 0
        Me.RefreshCashier1.ButtonText = "Refresh"
        Me.RefreshCashier1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshCashier1.DisabledColor = System.Drawing.Color.Gray
        Me.RefreshCashier1.Iconcolor = System.Drawing.Color.Transparent
        Me.RefreshCashier1.Iconimage = CType(resources.GetObject("RefreshCashier1.Iconimage"), System.Drawing.Image)
        Me.RefreshCashier1.Iconimage_right = Nothing
        Me.RefreshCashier1.Iconimage_right_Selected = Nothing
        Me.RefreshCashier1.Iconimage_Selected = Nothing
        Me.RefreshCashier1.IconMarginLeft = 0
        Me.RefreshCashier1.IconMarginRight = 0
        Me.RefreshCashier1.IconRightVisible = False
        Me.RefreshCashier1.IconRightZoom = 0R
        Me.RefreshCashier1.IconVisible = False
        Me.RefreshCashier1.IconZoom = 90.0R
        Me.RefreshCashier1.IsTab = False
        Me.RefreshCashier1.Location = New System.Drawing.Point(0, 207)
        Me.RefreshCashier1.Margin = New System.Windows.Forms.Padding(0)
        Me.RefreshCashier1.Name = "RefreshCashier1"
        Me.RefreshCashier1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshCashier1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.RefreshCashier1.OnHoverTextColor = System.Drawing.Color.White
        Me.RefreshCashier1.selected = False
        Me.RefreshCashier1.Size = New System.Drawing.Size(194, 67)
        Me.RefreshCashier1.TabIndex = 29
        Me.RefreshCashier1.Text = "Refresh"
        Me.RefreshCashier1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RefreshCashier1.Textcolor = System.Drawing.Color.White
        Me.RefreshCashier1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'DeleteCashierBtn1
        '
        Me.DeleteCashierBtn1.Active = False
        Me.DeleteCashierBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteCashierBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteCashierBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DeleteCashierBtn1.BorderRadius = 0
        Me.DeleteCashierBtn1.ButtonText = "Delete Cashier"
        Me.DeleteCashierBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteCashierBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.DeleteCashierBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.DeleteCashierBtn1.Iconimage = CType(resources.GetObject("DeleteCashierBtn1.Iconimage"), System.Drawing.Image)
        Me.DeleteCashierBtn1.Iconimage_right = Nothing
        Me.DeleteCashierBtn1.Iconimage_right_Selected = Nothing
        Me.DeleteCashierBtn1.Iconimage_Selected = Nothing
        Me.DeleteCashierBtn1.IconMarginLeft = 0
        Me.DeleteCashierBtn1.IconMarginRight = 0
        Me.DeleteCashierBtn1.IconRightVisible = False
        Me.DeleteCashierBtn1.IconRightZoom = 0R
        Me.DeleteCashierBtn1.IconVisible = False
        Me.DeleteCashierBtn1.IconZoom = 90.0R
        Me.DeleteCashierBtn1.IsTab = False
        Me.DeleteCashierBtn1.Location = New System.Drawing.Point(0, 140)
        Me.DeleteCashierBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.DeleteCashierBtn1.Name = "DeleteCashierBtn1"
        Me.DeleteCashierBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteCashierBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.DeleteCashierBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.DeleteCashierBtn1.selected = False
        Me.DeleteCashierBtn1.Size = New System.Drawing.Size(194, 67)
        Me.DeleteCashierBtn1.TabIndex = 28
        Me.DeleteCashierBtn1.Text = "Delete Cashier"
        Me.DeleteCashierBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DeleteCashierBtn1.Textcolor = System.Drawing.Color.White
        Me.DeleteCashierBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddCashierbtn1
        '
        Me.AddCashierbtn1.Active = False
        Me.AddCashierbtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddCashierbtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddCashierbtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddCashierbtn1.BorderRadius = 0
        Me.AddCashierbtn1.ButtonText = "Add Cashier"
        Me.AddCashierbtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddCashierbtn1.DisabledColor = System.Drawing.Color.Gray
        Me.AddCashierbtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.AddCashierbtn1.Iconimage = CType(resources.GetObject("AddCashierbtn1.Iconimage"), System.Drawing.Image)
        Me.AddCashierbtn1.Iconimage_right = Nothing
        Me.AddCashierbtn1.Iconimage_right_Selected = Nothing
        Me.AddCashierbtn1.Iconimage_Selected = Nothing
        Me.AddCashierbtn1.IconMarginLeft = 0
        Me.AddCashierbtn1.IconMarginRight = 0
        Me.AddCashierbtn1.IconRightVisible = False
        Me.AddCashierbtn1.IconRightZoom = 0R
        Me.AddCashierbtn1.IconVisible = False
        Me.AddCashierbtn1.IconZoom = 90.0R
        Me.AddCashierbtn1.IsTab = False
        Me.AddCashierbtn1.Location = New System.Drawing.Point(0, 6)
        Me.AddCashierbtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.AddCashierbtn1.Name = "AddCashierbtn1"
        Me.AddCashierbtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddCashierbtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.AddCashierbtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.AddCashierbtn1.selected = False
        Me.AddCashierbtn1.Size = New System.Drawing.Size(194, 67)
        Me.AddCashierbtn1.TabIndex = 27
        Me.AddCashierbtn1.Text = "Add Cashier"
        Me.AddCashierbtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddCashierbtn1.Textcolor = System.Drawing.Color.White
        Me.AddCashierbtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SearchCashier)
        Me.Panel1.Controls.Add(Me.SearchCashierBtn)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(2)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1309, 55)
        Me.Panel1.TabIndex = 1
        '
        'SearchCashier
        '
        Me.SearchCashier.Location = New System.Drawing.Point(0, 0)
        Me.SearchCashier.Margin = New System.Windows.Forms.Padding(0)
        Me.SearchCashier.Multiline = True
        Me.SearchCashier.Name = "SearchCashier"
        Me.SearchCashier.Size = New System.Drawing.Size(1112, 54)
        Me.SearchCashier.TabIndex = 20
        Me.SearchCashier.Text = "Search Cashier"
        '
        'SearchCashierBtn
        '
        Me.SearchCashierBtn.Active = False
        Me.SearchCashierBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchCashierBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchCashierBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SearchCashierBtn.BorderRadius = 0
        Me.SearchCashierBtn.ButtonText = "Search"
        Me.SearchCashierBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SearchCashierBtn.DisabledColor = System.Drawing.Color.Gray
        Me.SearchCashierBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.SearchCashierBtn.Iconimage = CType(resources.GetObject("SearchCashierBtn.Iconimage"), System.Drawing.Image)
        Me.SearchCashierBtn.Iconimage_right = Nothing
        Me.SearchCashierBtn.Iconimage_right_Selected = Nothing
        Me.SearchCashierBtn.Iconimage_Selected = Nothing
        Me.SearchCashierBtn.IconMarginLeft = 0
        Me.SearchCashierBtn.IconMarginRight = 0
        Me.SearchCashierBtn.IconRightVisible = False
        Me.SearchCashierBtn.IconRightZoom = 0R
        Me.SearchCashierBtn.IconVisible = False
        Me.SearchCashierBtn.IconZoom = 90.0R
        Me.SearchCashierBtn.IsTab = False
        Me.SearchCashierBtn.Location = New System.Drawing.Point(1115, 0)
        Me.SearchCashierBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.SearchCashierBtn.Name = "SearchCashierBtn"
        Me.SearchCashierBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchCashierBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SearchCashierBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.SearchCashierBtn.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.SearchCashierBtn.selected = False
        Me.SearchCashierBtn.Size = New System.Drawing.Size(193, 55)
        Me.SearchCashierBtn.TabIndex = 26
        Me.SearchCashierBtn.Text = "Search"
        Me.SearchCashierBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SearchCashierBtn.Textcolor = System.Drawing.Color.White
        Me.SearchCashierBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 646)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1347, 53)
        Me.Panel5.TabIndex = 4
        '
        'ManagerCashierComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.BunifuCards1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "ManagerCashierComponent"
        Me.Size = New System.Drawing.Size(1347, 699)
        Me.BunifuCards1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents Panel1 As Panel
    Friend WithEvents CashierList As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents SearchCashier As TextBox
    Friend WithEvents Panel2 As Panel
    Friend WithEvents EditCashierBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents RefreshCashier1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents DeleteCashierBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddCashierbtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SearchCashierBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel5 As Panel
End Class
