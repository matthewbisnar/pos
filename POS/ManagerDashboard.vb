﻿Public Class ManagerDashboard
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private modules As New Api.utilities

    Public Structure ManagerId
        Dim managerid As String
        Dim managerfirstname As String
        Dim managerlastname As String
        Dim managerposition As String
    End Structure

    Public manageridinstance As New ManagerId

    Private Sub CashierLogout_Click(sender As Object, e As EventArgs)
        Login.Show()
        MyBase.Hide()
    End Sub

    Private Sub ManagerDashboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ManagerPanelDashboard.BringToFront()
        Timer1.Start()
        Me.managerAccountsFunction()
        Me.loadItemList()
    End Sub

    Public Sub loadItemList()
        Dim loaditemlist As Object = dbConfig.Process_qry("SELECT * FROM items INNER JOIN itemquantity ON itemquantity.itemIdForeign = items.itemid")

        If loaditemlist.tables(0).Rows.Count > 0 Then
            ItemList.Items.Clear()
            For Each items As DataRow In loaditemlist.tables(0).Rows
                With ItemList.Items.Add(items("itemid"))
                    .subitems.add(items("itemname").ToString.ToUpper())
                    .subitems.add(items("brand").ToString.ToUpper())
                    .subitems.add(items("itemType"))
                    .subitems.add(items("itemcategory"))
                    .subitems.add(items("description"))
                    .subitems.add(FormatCurrency(items("price"), , , TriState.True, TriState.True))
                    .subitems.add(items("discount") & "%")
                    .subitems.add(items("qnty"))
                End With
            Next
        Else
            ItemList.Items.Clear()
        End If
    End Sub

    Private Sub managerAccountsFunction()
        Dim fetchManager As Object = dbConfig.Process_qry("SELECT * FROM manager WHERE managerid = ?", manageridinstance.managerid)

        If fetchManager.tables(0).Rows.Count > 0 Then
            For Each fetch As DataRow In fetchManager.tables(0).rows
                manageridinstance.managerfirstname = fetch("firstname")
                manageridinstance.managerlastname = fetch("lastname")
                manageridinstance.managerposition = fetch("position")
            Next
        End If

        AccountName.Text = manageridinstance.managerfirstname & " (" & manageridinstance.managerposition & ")"
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        timezone.Text = "Time zone: " & Format(Now, "dd/mm/yyyy hh:mm:ss tt")
    End Sub

    Private Sub LogoutBtn_Click(sender As Object, e As EventArgs) Handles LogoutBtn.Click
        Login.Show()
        MyBase.Hide()
    End Sub

    '---------------------------------------------------------------------------
    Private Sub CashierList_Click(sender As Object, e As EventArgs) Handles CashierList.Click
        ManagerCashierComponent1.BringToFront()
    End Sub

    Private Sub DashboardBtn_Click(sender As Object, e As EventArgs) Handles DashboardBtn.Click
        ManagerPanelDashboard.BringToFront()
    End Sub

    Private Sub SearchBtn_Click_1(sender As Object, e As EventArgs) Handles SearchBtn.Click
        Dim SearchedItems = {Searchitems.Text & "%", Searchitems.Text & "%", Searchitems.Text & "%", Searchitems.Text & "%", Searchitems.Text & "%"}

        Dim fetchSearchItems As Object = dbConfig.Process_qry("SELECT * FROM items INNER JOIN itemquantity ON itemquantity.itemIdForeign = items.itemid WHERE items.itemname LIKE ? OR 
        items.description LIKE ? OR items.itemcategory LIKE ? OR items.brand LIKE ? OR items.itemid LIKE ? ", SearchedItems)

        If fetchSearchItems.tables(0).Rows.Count > 0 Then
            ItemList.Items.Clear()
            For Each items As DataRow In fetchSearchItems.tables(0).Rows
                With ItemList.Items.Add(items("itemid"))
                    .subitems.add(items("itemname").ToString.ToUpper())
                    .subitems.add(items("brand"))
                    .subitems.add(items("itemType"))
                    .subitems.add(items("itemcategory"))
                    .subitems.add(items("description"))
                    .subitems.add(FormatCurrency(items("price"), , , TriState.True, TriState.True))
                    .subitems.add(items("discount") & "%")
                    .subitems.add(items("qnty"))
                End With
            Next
        Else
            ItemList.Items.Clear()
        End If
    End Sub

    Private Sub RefreshBtn_Click_1(sender As Object, e As EventArgs) Handles RefreshBtn.Click
        Me.loadItemList()
    End Sub

    Private Sub additem_Click(sender As Object, e As EventArgs) Handles additem.Click
        additemPopup.ShowDialog()
    End Sub

    Private Sub EditItem_Click_1(sender As Object, e As EventArgs) Handles EditItem.Click
        Try
            EditItemForm.edititeminstance.id = ItemList.SelectedItems(0).SubItems(0).Text
            EditItemForm.ShowDialog()
        Catch ex As Exception
            MessageBox.Show("Please Select Item", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub DeleteItem_Click_1(sender As Object, e As EventArgs) Handles DeleteItem.Click
        Try
            If MessageBox.Show("Are you sure you want to delete item with id " & ItemList.SelectedItems(0).SubItems(0).Text & " ?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.OK Then

                With dbConfig
                    .Process_qry("DELETE FROM items WHERE itemid = ?", ItemList.SelectedItems(0).SubItems(0).Text)
                    .Process_qry("DELETE FROM itemquantity WHERE itemIdForeign = ?", ItemList.SelectedItems(0).SubItems(0).Text)
                End With

                If MessageBox.Show("Manager Successfully deleted, close and/or refresh to view item list.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    systemlogs.loginlog(New Dictionary(Of String, String) From {
                               {"LoginId", ItemList.SelectedItems(0).SubItems(0).Text},
                               {"description", ItemList.SelectedItems(0).SubItems(0).Text & " item Successfully deleted!"},
                               {"type", "success"},
                               {"position", "manager"}
                            })
                    Me.loadItemList()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Please Select item!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub BunifuFlatButton5_Click_1(sender As Object, e As EventArgs) Handles BunifuFlatButton5.Click
        modules.exportcsv(ItemList, "items", "items\")
    End Sub

    Private Sub Searchitems_MouseEnter_1(sender As Object, e As EventArgs) Handles Searchitems.MouseEnter
        If Searchitems.Text.ToLower() = "Search items".ToLower() Then
            Searchitems.Text = String.Empty
        End If
    End Sub

    Private Sub Searchitems_MouseLeave_1(sender As Object, e As EventArgs) Handles Searchitems.MouseLeave
        If Searchitems.Text = String.Empty Then
            Searchitems.Text = "Search items"
        End If
    End Sub
End Class