﻿Public Class editcashier

    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Public Structure EditCashier
        Dim CashierId As String
        Dim firstname As String
        Dim lastname As String
        Dim position As String
        Dim CashierPassword As String
        Dim CashierConfirmPassword As String
    End Structure

    Public EditCashierInstance As New EditCashier

    Private Sub editcashier_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim loadCashier As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE cashierid = ?", EditCashierInstance.CashierId)

        If loadCashier.tables(0).rows.count > 0 Then
            For Each fetchManager As DataRow In loadCashier.tables(0).rows
                EditCashierInstance.CashierId = fetchManager("cashierid")
                EditCashierInstance.firstname = fetchManager("firstname")
                EditCashierInstance.lastname = fetchManager("lastname")
                EditCashierInstance.position = fetchManager("position")
            Next

            EditCashierId.ReadOnly = True
            EditCashierPosition.ReadOnly = True
            EditCashierId.Text = EditCashierInstance.CashierId
            EditCashierFirstname.Text = EditCashierInstance.firstname
            EditCashierLastname.Text = EditCashierInstance.lastname
            EditCashierPosition.Text = EditCashierInstance.position
        End If
    End Sub

    Private Sub EditManagerBtn_Click(sender As Object, e As EventArgs) Handles EditManagerBtn.Click
        If EditCashierFirstname.Text <> String.Empty And EditCashierLastname.Text <> String.Empty Then

            EditCashierInstance.firstname = EditCashierFirstname.Text
            EditCashierInstance.lastname = EditCashierLastname.Text

            With dbConfig
                .Process_qry("UPDATE cashier SET firstname = ?, lastname = ? WHERE cashierid = ?",
                EditCashierInstance.firstname, EditCashierInstance.lastname, EditCashierInstance.CashierId
            )
            End With

            systemlogs.loginlog(New Dictionary(Of String, String) From {
                {"LoginId", EditCashierInstance.CashierId},
                {"description", "Account Successfully updated!"},
                {"type", "success"},
                {"position", "cashier"}
                })

            If MessageBox.Show("Account Successfully Edited.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                MyBase.Close()
            End If
        Else
            MessageBox.Show("Please Input firstname and lastname!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub CancelBtn_Click(sender As Object, e As EventArgs) Handles CancelBtn.Click
        MyBase.Close()
    End Sub

    Private Sub CancelBtn1_Click(sender As Object, e As EventArgs) Handles CancelBtn1.Click
        MyBase.Close()
    End Sub

    Private Sub Changepassword_Click(sender As Object, e As EventArgs) Handles Changepassword.Click
        If CashierChangePassword.Text <> String.Empty And CashierConfirmChangePassword.Text <> String.Empty Then
            If CashierChangePassword.Text = CashierConfirmChangePassword.Text Then

                With dbConfig
                    .Process_qry("UPDATE cashier SET cashierpassword = ? WHERE cashierid = ?", util.setHash(CashierChangePassword.Text), EditCashierInstance.CashierId)
                End With

                CashierChangePassword.Text = String.Empty
                CashierConfirmChangePassword.Text = String.Empty

                systemlogs.loginlog(New Dictionary(Of String, String) From {
                    {"LoginId", EditCashierInstance.CashierId},
                    {"description", "Password Successfully updated!"},
                    {"type", "success"},
                    {"position", "cashier"}
                })

                If MessageBox.Show("Password Successfully Updated.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    MyBase.Close()
                End If
            Else
                MessageBox.Show("Change password and Confirm password does not match.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        Else
            MessageBox.Show("Please Input Change password and Confirm password.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub
End Class