﻿Public Class SoftwareLogComponents
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private modules As New Api.utilities

    Private Sub SoftwareLogComponents_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.loadlogs()
    End Sub

    Private Sub loadlogs()
        Dim fetchlog As Object = dbConfig.Process_qry("SELECT * FROM systemlogs")

        If fetchlog.tables(0).Rows.Count > 0 Then

            LogList.Items.Clear()

            For Each logs As DataRow In fetchlog.tables(0).rows
                With LogList.Items.Add(logs("logid"))
                    .SubItems.Add(logs("dateandtime"))
                    .SubItems.Add(logs("Employeeid"))
                    .SubItems.Add(logs("EmployeePosition"))
                    .SubItems.Add(logs("type"))
                    .SubItems.Add(logs("description"))
                End With
            Next
        Else
            LogList.Items.Clear()
        End If
    End Sub

    'Private Sub logTypeComboBox_SelectedIndexChanged(sender As Object, e As EventArgs)

    'Dim fetchSearchAccount As Object = dbConfig.Process_qry("SELECT * FROM systemlogs WHERE employeeposition = ?", logTypeComboBox.SelectedItem.ToString())

    '    DatePickerLog1.Text = String.Empty
    '   SearchLogs.Text = "Search logs"
    'PositionCombox.Text = String.Empty

    'If fetchSearchAccount.tables(0).Rows.Count > 0 Then

    '       LogList.Items.Clear()

    'For Each logs As DataRow In fetchSearchAccount.tables(0).rows
    'With LogList.Items.Add(logs("logid"))
    '.SubItems.Add(logs("dateandtime"))
    '.SubItems.Add(logs("Employeeid"))
    '.SubItems.Add(logs("EmployeePosition"))
    '.SubItems.Add(logs("type"))
    '.SubItems.Add(logs("description"))
    'End With
    'Next
    'Else
    '       LogList.Items.Clear()
    'End If
    'End Sub

    'Private Sub PositionCombox_SelectedIndexChanged(sender As Object, e As EventArgs)
    ' Dim fetchSearchAccount As Object = dbConfig.Process_qry("SELECT * FROM systemlogs WHERE type = ?", PositionCombox.SelectedItem.ToString())

    '   DatePickerLog.Text = String.Empty
    '    logTypeComboBox.Text = String.Empty
    '  SearchLogs.Text = "Search logs"

    'If fetchSearchAccount.tables(0).Rows.Count > 0 Then

    '       LogList.Items.Clear()

    'For Each logs As DataRow In fetchSearchAccount.tables(0).rows
    'With LogList.Items.Add(logs("logid"))
    '.SubItems.Add(logs("dateandtime"))
    '.SubItems.Add(logs("Employeeid"))
    '.SubItems.Add(logs("EmployeePosition"))
    '.SubItems.Add(logs("type"))
    '.SubItems.Add(logs("description"))
    'End With
    'Next
    'Else
    ''       LogList.Items.Clear()
    'End If
    'End Sub

    Private Sub Searchbtn1_Click(sender As Object, e As EventArgs) Handles SearchBtn1.Click
        Dim fetchSearchAccount As Object = dbConfig.Process_qry("SELECT * FROM systemlogs WHERE employeeid = ? OR employeeposition = ?", SearchLogs.Text, SearchLogs.Text)

        ' DatePickerLog1.Text = String.Empty
        'logTypeComboBox.Text = String.Empty
        'PositionCombox.Text = String.Empty

        If fetchSearchAccount.tables(0).Rows.Count > 0 Then

            LogList.Items.Clear()

            For Each logs As DataRow In fetchSearchAccount.tables(0).rows
                With LogList.Items.Add(logs("logid"))
                    .SubItems.Add(logs("dateandtime"))
                    .SubItems.Add(logs("Employeeid"))
                    .SubItems.Add(logs("EmployeePosition"))
                    .SubItems.Add(logs("type"))
                    .SubItems.Add(logs("description"))
                End With
            Next
        Else
            LogList.Items.Clear()
        End If
    End Sub

    Private Sub ExportBtn_Click(sender As Object, e As EventArgs) Handles ExportBtn.Click
        Me.loadlogs()
        modules.exportcsv(LogList, "systemlogs", "logs\systemlogs\")

        ' DatePickerLog.Text = String.Empty
        'logTypeComboBox.Text = String.Empty
        SearchLogs.Text = "Search logs"
        'PositionCombox.Text = String.Empty
        Me.loadlogs()
    End Sub

    Private Sub RefreshBtn_Click(sender As Object, e As EventArgs) Handles RefreshBtn.Click
        'DatePickerLog1.Text = String.Empty
        'logTypeComboBox.Text = String.Empty
        SearchLogs.Text = "Search logs"
        'PositionCombox.Text = String.Empty
        Me.loadlogs()
    End Sub

    Private Sub SearchLogs_MouseEnter(sender As Object, e As EventArgs) Handles SearchLogs.MouseEnter
        If SearchLogs.Text.ToLower() = "Search logs".ToLower() Then
            SearchLogs.Text = String.Empty
        End If
    End Sub

    Private Sub SearchLogs_MouseLeave(sender As Object, e As EventArgs) Handles SearchLogs.MouseLeave
        If SearchLogs.Text = String.Empty Then
            SearchLogs.Text = "Search logs"
        End If
    End Sub

    'Private Sub DatePickerLog1_onValueChanged(sender As Object, e As EventArgs)
    'Dim fetchSearchAccount As Object = dbConfig.Process_qry("SELECT * FROM systemlogs WHERE createdat = ?", DatePickerLog1.Text)


    'logTypeComboBox.Text = String.Empty
    '    SearchLogs.Text = "Search logs"
    'PositionCombox.Text = String.Empty

    'If fetchSearchAccount.tables(0).Rows.Count > 0 Then

    '       LogList.Items.Clear()

    'For Each logs As DataRow In fetchSearchAccount.tables(0).rows
    'With LogList.Items.Add(logs("logid"))
    '.SubItems.Add(logs("dateandtime"))
    '.SubItems.Add(logs("Employeeid"))
    '.SubItems.Add(logs("EmployeePosition"))
    '.SubItems.Add(logs("type"))
    ''.SubItems.Add(logs("description"))
    'End With
    'Next
    'Else
    '       LogList.Items.Clear()
    'End If
    'End Sub
End Class
