﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class additemPopup
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(additemPopup))
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.TypecomboBox = New System.Windows.Forms.ComboBox()
        Me.itemQntity = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.Cancel = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.ClearInputField = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddItemtolist = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.descriptionInput = New WindowsFormsControlLibrary1.BunifuCustomTextbox()
        Me.DiscountIInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.itemPriceInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.CategoryComboBox = New System.Windows.Forms.ComboBox()
        Me.ProductBrandInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.ProductNameInput = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.GenerateId = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.productid = New Bunifu.Framework.UI.BunifuMetroTextbox()
        Me.BunifuCards1.SuspendLayout()
        Me.SuspendLayout()
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.TypecomboBox)
        Me.BunifuCards1.Controls.Add(Me.itemQntity)
        Me.BunifuCards1.Controls.Add(Me.Cancel)
        Me.BunifuCards1.Controls.Add(Me.ClearInputField)
        Me.BunifuCards1.Controls.Add(Me.AddItemtolist)
        Me.BunifuCards1.Controls.Add(Me.descriptionInput)
        Me.BunifuCards1.Controls.Add(Me.DiscountIInput)
        Me.BunifuCards1.Controls.Add(Me.itemPriceInput)
        Me.BunifuCards1.Controls.Add(Me.CategoryComboBox)
        Me.BunifuCards1.Controls.Add(Me.ProductBrandInput)
        Me.BunifuCards1.Controls.Add(Me.ProductNameInput)
        Me.BunifuCards1.Controls.Add(Me.GenerateId)
        Me.BunifuCards1.Controls.Add(Me.productid)
        Me.BunifuCards1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(0, 0)
        Me.BunifuCards1.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(879, 809)
        Me.BunifuCards1.TabIndex = 0
        '
        'TypecomboBox
        '
        Me.TypecomboBox.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.TypecomboBox.FormattingEnabled = True
        Me.TypecomboBox.Items.AddRange(New Object() {"Brand New", "Second Hand"})
        Me.TypecomboBox.Location = New System.Drawing.Point(450, 260)
        Me.TypecomboBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.TypecomboBox.Name = "TypecomboBox"
        Me.TypecomboBox.Size = New System.Drawing.Size(403, 31)
        Me.TypecomboBox.TabIndex = 13
        Me.TypecomboBox.Text = "Brand New"
        '
        'itemQntity
        '
        Me.itemQntity.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.itemQntity.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemQntity.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.itemQntity.BorderThickness = 1
        Me.itemQntity.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.itemQntity.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.itemQntity.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.itemQntity.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemQntity.isPassword = False
        Me.itemQntity.Location = New System.Drawing.Point(18, 329)
        Me.itemQntity.Margin = New System.Windows.Forms.Padding(6)
        Me.itemQntity.MaxLength = 100
        Me.itemQntity.Name = "itemQntity"
        Me.itemQntity.Size = New System.Drawing.Size(137, 68)
        Me.itemQntity.TabIndex = 12
        Me.itemQntity.Text = "Quantity"
        Me.itemQntity.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'Cancel
        '
        Me.Cancel.Active = False
        Me.Cancel.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Cancel.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Cancel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Cancel.BorderRadius = 0
        Me.Cancel.ButtonText = "Cancel"
        Me.Cancel.Cursor = System.Windows.Forms.Cursors.Hand
        Me.Cancel.DisabledColor = System.Drawing.Color.Gray
        Me.Cancel.Iconcolor = System.Drawing.Color.Transparent
        Me.Cancel.Iconimage = CType(resources.GetObject("Cancel.Iconimage"), System.Drawing.Image)
        Me.Cancel.Iconimage_right = Nothing
        Me.Cancel.Iconimage_right_Selected = Nothing
        Me.Cancel.Iconimage_Selected = Nothing
        Me.Cancel.IconMarginLeft = 0
        Me.Cancel.IconMarginRight = 0
        Me.Cancel.IconRightVisible = True
        Me.Cancel.IconRightZoom = 0R
        Me.Cancel.IconVisible = True
        Me.Cancel.IconZoom = 90.0R
        Me.Cancel.IsTab = False
        Me.Cancel.Location = New System.Drawing.Point(256, 718)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.Cancel.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.Cancel.OnHoverTextColor = System.Drawing.Color.White
        Me.Cancel.selected = False
        Me.Cancel.Size = New System.Drawing.Size(186, 68)
        Me.Cancel.TabIndex = 11
        Me.Cancel.Text = "Cancel"
        Me.Cancel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.Cancel.Textcolor = System.Drawing.Color.White
        Me.Cancel.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'ClearInputField
        '
        Me.ClearInputField.Active = False
        Me.ClearInputField.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearInputField.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearInputField.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClearInputField.BorderRadius = 0
        Me.ClearInputField.ButtonText = "Clear"
        Me.ClearInputField.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ClearInputField.DisabledColor = System.Drawing.Color.Gray
        Me.ClearInputField.Iconcolor = System.Drawing.Color.Transparent
        Me.ClearInputField.Iconimage = CType(resources.GetObject("ClearInputField.Iconimage"), System.Drawing.Image)
        Me.ClearInputField.Iconimage_right = Nothing
        Me.ClearInputField.Iconimage_right_Selected = Nothing
        Me.ClearInputField.Iconimage_Selected = Nothing
        Me.ClearInputField.IconMarginLeft = 0
        Me.ClearInputField.IconMarginRight = 0
        Me.ClearInputField.IconRightVisible = True
        Me.ClearInputField.IconRightZoom = 0R
        Me.ClearInputField.IconVisible = True
        Me.ClearInputField.IconZoom = 90.0R
        Me.ClearInputField.IsTab = False
        Me.ClearInputField.Location = New System.Drawing.Point(452, 718)
        Me.ClearInputField.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.ClearInputField.Name = "ClearInputField"
        Me.ClearInputField.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ClearInputField.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ClearInputField.OnHoverTextColor = System.Drawing.Color.White
        Me.ClearInputField.selected = False
        Me.ClearInputField.Size = New System.Drawing.Size(174, 68)
        Me.ClearInputField.TabIndex = 10
        Me.ClearInputField.Text = "Clear"
        Me.ClearInputField.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.ClearInputField.Textcolor = System.Drawing.Color.White
        Me.ClearInputField.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddItemtolist
        '
        Me.AddItemtolist.Active = False
        Me.AddItemtolist.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddItemtolist.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddItemtolist.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddItemtolist.BorderRadius = 0
        Me.AddItemtolist.ButtonText = "Add Item"
        Me.AddItemtolist.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddItemtolist.DisabledColor = System.Drawing.Color.Gray
        Me.AddItemtolist.Iconcolor = System.Drawing.Color.Transparent
        Me.AddItemtolist.Iconimage = CType(resources.GetObject("AddItemtolist.Iconimage"), System.Drawing.Image)
        Me.AddItemtolist.Iconimage_right = Nothing
        Me.AddItemtolist.Iconimage_right_Selected = Nothing
        Me.AddItemtolist.Iconimage_Selected = Nothing
        Me.AddItemtolist.IconMarginLeft = 0
        Me.AddItemtolist.IconMarginRight = 0
        Me.AddItemtolist.IconRightVisible = True
        Me.AddItemtolist.IconRightZoom = 0R
        Me.AddItemtolist.IconVisible = True
        Me.AddItemtolist.IconZoom = 90.0R
        Me.AddItemtolist.IsTab = False
        Me.AddItemtolist.Location = New System.Drawing.Point(635, 718)
        Me.AddItemtolist.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.AddItemtolist.Name = "AddItemtolist"
        Me.AddItemtolist.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddItemtolist.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.AddItemtolist.OnHoverTextColor = System.Drawing.Color.White
        Me.AddItemtolist.selected = False
        Me.AddItemtolist.Size = New System.Drawing.Size(222, 68)
        Me.AddItemtolist.TabIndex = 9
        Me.AddItemtolist.Text = "Add Item"
        Me.AddItemtolist.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.AddItemtolist.Textcolor = System.Drawing.Color.White
        Me.AddItemtolist.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'descriptionInput
        '
        Me.descriptionInput.BorderColor = System.Drawing.Color.SeaGreen
        Me.descriptionInput.Location = New System.Drawing.Point(18, 426)
        Me.descriptionInput.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.descriptionInput.Multiline = True
        Me.descriptionInput.Name = "descriptionInput"
        Me.descriptionInput.Size = New System.Drawing.Size(838, 264)
        Me.descriptionInput.TabIndex = 8
        Me.descriptionInput.Text = "Specifications"
        '
        'DiscountIInput
        '
        Me.DiscountIInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.DiscountIInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DiscountIInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.DiscountIInput.BorderThickness = 1
        Me.DiscountIInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.DiscountIInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.DiscountIInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.DiscountIInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.DiscountIInput.isPassword = False
        Me.DiscountIInput.Location = New System.Drawing.Point(585, 329)
        Me.DiscountIInput.Margin = New System.Windows.Forms.Padding(6)
        Me.DiscountIInput.MaxLength = 100
        Me.DiscountIInput.Name = "DiscountIInput"
        Me.DiscountIInput.Size = New System.Drawing.Size(270, 68)
        Me.DiscountIInput.TabIndex = 7
        Me.DiscountIInput.Text = "Discount"
        Me.DiscountIInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'itemPriceInput
        '
        Me.itemPriceInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.itemPriceInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemPriceInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.itemPriceInput.BorderThickness = 1
        Me.itemPriceInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.itemPriceInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.itemPriceInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.itemPriceInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.itemPriceInput.isPassword = False
        Me.itemPriceInput.Location = New System.Drawing.Point(158, 329)
        Me.itemPriceInput.Margin = New System.Windows.Forms.Padding(6)
        Me.itemPriceInput.MaxLength = 32767
        Me.itemPriceInput.Name = "itemPriceInput"
        Me.itemPriceInput.Size = New System.Drawing.Size(423, 68)
        Me.itemPriceInput.TabIndex = 6
        Me.itemPriceInput.Text = "Price"
        Me.itemPriceInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'CategoryComboBox
        '
        Me.CategoryComboBox.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.CategoryComboBox.FormattingEnabled = True
        Me.CategoryComboBox.Items.AddRange(New Object() {"Other Category"})
        Me.CategoryComboBox.Location = New System.Drawing.Point(18, 260)
        Me.CategoryComboBox.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.CategoryComboBox.Name = "CategoryComboBox"
        Me.CategoryComboBox.Size = New System.Drawing.Size(422, 31)
        Me.CategoryComboBox.TabIndex = 5
        Me.CategoryComboBox.Text = "No Category"
        '
        'ProductBrandInput
        '
        Me.ProductBrandInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.ProductBrandInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductBrandInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.ProductBrandInput.BorderThickness = 1
        Me.ProductBrandInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.ProductBrandInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ProductBrandInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.ProductBrandInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductBrandInput.isPassword = False
        Me.ProductBrandInput.Location = New System.Drawing.Point(450, 157)
        Me.ProductBrandInput.Margin = New System.Windows.Forms.Padding(6)
        Me.ProductBrandInput.MaxLength = 32767
        Me.ProductBrandInput.Name = "ProductBrandInput"
        Me.ProductBrandInput.Size = New System.Drawing.Size(406, 68)
        Me.ProductBrandInput.TabIndex = 4
        Me.ProductBrandInput.Text = "Product Brand"
        Me.ProductBrandInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'ProductNameInput
        '
        Me.ProductNameInput.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.ProductNameInput.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductNameInput.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.ProductNameInput.BorderThickness = 1
        Me.ProductNameInput.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.ProductNameInput.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.ProductNameInput.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.ProductNameInput.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.ProductNameInput.isPassword = False
        Me.ProductNameInput.Location = New System.Drawing.Point(15, 157)
        Me.ProductNameInput.Margin = New System.Windows.Forms.Padding(6)
        Me.ProductNameInput.MaxLength = 32767
        Me.ProductNameInput.Name = "ProductNameInput"
        Me.ProductNameInput.Size = New System.Drawing.Size(423, 68)
        Me.ProductNameInput.TabIndex = 3
        Me.ProductNameInput.Text = "Product Name"
        Me.ProductNameInput.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'GenerateId
        '
        Me.GenerateId.Active = False
        Me.GenerateId.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.GenerateId.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.GenerateId.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.GenerateId.BorderRadius = 0
        Me.GenerateId.ButtonText = "Generate ID"
        Me.GenerateId.Cursor = System.Windows.Forms.Cursors.Hand
        Me.GenerateId.DisabledColor = System.Drawing.Color.Gray
        Me.GenerateId.Iconcolor = System.Drawing.Color.Transparent
        Me.GenerateId.Iconimage = CType(resources.GetObject("GenerateId.Iconimage"), System.Drawing.Image)
        Me.GenerateId.Iconimage_right = Nothing
        Me.GenerateId.Iconimage_right_Selected = Nothing
        Me.GenerateId.Iconimage_Selected = Nothing
        Me.GenerateId.IconMarginLeft = 0
        Me.GenerateId.IconMarginRight = 0
        Me.GenerateId.IconRightVisible = True
        Me.GenerateId.IconRightZoom = 0R
        Me.GenerateId.IconVisible = True
        Me.GenerateId.IconZoom = 90.0R
        Me.GenerateId.IsTab = False
        Me.GenerateId.Location = New System.Drawing.Point(615, 62)
        Me.GenerateId.Margin = New System.Windows.Forms.Padding(6, 8, 6, 8)
        Me.GenerateId.Name = "GenerateId"
        Me.GenerateId.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.GenerateId.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.GenerateId.OnHoverTextColor = System.Drawing.Color.White
        Me.GenerateId.selected = False
        Me.GenerateId.Size = New System.Drawing.Size(242, 68)
        Me.GenerateId.TabIndex = 2
        Me.GenerateId.Text = "Generate ID"
        Me.GenerateId.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.GenerateId.Textcolor = System.Drawing.Color.White
        Me.GenerateId.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'productid
        '
        Me.productid.BorderColorFocused = System.Drawing.Color.SlateGray
        Me.productid.BorderColorIdle = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.productid.BorderColorMouseHover = System.Drawing.Color.SlateGray
        Me.productid.BorderThickness = 1
        Me.productid.characterCasing = System.Windows.Forms.CharacterCasing.Normal
        Me.productid.Cursor = System.Windows.Forms.Cursors.IBeam
        Me.productid.Enabled = False
        Me.productid.Font = New System.Drawing.Font("Century Gothic", 9.75!)
        Me.productid.ForeColor = System.Drawing.Color.FromArgb(CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(64, Byte), Integer))
        Me.productid.isPassword = False
        Me.productid.Location = New System.Drawing.Point(15, 62)
        Me.productid.Margin = New System.Windows.Forms.Padding(6)
        Me.productid.MaxLength = 32767
        Me.productid.Name = "productid"
        Me.productid.Size = New System.Drawing.Size(590, 68)
        Me.productid.TabIndex = 1
        Me.productid.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        '
        'additemPopup
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(879, 809)
        Me.Controls.Add(Me.BunifuCards1)
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "additemPopup"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "additem"
        Me.BunifuCards1.ResumeLayout(False)
        Me.BunifuCards1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents GenerateId As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents productid As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ProductBrandInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents ProductNameInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents itemPriceInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents CategoryComboBox As ComboBox
    Friend WithEvents DiscountIInput As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents descriptionInput As WindowsFormsControlLibrary1.BunifuCustomTextbox
    Friend WithEvents Cancel As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ClearInputField As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddItemtolist As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents itemQntity As Bunifu.Framework.UI.BunifuMetroTextbox
    Friend WithEvents TypecomboBox As ComboBox
End Class
