﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SoftwareLogComponents
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SoftwareLogComponents))
        Me.LogList = New System.Windows.Forms.ListView()
        Me.logid = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.dateandtime = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Employeeid = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.EmployeePosition = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.typelog = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.descriptionlog = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.SplitContainer1 = New System.Windows.Forms.SplitContainer()
        Me.ExportBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.RefreshBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SearchBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.SearchLogs = New System.Windows.Forms.TextBox()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.BunifuCards1.SuspendLayout()
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SplitContainer1.Panel1.SuspendLayout()
        Me.SplitContainer1.Panel2.SuspendLayout()
        Me.SplitContainer1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LogList
        '
        Me.LogList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.logid, Me.dateandtime, Me.Employeeid, Me.EmployeePosition, Me.typelog, Me.descriptionlog})
        Me.LogList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LogList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LogList.FullRowSelect = True
        Me.LogList.GridLines = True
        Me.LogList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LogList.HideSelection = False
        Me.LogList.Location = New System.Drawing.Point(0, 0)
        Me.LogList.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.LogList.Name = "LogList"
        Me.LogList.Size = New System.Drawing.Size(1098, 584)
        Me.LogList.TabIndex = 3
        Me.LogList.UseCompatibleStateImageBehavior = False
        Me.LogList.View = System.Windows.Forms.View.Details
        '
        'logid
        '
        Me.logid.Text = "Log ID"
        Me.logid.Width = 273
        '
        'dateandtime
        '
        Me.dateandtime.Text = "Date and Time"
        Me.dateandtime.Width = 250
        '
        'Employeeid
        '
        Me.Employeeid.Text = "Employee ID"
        Me.Employeeid.Width = 229
        '
        'EmployeePosition
        '
        Me.EmployeePosition.Text = "Position"
        Me.EmployeePosition.Width = 279
        '
        'typelog
        '
        Me.typelog.Text = "type"
        Me.typelog.Width = 225
        '
        'descriptionlog
        '
        Me.descriptionlog.Text = "Description"
        Me.descriptionlog.Width = 683
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.SplitContainer1)
        Me.BunifuCards1.Controls.Add(Me.Panel1)
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(17, 16)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(1313, 638)
        Me.BunifuCards1.TabIndex = 19
        '
        'SplitContainer1
        '
        Me.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SplitContainer1.Location = New System.Drawing.Point(0, 54)
        Me.SplitContainer1.Name = "SplitContainer1"
        '
        'SplitContainer1.Panel1
        '
        Me.SplitContainer1.Panel1.Controls.Add(Me.LogList)
        '
        'SplitContainer1.Panel2
        '
        Me.SplitContainer1.Panel2.Controls.Add(Me.ExportBtn)
        Me.SplitContainer1.Panel2.Controls.Add(Me.RefreshBtn)
        Me.SplitContainer1.Size = New System.Drawing.Size(1313, 584)
        Me.SplitContainer1.SplitterDistance = 1098
        Me.SplitContainer1.TabIndex = 2
        '
        'ExportBtn
        '
        Me.ExportBtn.Active = False
        Me.ExportBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ExportBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ExportBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ExportBtn.BorderRadius = 0
        Me.ExportBtn.ButtonText = "Export"
        Me.ExportBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.ExportBtn.DisabledColor = System.Drawing.Color.Gray
        Me.ExportBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.ExportBtn.Iconimage = CType(resources.GetObject("ExportBtn.Iconimage"), System.Drawing.Image)
        Me.ExportBtn.Iconimage_right = Nothing
        Me.ExportBtn.Iconimage_right_Selected = Nothing
        Me.ExportBtn.Iconimage_Selected = Nothing
        Me.ExportBtn.IconMarginLeft = 0
        Me.ExportBtn.IconMarginRight = 0
        Me.ExportBtn.IconRightVisible = True
        Me.ExportBtn.IconRightZoom = 0R
        Me.ExportBtn.IconVisible = True
        Me.ExportBtn.IconZoom = 90.0R
        Me.ExportBtn.IsTab = False
        Me.ExportBtn.Location = New System.Drawing.Point(0, 2)
        Me.ExportBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.ExportBtn.Name = "ExportBtn"
        Me.ExportBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.ExportBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.ExportBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.ExportBtn.selected = False
        Me.ExportBtn.Size = New System.Drawing.Size(210, 59)
        Me.ExportBtn.TabIndex = 18
        Me.ExportBtn.Text = "Export"
        Me.ExportBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.ExportBtn.Textcolor = System.Drawing.Color.White
        Me.ExportBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'RefreshBtn
        '
        Me.RefreshBtn.Active = False
        Me.RefreshBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RefreshBtn.BorderRadius = 0
        Me.RefreshBtn.ButtonText = "Refresh"
        Me.RefreshBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshBtn.DisabledColor = System.Drawing.Color.Gray
        Me.RefreshBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.RefreshBtn.Iconimage = CType(resources.GetObject("RefreshBtn.Iconimage"), System.Drawing.Image)
        Me.RefreshBtn.Iconimage_right = Nothing
        Me.RefreshBtn.Iconimage_right_Selected = Nothing
        Me.RefreshBtn.Iconimage_Selected = Nothing
        Me.RefreshBtn.IconMarginLeft = 0
        Me.RefreshBtn.IconMarginRight = 0
        Me.RefreshBtn.IconRightVisible = True
        Me.RefreshBtn.IconRightZoom = 0R
        Me.RefreshBtn.IconVisible = True
        Me.RefreshBtn.IconZoom = 90.0R
        Me.RefreshBtn.IsTab = False
        Me.RefreshBtn.Location = New System.Drawing.Point(0, 61)
        Me.RefreshBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.RefreshBtn.Name = "RefreshBtn"
        Me.RefreshBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.RefreshBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.RefreshBtn.selected = False
        Me.RefreshBtn.Size = New System.Drawing.Size(210, 59)
        Me.RefreshBtn.TabIndex = 19
        Me.RefreshBtn.Text = "Refresh"
        Me.RefreshBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RefreshBtn.Textcolor = System.Drawing.Color.White
        Me.RefreshBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SearchBtn1)
        Me.Panel1.Controls.Add(Me.SearchLogs)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1313, 54)
        Me.Panel1.TabIndex = 1
        '
        'SearchBtn1
        '
        Me.SearchBtn1.Active = False
        Me.SearchBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SearchBtn1.BorderRadius = 0
        Me.SearchBtn1.ButtonText = "Search"
        Me.SearchBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SearchBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.SearchBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.SearchBtn1.Iconimage = CType(resources.GetObject("SearchBtn1.Iconimage"), System.Drawing.Image)
        Me.SearchBtn1.Iconimage_right = Nothing
        Me.SearchBtn1.Iconimage_right_Selected = Nothing
        Me.SearchBtn1.Iconimage_Selected = Nothing
        Me.SearchBtn1.IconMarginLeft = 0
        Me.SearchBtn1.IconMarginRight = 0
        Me.SearchBtn1.IconRightVisible = True
        Me.SearchBtn1.IconRightZoom = 0R
        Me.SearchBtn1.IconVisible = True
        Me.SearchBtn1.IconZoom = 90.0R
        Me.SearchBtn1.IsTab = False
        Me.SearchBtn1.Location = New System.Drawing.Point(1101, -2)
        Me.SearchBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.SearchBtn1.Name = "SearchBtn1"
        Me.SearchBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SearchBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.SearchBtn1.selected = False
        Me.SearchBtn1.Size = New System.Drawing.Size(210, 56)
        Me.SearchBtn1.TabIndex = 16
        Me.SearchBtn1.Text = "Search"
        Me.SearchBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SearchBtn1.Textcolor = System.Drawing.Color.White
        Me.SearchBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'SearchLogs
        '
        Me.SearchLogs.Location = New System.Drawing.Point(384, -3)
        Me.SearchLogs.Margin = New System.Windows.Forms.Padding(2)
        Me.SearchLogs.Multiline = True
        Me.SearchLogs.Name = "SearchLogs"
        Me.SearchLogs.Size = New System.Drawing.Size(713, 57)
        Me.SearchLogs.TabIndex = 15
        Me.SearchLogs.Text = "Search logs"
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 676)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1346, 53)
        Me.Panel5.TabIndex = 20
        '
        'SoftwareLogComponents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.BunifuCards1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "SoftwareLogComponents"
        Me.Size = New System.Drawing.Size(1346, 729)
        Me.BunifuCards1.ResumeLayout(False)
        Me.SplitContainer1.Panel1.ResumeLayout(False)
        Me.SplitContainer1.Panel2.ResumeLayout(False)
        CType(Me.SplitContainer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.SplitContainer1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents LogList As ListView
    Friend WithEvents logid As ColumnHeader
    Friend WithEvents dateandtime As ColumnHeader
    Friend WithEvents Employeeid As ColumnHeader
    Friend WithEvents EmployeePosition As ColumnHeader
    Friend WithEvents typelog As ColumnHeader
    Friend WithEvents descriptionlog As ColumnHeader
    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents SearchBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel5 As Panel
    Friend WithEvents RefreshBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents ExportBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents SearchLogs As TextBox
    Friend WithEvents SplitContainer1 As SplitContainer
    Friend WithEvents Panel1 As Panel
End Class
