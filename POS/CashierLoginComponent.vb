﻿Public Class CashierLoginComponent
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption
    Private Structure LoginAttempt
        Dim LoginAttempt As Integer
        Dim LoginCounterTimer As Integer
    End Structure

    Dim attempt As New LoginAttempt

    Private Sub CashierLoginComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        attempt.LoginAttempt = 0
        attempt.LoginCounterTimer = 60

        Me.enabledInput()
    End Sub

    Private Sub CashierLoginBtn_Click(sender As Object, e As EventArgs) Handles CashierLoginBtn.Click
        loginEvent()
    End Sub

    Private Sub loginEvent()
        If (CashierUsernameInput.Text <> String.Empty Or CashierUsernameInput.Text.ToLower() = "cashier login".ToLower()) Or (CashierPaswordInput.Text <> String.Empty Or CashierPaswordInput.Text.ToLower() = "bunifumaterialtextbox1".ToLower()) Then
            With dbConfig
                Dim loginCashier As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE cashierid = ? AND cashierpassword = ?", CashierUsernameInput.Text, util.setHash(CashierPaswordInput.Text))

                If loginCashier.tables(0).Rows.Count > 0 Then
                    For Each cashierPassword As DataRow In loginCashier.tables(0).Rows
                        If util.verifyHash(util.setHash(cashierPassword("cashierpassword"))) = True Then
                            systemlogs.loginlog(New Dictionary(Of String, String) From {
                                {"LoginId", CashierUsernameInput.Text},
                                {"description", "Account Successfully Login!"},
                                {"type", "success"},
                                {"position", "cashier"}
                            })
                            attempt.LoginAttempt = 0
                            attempt.LoginCounterTimer = 30
                            SS.CashierInstance.id = CashierUsernameInput.Text
                            CashierUsernameInput.Text = String.Empty
                            CashierPaswordInput.Text = String.Empty
                            Login.Hide()
                            SS.ShowDialog()
                        End If
                    Next
                Else
                    If attempt.LoginAttempt < 3 Then
                        MessageBox.Show("Username and/or Password you Entered is Incorrect!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        systemlogs.loginlog(New Dictionary(Of String, String) From {
                            {"LoginId", CashierUsernameInput.Text},
                            {"description", "Username and/or Password you Entered is Incorrect!"},
                            {"type", "error"},
                            {"position", "cashier"}
                        })

                        attempt.LoginAttempt = attempt.LoginAttempt + 1
                        LoginAttemptNotif.Text = "Login Failed " & attempt.LoginAttempt & " attempt."
                        CashierUsernameInput.Text = String.Empty
                        CashierPaswordInput.Text = String.Empty
                    Else
                        MessageBox.Show("Login Maximum 3 Attempt Exceeded!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
                        LoginAttemptNotif.Text = "Login Maximum " & attempt.LoginAttempt & " Attempt Exceeded!"
                        Me.disableInputs()
                        maximumAttemptLoginAuth.ShowDialog()
                        Timer1.Start()
                    End If
                End If
            End With
        Else
            MessageBox.Show("Username and/or Password Must Not be Empty!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error)
        End If
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        attempt.LoginCounterTimer = attempt.LoginCounterTimer - 1
        LoginAttemptNotif.Text = "Login Maximum " & attempt.LoginAttempt & " Attempt Exceeded! " & vbCrLf & "Wait " & attempt.LoginCounterTimer & " seconds to be able to login."

        If attempt.LoginCounterTimer <= 0 Then
            Timer1.Stop()
            Me.enabledInput()
        End If
    End Sub

    Private Sub disableInputs()
        CashierUsernameInput.Text = String.Empty
        CashierPaswordInput.Text = String.Empty

        CashierUsernameInput.Enabled = False
        CashierPaswordInput.Enabled = False
        CashierLoginBtn.Enabled = False

    End Sub
    Public Sub enabledInput()

        CashierUsernameInput.Enabled = True
        CashierPaswordInput.Enabled = True
        CashierLoginBtn.Enabled = True
        attempt.LoginAttempt = 0
        LoginAttemptNotif.Text = String.Empty
    End Sub

    Private Sub CashierUsernameInput_MouseEnter(sender As Object, e As EventArgs) Handles CashierUsernameInput.MouseEnter
        If CashierUsernameInput.Text.ToLower() = "cashier login".ToLower() Then
            CashierUsernameInput.Text = String.Empty
        End If
    End Sub

    Private Sub CashierUsernameInput_MouseLeave(sender As Object, e As EventArgs) Handles CashierUsernameInput.MouseLeave
        If CashierUsernameInput.Text = String.Empty Then
            CashierUsernameInput.Text = "Cashier Login"
        End If
    End Sub

    Private Sub CashierPaswordInput_MouseEnter(sender As Object, e As EventArgs) Handles CashierPaswordInput.MouseEnter
        If CashierPaswordInput.Text.ToLower() = "bunifumaterialtextbox1".ToLower() Then
            CashierPaswordInput.Text = String.Empty
        End If
    End Sub

    Private Sub CashierPaswordInput_MouseLeave(sender As Object, e As EventArgs) Handles CashierPaswordInput.MouseLeave
        If CashierPaswordInput.Text = String.Empty Then
            CashierPaswordInput.isPassword = True
            CashierPaswordInput.Text = "bunifumaterialtextbox1"
        End If
    End Sub
End Class
