﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class AddCashier
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.CashierConfirmPassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.CashierPassword = New System.Windows.Forms.TextBox()
        Me.CancelBtn = New System.Windows.Forms.Button()
        Me.AddCashierBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.CashierLastname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.CashierFirstname = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.AddCashierId = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Label5)
        Me.Panel1.Controls.Add(Me.CashierConfirmPassword)
        Me.Panel1.Controls.Add(Me.Label4)
        Me.Panel1.Controls.Add(Me.CashierPassword)
        Me.Panel1.Controls.Add(Me.CancelBtn)
        Me.Panel1.Controls.Add(Me.AddCashierBtn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.CashierLastname)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.CashierFirstname)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.AddCashierId)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(693, 966)
        Me.Panel1.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(92, 664)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(210, 29)
        Me.Label5.TabIndex = 15
        Me.Label5.Text = "Confirm Password"
        '
        'CashierConfirmPassword
        '
        Me.CashierConfirmPassword.Location = New System.Drawing.Point(97, 716)
        Me.CashierConfirmPassword.Multiline = True
        Me.CashierConfirmPassword.Name = "CashierConfirmPassword"
        Me.CashierConfirmPassword.Size = New System.Drawing.Size(503, 56)
        Me.CashierConfirmPassword.TabIndex = 14
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(92, 536)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(120, 29)
        Me.Label4.TabIndex = 13
        Me.Label4.Text = "Password"
        '
        'CashierPassword
        '
        Me.CashierPassword.Location = New System.Drawing.Point(97, 588)
        Me.CashierPassword.Multiline = True
        Me.CashierPassword.Name = "CashierPassword"
        Me.CashierPassword.Size = New System.Drawing.Size(503, 56)
        Me.CashierPassword.TabIndex = 12
        '
        'CancelBtn
        '
        Me.CancelBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelBtn.Location = New System.Drawing.Point(215, 816)
        Me.CancelBtn.Name = "CancelBtn"
        Me.CancelBtn.Size = New System.Drawing.Size(188, 65)
        Me.CancelBtn.TabIndex = 11
        Me.CancelBtn.Text = "Cancel"
        Me.CancelBtn.UseVisualStyleBackColor = True
        '
        'AddCashierBtn
        '
        Me.AddCashierBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.AddCashierBtn.Location = New System.Drawing.Point(409, 816)
        Me.AddCashierBtn.Name = "AddCashierBtn"
        Me.AddCashierBtn.Size = New System.Drawing.Size(188, 65)
        Me.AddCashierBtn.TabIndex = 10
        Me.AddCashierBtn.Text = "Create"
        Me.AddCashierBtn.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(90, 402)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 29)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Lastname"
        '
        'CashierLastname
        '
        Me.CashierLastname.Location = New System.Drawing.Point(95, 454)
        Me.CashierLastname.Multiline = True
        Me.CashierLastname.Name = "CashierLastname"
        Me.CashierLastname.Size = New System.Drawing.Size(503, 56)
        Me.CashierLastname.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(90, 274)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 29)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Firstname"
        '
        'CashierFirstname
        '
        Me.CashierFirstname.Location = New System.Drawing.Point(95, 326)
        Me.CashierFirstname.Multiline = True
        Me.CashierFirstname.Name = "CashierFirstname"
        Me.CashierFirstname.Size = New System.Drawing.Size(503, 56)
        Me.CashierFirstname.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(86, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(125, 29)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Cashier ID"
        '
        'AddCashierId
        '
        Me.AddCashierId.Location = New System.Drawing.Point(92, 179)
        Me.AddCashierId.MaxLength = 8
        Me.AddCashierId.Multiline = True
        Me.AddCashierId.Name = "AddCashierId"
        Me.AddCashierId.Size = New System.Drawing.Size(503, 56)
        Me.AddCashierId.TabIndex = 4
        '
        'AddCashier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 966)
        Me.Controls.Add(Me.Panel1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "AddCashier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "AddCashier"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Label5 As Label
    Friend WithEvents CashierConfirmPassword As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents CashierPassword As TextBox
    Friend WithEvents CancelBtn As Button
    Friend WithEvents AddCashierBtn As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents CashierLastname As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents CashierFirstname As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents AddCashierId As TextBox
End Class
