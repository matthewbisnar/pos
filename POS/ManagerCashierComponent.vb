﻿Public Class ManagerCashierComponent
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Private Sub cashierAccountsfunction()
        Dim fetchCashier As Object = dbConfig.Process_qry("SELECT * FROM cashier")

        If fetchCashier.tables(0).Rows.Count > 0 Then

            CashierList.Items.Clear()

            For Each Cashiers As DataRow In fetchCashier.tables(0).rows
                With CashierList.Items.Add(Cashiers("cashierid"))
                    .SubItems.Add(Cashiers("firstname"))
                    .SubItems.Add(Cashiers("lastname"))
                    .SubItems.Add(Cashiers("position"))
                    .SubItems.Add(Cashiers("createdate"))
                End With
            Next
        Else
            CashierList.Items.Clear()
        End If
    End Sub

    Private Sub ManagerCashierComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.cashierAccountsfunction()
    End Sub

    Private Sub SearchCashierBtn_Click(sender As Object, e As EventArgs) Handles SearchCashierBtn.Click
        Dim fetchCashierSearch As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE firstname = ? OR lastname = ? OR cashierid = ?", SearchCashier.Text, SearchCashier.Text, SearchCashier.Text)

        If fetchCashierSearch.tables(0).Rows.Count > 0 Then

            CashierList.Items.Clear()

            For Each cashiers As DataRow In fetchCashierSearch.tables(0).rows
                With CashierList.Items.Add(cashiers("cashierid"))
                    .SubItems.Add(cashiers("firstname"))
                    .SubItems.Add(cashiers("lastname"))
                    .SubItems.Add(cashiers("position"))
                    .SubItems.Add(cashiers("createdate"))
                End With
            Next
        Else
            CashierList.Items.Clear()
        End If
    End Sub

    Private Sub AddCashierbtn1_Click(sender As Object, e As EventArgs) Handles AddCashierbtn1.Click
        AddCashier.ShowDialog()
    End Sub

    Private Sub EditCashierBtn1_Click(sender As Object, e As EventArgs) Handles EditCashierBtn1.Click
        Try
            ChangeCashierAccountForManager.EditCashierInstance.CashierId = CashierList.SelectedItems(0).SubItems(0).Text
            ChangeCashierAccountForManager.ShowDialog()
        Catch ex As Exception
            MessageBox.Show("Please Select Cashier ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub DeleteCashierBtn1_Click(sender As Object, e As EventArgs) Handles DeleteCashierBtn1.Click
        Try
            If MessageBox.Show("Are you sure you want to delete Cashier " & CashierList.SelectedItems(0).SubItems(0).Text & " ?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.OK Then
                Dim deleteManager As Object = dbConfig.Process_qry("DELETE FROM cashier WHERE cashierid = ?", CashierList.SelectedItems(0).SubItems(0).Text)

                If MessageBox.Show("Cashier Successfully deleted, refresh to view Cashier list.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    systemlogs.loginlog(New Dictionary(Of String, String) From {
                               {"LoginId", CashierList.SelectedItems(0).SubItems(0).Text},
                               {"description", "Account Successfully deleted!"},
                               {"type", "success"},
                               {"position", "cashier"}
                            })
                    Me.cashierAccountsfunction()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Please Select Cashier ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub RefreshCashier1_Click(sender As Object, e As EventArgs) Handles RefreshCashier1.Click
        Me.cashierAccountsfunction()
    End Sub
End Class
