﻿Public Class SoftwareDashboardComponent
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Private Sub SoftwareDashboardComponent_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.managerAccountsFunction()
    End Sub

    Private Sub CashierAccount_Click(sender As Object, e As EventArgs) Handles CashierAccount.Click
        Me.cashierAccountsfunction()
    End Sub

    Private Sub ManagerAccount_Click(sender As Object, e As EventArgs) Handles ManagerAccount.Click
        Me.managerAccountsFunction()
    End Sub

    Private Sub cashierAccountsfunction()
        Dim fetchCashier As Object = dbConfig.Process_qry("SELECT * FROM cashier")

        If fetchCashier.tables(0).Rows.Count > 0 Then

            CashierList.Items.Clear()

            For Each Cashiers As DataRow In fetchCashier.tables(0).rows
                With CashierList.Items.Add(Cashiers("cashierid"))
                    .SubItems.Add(Cashiers("firstname"))
                    .SubItems.Add(Cashiers("lastname"))
                    .SubItems.Add(Cashiers("position"))
                    .SubItems.Add(Cashiers("createdate"))
                End With
            Next
        Else
            CashierList.Items.Clear()
        End If
    End Sub

    Private Sub managerAccountsFunction()
        Dim fetchManager As Object = dbConfig.Process_qry("SELECT * FROM manager")

        If fetchManager.tables(0).Rows.Count > 0 Then

            LogList.Items.Clear()

            For Each Managers As DataRow In fetchManager.tables(0).rows
                With LogList.Items.Add(Managers("managerid"))
                    .subItems.add(Managers("firstname"))
                    .subItems.add(Managers("lastname"))
                    .subItems.add(Managers("position"))
                    .subItems.add(Managers("createdate"))
                End With
            Next
        Else
            LogList.Items.Clear()
        End If
    End Sub

    Private Sub AddCashierbtn_Click(sender As Object, e As EventArgs)
        AddCashier.ShowDialog()
    End Sub

    'Cashier Search
    Private Sub SearchCashierBtn_Click_1(sender As Object, e As EventArgs) Handles SearchCashierBtn.Click
        Dim fetchCashierSearch As Object = dbConfig.Process_qry("SELECT * FROM cashier WHERE firstname = ? OR lastname = ? OR cashierid = ?", SearchCashier.Text, SearchCashier.Text, SearchCashier.Text)

        If fetchCashierSearch.tables(0).Rows.Count > 0 Then

            CashierList.Items.Clear()

            For Each cashiers As DataRow In fetchCashierSearch.tables(0).rows
                With CashierList.Items.Add(cashiers("cashierid"))
                    .SubItems.Add(cashiers("firstname"))
                    .SubItems.Add(cashiers("lastname"))
                    .SubItems.Add(cashiers("position"))
                    .SubItems.Add(cashiers("createdate"))
                End With
            Next
        Else
            CashierList.Items.Clear()
        End If
    End Sub

    Private Sub AddCashierbtn1_Click(sender As Object, e As EventArgs) Handles AddCashierbtn1.Click
        AddCashier.ShowDialog()
    End Sub

    Private Sub EditCashierBtn1_Click(sender As Object, e As EventArgs) Handles EditCashierBtn1.Click
        Try
            editcashier.EditCashierInstance.CashierId = CashierList.SelectedItems(0).SubItems(0).Text
            editcashier.ShowDialog()
        Catch ex As Exception
            MessageBox.Show("Please Select Cashier ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub DeleteCashierBtn1_Click(sender As Object, e As EventArgs) Handles DeleteCashierBtn1.Click
        Try
            If MessageBox.Show("Are you sure you want to delete Cashier " & CashierList.SelectedItems(0).SubItems(0).Text & " ?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.OK Then
                Dim deleteManager As Object = dbConfig.Process_qry("DELETE FROM cashier WHERE cashierid = ?", CashierList.SelectedItems(0).SubItems(0).Text)

                If MessageBox.Show("Cashier Successfully deleted, refresh to view Cashier list.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    systemlogs.loginlog(New Dictionary(Of String, String) From {
                               {"LoginId", CashierList.SelectedItems(0).SubItems(0).Text},
                               {"description", "Account Successfully deleted!"},
                               {"type", "success"},
                               {"position", "cashier"}
                            })
                    Me.cashierAccountsfunction()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Please Select Cashier ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub RefreshCashier1_Click(sender As Object, e As EventArgs) Handles RefreshCashier1.Click
        Me.cashierAccountsfunction()
    End Sub

    Private Sub BunifuFlatButton1_Click(sender As Object, e As EventArgs) Handles BunifuFlatButton1.Click
        Dim fetchSearchAccount As Object = dbConfig.Process_qry("SELECT * FROM manager WHERE firstname = ? OR lastname = ? OR managerid = ?", SearchManager.Text, SearchManager.Text, SearchManager.Text)

        If fetchSearchAccount.tables(0).Rows.Count > 0 Then

            LogList.Items.Clear()
            For Each managers As DataRow In fetchSearchAccount.tables(0).rows
                With LogList.Items.Add(managers("managerid"))
                    .subItems.add(managers("firstname"))
                    .subItems.add(managers("lastname"))
                    .subItems.add(managers("position"))
                    .subItems.add(managers("createdate"))
                End With
            Next
        Else
            LogList.Items.Clear()
        End If
    End Sub

    Private Sub AddManagerBtn1_Click(sender As Object, e As EventArgs) Handles AddManagerBtn1.Click
        Addmanager.ShowDialog()
    End Sub

    Private Sub EditManagerBtn1_Click(sender As Object, e As EventArgs) Handles EditManagerBtn1.Click
        Try
            EditManager.EditManagerInstance.ManagerId = LogList.SelectedItems(0).SubItems(0).Text
            EditManager.ShowDialog()
        Catch ex As Exception
            MessageBox.Show("Please Select Manager ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub DeleteManagerBtn1_Click(sender As Object, e As EventArgs) Handles DeleteManagerBtn1.Click
        Try
            If MessageBox.Show("Are you sure you want to delete Manager " & LogList.SelectedItems(0).SubItems(0).Text & " ?", "Warning", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = DialogResult.OK Then
                Dim deleteManager As Object = dbConfig.Process_qry("DELETE FROM manager WHERE managerid = ?", LogList.SelectedItems(0).SubItems(0).Text)

                If MessageBox.Show("Manager Successfully deleted, refresh to view manager list.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information) = DialogResult.OK Then
                    systemlogs.loginlog(New Dictionary(Of String, String) From {
                               {"LoginId", LogList.SelectedItems(0).SubItems(0).Text},
                               {"description", "Account Successfully deleted!"},
                               {"type", "success"},
                               {"position", "manager"}
                            })
                    Me.managerAccountsFunction()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show("Please Select Manager ID", "Error", MessageBoxButtons.OK, MessageBoxIcon.Hand)
        End Try
    End Sub

    Private Sub RefreshManagerBtn1_Click(sender As Object, e As EventArgs) Handles RefreshManagerBtn1.Click
        Me.managerAccountsFunction()
    End Sub

    Private Sub CashierList_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CashierList.SelectedIndexChanged

    End Sub
End Class
