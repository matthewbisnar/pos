﻿Public Class SoftwareAdminDashboard
    Private dbConfig As New Api.Configs
    Private systemlogs As New Api.logs
    Private util As New Api.md5Encryption

    Public Structure SoftwareAdmin
        Dim id As String
        Dim firstname As String
        Dim lastname As String
        Dim position As String
    End Structure

    Public SoftwareAdminInstance As New SoftwareAdmin
    Private Sub SoftwareAdminDashboard_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        SoftwareDashboardComponent1.BringToFront()
        Timer1.Start()
        Me.loadAccountinfo()
    End Sub

    Private Sub loadAccountinfo()
        Dim fetchSoftwareAdmin As Object = dbConfig.Process_qry("SELECT * FROM softwareadmin WHERE adminid = ?", SoftwareAdminInstance.id)

        If fetchSoftwareAdmin.tables(0).Rows.Count > 0 Then
            For Each fetch As DataRow In fetchSoftwareAdmin.tables(0).rows
                SoftwareAdminInstance.firstname = fetch("adminname")
            Next
        End If

        AccountName.Text = SoftwareAdminInstance.firstname & " (admin)"
    End Sub

    Private Sub SoftwareAdminLogout_Click(sender As Object, e As EventArgs)
        Login.Show()
        MyBase.Hide()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        timezone.Text = "Time zone: " & Format(Now, "dd/mm/yyyy hh:mm:ss tt")
    End Sub

    Private Sub DashboardBtn_Click(sender As Object, e As EventArgs) Handles SoftwareDashboard.Click
        SoftwareDashboardComponent1.BringToFront()
    End Sub

    Private Sub ActivityLogs_Click(sender As Object, e As EventArgs) Handles ActivityLogs.Click
        SoftwareLogComponents1.BringToFront()
    End Sub

    Private Sub SoftwareAdminLogout_Click_1(sender As Object, e As EventArgs) Handles SoftwareAdminLogout.Click
        MyBase.Hide()
        Login.Show()
    End Sub

    Private Sub SoftwareAdminSettings_Click(sender As Object, e As EventArgs) Handles SoftwareAdminSettings.Click
        SoftwareAdminSettingsComponent1.BringToFront()
    End Sub
End Class