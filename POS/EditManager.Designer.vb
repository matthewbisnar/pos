﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class EditManager
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lable4 = New System.Windows.Forms.Label()
        Me.EditCashierPosition = New System.Windows.Forms.TextBox()
        Me.CancelBtn = New System.Windows.Forms.Button()
        Me.EditCashierBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.EditCashierLastname = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.EditCashierFirstname = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.EditCashierId = New System.Windows.Forms.TextBox()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.CancelBtn1 = New System.Windows.Forms.Button()
        Me.ChangeCashierpassword = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.ConfirmChangePassword = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ManagerChangePassword = New System.Windows.Forms.TextBox()
        Me.Panel1.SuspendLayout()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lable4)
        Me.Panel1.Controls.Add(Me.EditCashierPosition)
        Me.Panel1.Controls.Add(Me.CancelBtn)
        Me.Panel1.Controls.Add(Me.EditCashierBtn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.EditCashierLastname)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.EditCashierFirstname)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.EditCashierId)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(3, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(679, 902)
        Me.Panel1.TabIndex = 1
        '
        'lable4
        '
        Me.lable4.AutoSize = True
        Me.lable4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lable4.Location = New System.Drawing.Point(89, 279)
        Me.lable4.Name = "lable4"
        Me.lable4.Size = New System.Drawing.Size(100, 29)
        Me.lable4.TabIndex = 13
        Me.lable4.Text = "Position"
        '
        'EditCashierPosition
        '
        Me.EditCashierPosition.Location = New System.Drawing.Point(94, 331)
        Me.EditCashierPosition.Multiline = True
        Me.EditCashierPosition.Name = "EditCashierPosition"
        Me.EditCashierPosition.Size = New System.Drawing.Size(503, 56)
        Me.EditCashierPosition.TabIndex = 12
        '
        'CancelBtn
        '
        Me.CancelBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelBtn.Location = New System.Drawing.Point(215, 772)
        Me.CancelBtn.Name = "CancelBtn"
        Me.CancelBtn.Size = New System.Drawing.Size(188, 65)
        Me.CancelBtn.TabIndex = 11
        Me.CancelBtn.Text = "Cancel"
        Me.CancelBtn.UseVisualStyleBackColor = True
        '
        'EditCashierBtn
        '
        Me.EditCashierBtn.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.EditCashierBtn.Location = New System.Drawing.Point(409, 772)
        Me.EditCashierBtn.Name = "EditCashierBtn"
        Me.EditCashierBtn.Size = New System.Drawing.Size(188, 65)
        Me.EditCashierBtn.TabIndex = 10
        Me.EditCashierBtn.Text = "Edit"
        Me.EditCashierBtn.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(87, 559)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(117, 29)
        Me.Label3.TabIndex = 9
        Me.Label3.Text = "Lastname"
        '
        'EditCashierLastname
        '
        Me.EditCashierLastname.Location = New System.Drawing.Point(92, 611)
        Me.EditCashierLastname.Multiline = True
        Me.EditCashierLastname.Name = "EditCashierLastname"
        Me.EditCashierLastname.Size = New System.Drawing.Size(503, 56)
        Me.EditCashierLastname.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(87, 431)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(120, 29)
        Me.Label1.TabIndex = 7
        Me.Label1.Text = "Firstname"
        '
        'EditCashierFirstname
        '
        Me.EditCashierFirstname.Location = New System.Drawing.Point(92, 483)
        Me.EditCashierFirstname.Multiline = True
        Me.EditCashierFirstname.Name = "EditCashierFirstname"
        Me.EditCashierFirstname.Size = New System.Drawing.Size(503, 56)
        Me.EditCashierFirstname.TabIndex = 6
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(86, 127)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 29)
        Me.Label2.TabIndex = 5
        Me.Label2.Text = "Admin ID"
        '
        'EditCashierId
        '
        Me.EditCashierId.Location = New System.Drawing.Point(92, 179)
        Me.EditCashierId.MaxLength = 8
        Me.EditCashierId.Multiline = True
        Me.EditCashierId.Name = "EditCashierId"
        Me.EditCashierId.Size = New System.Drawing.Size(503, 56)
        Me.EditCashierId.TabIndex = 4
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TabControl1.ItemSize = New System.Drawing.Size(120, 50)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(693, 966)
        Me.TabControl1.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.Panel1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 54)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(685, 908)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Change Information"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.CancelBtn1)
        Me.TabPage2.Controls.Add(Me.ChangeCashierpassword)
        Me.TabPage2.Controls.Add(Me.Label5)
        Me.TabPage2.Controls.Add(Me.ConfirmChangePassword)
        Me.TabPage2.Controls.Add(Me.Label4)
        Me.TabPage2.Controls.Add(Me.ManagerChangePassword)
        Me.TabPage2.Location = New System.Drawing.Point(4, 54)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(685, 908)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Change Password"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'CancelBtn1
        '
        Me.CancelBtn1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CancelBtn1.Location = New System.Drawing.Point(87, 522)
        Me.CancelBtn1.Name = "CancelBtn1"
        Me.CancelBtn1.Size = New System.Drawing.Size(242, 65)
        Me.CancelBtn1.TabIndex = 21
        Me.CancelBtn1.Text = "Cancel"
        Me.CancelBtn1.UseVisualStyleBackColor = True
        '
        'ChangeCashierpassword
        '
        Me.ChangeCashierpassword.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ChangeCashierpassword.Location = New System.Drawing.Point(335, 522)
        Me.ChangeCashierpassword.Name = "ChangeCashierpassword"
        Me.ChangeCashierpassword.Size = New System.Drawing.Size(254, 65)
        Me.ChangeCashierpassword.TabIndex = 20
        Me.ChangeCashierpassword.Text = "Change Password"
        Me.ChangeCashierpassword.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(82, 350)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(210, 29)
        Me.Label5.TabIndex = 19
        Me.Label5.Text = "Confirm Password"
        '
        'ConfirmChangePassword
        '
        Me.ConfirmChangePassword.Location = New System.Drawing.Point(87, 402)
        Me.ConfirmChangePassword.Multiline = True
        Me.ConfirmChangePassword.Name = "ConfirmChangePassword"
        Me.ConfirmChangePassword.Size = New System.Drawing.Size(503, 56)
        Me.ConfirmChangePassword.TabIndex = 18
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(82, 222)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(210, 29)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Change Password"
        '
        'ManagerChangePassword
        '
        Me.ManagerChangePassword.Location = New System.Drawing.Point(87, 274)
        Me.ManagerChangePassword.Multiline = True
        Me.ManagerChangePassword.Name = "ManagerChangePassword"
        Me.ManagerChangePassword.Size = New System.Drawing.Size(503, 56)
        Me.ManagerChangePassword.TabIndex = 16
        '
        'EditManager
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(693, 966)
        Me.Controls.Add(Me.TabControl1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "EditManager"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "EditManager"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents CancelBtn As Button
    Friend WithEvents EditCashierBtn As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents EditCashierLastname As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents EditCashierFirstname As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents EditCashierId As TextBox
    Friend WithEvents lable4 As Label
    Friend WithEvents EditCashierPosition As TextBox
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents Label5 As Label
    Friend WithEvents ConfirmChangePassword As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents ManagerChangePassword As TextBox
    Friend WithEvents CancelBtn1 As Button
    Friend WithEvents ChangeCashierpassword As Button
End Class
