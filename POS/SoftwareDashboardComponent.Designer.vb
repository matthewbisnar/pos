﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class SoftwareDashboardComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SoftwareDashboardComponent))
        Me.LogList = New System.Windows.Forms.ListView()
        Me.EmployeeId = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.EmployeeFirstname = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.EmployeeLastname = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.EmployeePosition = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.DateCreated = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.SearchManager = New System.Windows.Forms.TextBox()
        Me.CashierAccount = New System.Windows.Forms.TabControl()
        Me.ManagerAccount = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.BunifuFlatButton1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.RefreshManagerBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.DeleteManagerBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.EditManagerBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddManagerBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.CashierAccounts = New System.Windows.Forms.TabPage()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.SearchCashierBtn = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.SearchCashier = New System.Windows.Forms.TextBox()
        Me.CashierList = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.EditCashierBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.RefreshCashier1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.DeleteCashierBtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.AddCashierbtn1 = New Bunifu.Framework.UI.BunifuFlatButton()
        Me.BunifuCards1 = New Bunifu.Framework.UI.BunifuCards()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.CashierAccount.SuspendLayout()
        Me.ManagerAccount.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.CashierAccounts.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.BunifuCards1.SuspendLayout()
        Me.SuspendLayout()
        '
        'LogList
        '
        Me.LogList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.EmployeeId, Me.EmployeeFirstname, Me.EmployeeLastname, Me.EmployeePosition, Me.DateCreated})
        Me.LogList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.LogList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LogList.FullRowSelect = True
        Me.LogList.GridLines = True
        Me.LogList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.LogList.HideSelection = False
        Me.LogList.Location = New System.Drawing.Point(0, 51)
        Me.LogList.Margin = New System.Windows.Forms.Padding(0)
        Me.LogList.Name = "LogList"
        Me.LogList.Size = New System.Drawing.Size(1115, 533)
        Me.LogList.TabIndex = 2
        Me.LogList.UseCompatibleStateImageBehavior = False
        Me.LogList.View = System.Windows.Forms.View.Details
        '
        'EmployeeId
        '
        Me.EmployeeId.Text = "Employee ID"
        Me.EmployeeId.Width = 631
        '
        'EmployeeFirstname
        '
        Me.EmployeeFirstname.Text = "Firstname"
        Me.EmployeeFirstname.Width = 595
        '
        'EmployeeLastname
        '
        Me.EmployeeLastname.Text = "Lastname"
        Me.EmployeeLastname.Width = 793
        '
        'EmployeePosition
        '
        Me.EmployeePosition.Text = "Position"
        Me.EmployeePosition.Width = 718
        '
        'DateCreated
        '
        Me.DateCreated.Text = "Date Created"
        Me.DateCreated.Width = 733
        '
        'SearchManager
        '
        Me.SearchManager.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SearchManager.Location = New System.Drawing.Point(0, 0)
        Me.SearchManager.Margin = New System.Windows.Forms.Padding(0)
        Me.SearchManager.Multiline = True
        Me.SearchManager.Name = "SearchManager"
        Me.SearchManager.Size = New System.Drawing.Size(1115, 51)
        Me.SearchManager.TabIndex = 12
        Me.SearchManager.Text = "Search items..."
        '
        'CashierAccount
        '
        Me.CashierAccount.Controls.Add(Me.ManagerAccount)
        Me.CashierAccount.Controls.Add(Me.CashierAccounts)
        Me.CashierAccount.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierAccount.ItemSize = New System.Drawing.Size(110, 40)
        Me.CashierAccount.Location = New System.Drawing.Point(0, 0)
        Me.CashierAccount.Margin = New System.Windows.Forms.Padding(0)
        Me.CashierAccount.Name = "CashierAccount"
        Me.CashierAccount.Padding = New System.Drawing.Point(0, 0)
        Me.CashierAccount.SelectedIndex = 0
        Me.CashierAccount.Size = New System.Drawing.Size(1319, 637)
        Me.CashierAccount.TabIndex = 14
        '
        'ManagerAccount
        '
        Me.ManagerAccount.Controls.Add(Me.TableLayoutPanel2)
        Me.ManagerAccount.Location = New System.Drawing.Point(4, 44)
        Me.ManagerAccount.Margin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.ManagerAccount.Name = "ManagerAccount"
        Me.ManagerAccount.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
        Me.ManagerAccount.Size = New System.Drawing.Size(1311, 589)
        Me.ManagerAccount.TabIndex = 0
        Me.ManagerAccount.Text = "Manager Account"
        Me.ManagerAccount.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 85.04958!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.95042!))
        Me.TableLayoutPanel2.Controls.Add(Me.BunifuFlatButton1, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.SearchManager, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.LogList, 0, 1)
        Me.TableLayoutPanel2.Controls.Add(Me.Panel3, 1, 1)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 5)
        Me.TableLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 2
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 8.732877!))
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 91.26712!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(1311, 584)
        Me.TableLayoutPanel2.TabIndex = 0
        '
        'BunifuFlatButton1
        '
        Me.BunifuFlatButton1.Active = False
        Me.BunifuFlatButton1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.BunifuFlatButton1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.BunifuFlatButton1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.BunifuFlatButton1.BorderRadius = 0
        Me.BunifuFlatButton1.ButtonText = "Search"
        Me.BunifuFlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.BunifuFlatButton1.DisabledColor = System.Drawing.Color.Gray
        Me.BunifuFlatButton1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.BunifuFlatButton1.Iconcolor = System.Drawing.Color.Transparent
        Me.BunifuFlatButton1.Iconimage = CType(resources.GetObject("BunifuFlatButton1.Iconimage"), System.Drawing.Image)
        Me.BunifuFlatButton1.Iconimage_right = Nothing
        Me.BunifuFlatButton1.Iconimage_right_Selected = Nothing
        Me.BunifuFlatButton1.Iconimage_Selected = Nothing
        Me.BunifuFlatButton1.IconMarginLeft = 0
        Me.BunifuFlatButton1.IconMarginRight = 0
        Me.BunifuFlatButton1.IconRightVisible = False
        Me.BunifuFlatButton1.IconRightZoom = 0R
        Me.BunifuFlatButton1.IconVisible = False
        Me.BunifuFlatButton1.IconZoom = 90.0R
        Me.BunifuFlatButton1.IsTab = False
        Me.BunifuFlatButton1.Location = New System.Drawing.Point(1115, 0)
        Me.BunifuFlatButton1.Margin = New System.Windows.Forms.Padding(0)
        Me.BunifuFlatButton1.Name = "BunifuFlatButton1"
        Me.BunifuFlatButton1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.BunifuFlatButton1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.BunifuFlatButton1.OnHoverTextColor = System.Drawing.Color.White
        Me.BunifuFlatButton1.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.BunifuFlatButton1.selected = False
        Me.BunifuFlatButton1.Size = New System.Drawing.Size(196, 51)
        Me.BunifuFlatButton1.TabIndex = 19
        Me.BunifuFlatButton1.Text = "Search"
        Me.BunifuFlatButton1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BunifuFlatButton1.Textcolor = System.Drawing.Color.White
        Me.BunifuFlatButton1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.RefreshManagerBtn1)
        Me.Panel3.Controls.Add(Me.DeleteManagerBtn1)
        Me.Panel3.Controls.Add(Me.EditManagerBtn1)
        Me.Panel3.Controls.Add(Me.AddManagerBtn1)
        Me.Panel3.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel3.Location = New System.Drawing.Point(1115, 51)
        Me.Panel3.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(196, 533)
        Me.Panel3.TabIndex = 20
        '
        'RefreshManagerBtn1
        '
        Me.RefreshManagerBtn1.Active = False
        Me.RefreshManagerBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshManagerBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshManagerBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RefreshManagerBtn1.BorderRadius = 0
        Me.RefreshManagerBtn1.ButtonText = "Refresh"
        Me.RefreshManagerBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshManagerBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.RefreshManagerBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.RefreshManagerBtn1.Iconimage = CType(resources.GetObject("RefreshManagerBtn1.Iconimage"), System.Drawing.Image)
        Me.RefreshManagerBtn1.Iconimage_right = Nothing
        Me.RefreshManagerBtn1.Iconimage_right_Selected = Nothing
        Me.RefreshManagerBtn1.Iconimage_Selected = Nothing
        Me.RefreshManagerBtn1.IconMarginLeft = 0
        Me.RefreshManagerBtn1.IconMarginRight = 0
        Me.RefreshManagerBtn1.IconRightVisible = False
        Me.RefreshManagerBtn1.IconRightZoom = 0R
        Me.RefreshManagerBtn1.IconVisible = False
        Me.RefreshManagerBtn1.IconZoom = 90.0R
        Me.RefreshManagerBtn1.IsTab = False
        Me.RefreshManagerBtn1.Location = New System.Drawing.Point(0, 189)
        Me.RefreshManagerBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.RefreshManagerBtn1.Name = "RefreshManagerBtn1"
        Me.RefreshManagerBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshManagerBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.RefreshManagerBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.RefreshManagerBtn1.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.RefreshManagerBtn1.selected = False
        Me.RefreshManagerBtn1.Size = New System.Drawing.Size(199, 63)
        Me.RefreshManagerBtn1.TabIndex = 23
        Me.RefreshManagerBtn1.Text = "Refresh"
        Me.RefreshManagerBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RefreshManagerBtn1.Textcolor = System.Drawing.Color.White
        Me.RefreshManagerBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'DeleteManagerBtn1
        '
        Me.DeleteManagerBtn1.Active = False
        Me.DeleteManagerBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteManagerBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteManagerBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DeleteManagerBtn1.BorderRadius = 0
        Me.DeleteManagerBtn1.ButtonText = "Delete Manager"
        Me.DeleteManagerBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteManagerBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.DeleteManagerBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.DeleteManagerBtn1.Iconimage = CType(resources.GetObject("DeleteManagerBtn1.Iconimage"), System.Drawing.Image)
        Me.DeleteManagerBtn1.Iconimage_right = Nothing
        Me.DeleteManagerBtn1.Iconimage_right_Selected = Nothing
        Me.DeleteManagerBtn1.Iconimage_Selected = Nothing
        Me.DeleteManagerBtn1.IconMarginLeft = 0
        Me.DeleteManagerBtn1.IconMarginRight = 0
        Me.DeleteManagerBtn1.IconRightVisible = False
        Me.DeleteManagerBtn1.IconRightZoom = 0R
        Me.DeleteManagerBtn1.IconVisible = False
        Me.DeleteManagerBtn1.IconZoom = 90.0R
        Me.DeleteManagerBtn1.IsTab = False
        Me.DeleteManagerBtn1.Location = New System.Drawing.Point(0, 126)
        Me.DeleteManagerBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.DeleteManagerBtn1.Name = "DeleteManagerBtn1"
        Me.DeleteManagerBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteManagerBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.DeleteManagerBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.DeleteManagerBtn1.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.DeleteManagerBtn1.selected = False
        Me.DeleteManagerBtn1.Size = New System.Drawing.Size(199, 63)
        Me.DeleteManagerBtn1.TabIndex = 22
        Me.DeleteManagerBtn1.Text = "Delete Manager"
        Me.DeleteManagerBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DeleteManagerBtn1.Textcolor = System.Drawing.Color.White
        Me.DeleteManagerBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'EditManagerBtn1
        '
        Me.EditManagerBtn1.Active = False
        Me.EditManagerBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditManagerBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditManagerBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.EditManagerBtn1.BorderRadius = 0
        Me.EditManagerBtn1.ButtonText = "Edit Manager"
        Me.EditManagerBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditManagerBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.EditManagerBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.EditManagerBtn1.Iconimage = CType(resources.GetObject("EditManagerBtn1.Iconimage"), System.Drawing.Image)
        Me.EditManagerBtn1.Iconimage_right = Nothing
        Me.EditManagerBtn1.Iconimage_right_Selected = Nothing
        Me.EditManagerBtn1.Iconimage_Selected = Nothing
        Me.EditManagerBtn1.IconMarginLeft = 0
        Me.EditManagerBtn1.IconMarginRight = 0
        Me.EditManagerBtn1.IconRightVisible = False
        Me.EditManagerBtn1.IconRightZoom = 0R
        Me.EditManagerBtn1.IconVisible = False
        Me.EditManagerBtn1.IconZoom = 90.0R
        Me.EditManagerBtn1.IsTab = False
        Me.EditManagerBtn1.Location = New System.Drawing.Point(0, 63)
        Me.EditManagerBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.EditManagerBtn1.Name = "EditManagerBtn1"
        Me.EditManagerBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditManagerBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.EditManagerBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.EditManagerBtn1.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.EditManagerBtn1.selected = False
        Me.EditManagerBtn1.Size = New System.Drawing.Size(199, 63)
        Me.EditManagerBtn1.TabIndex = 21
        Me.EditManagerBtn1.Text = "Edit Manager"
        Me.EditManagerBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.EditManagerBtn1.Textcolor = System.Drawing.Color.White
        Me.EditManagerBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddManagerBtn1
        '
        Me.AddManagerBtn1.Active = False
        Me.AddManagerBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddManagerBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddManagerBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddManagerBtn1.BorderRadius = 0
        Me.AddManagerBtn1.ButtonText = "Add Manager"
        Me.AddManagerBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddManagerBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.AddManagerBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.AddManagerBtn1.Iconimage = CType(resources.GetObject("AddManagerBtn1.Iconimage"), System.Drawing.Image)
        Me.AddManagerBtn1.Iconimage_right = Nothing
        Me.AddManagerBtn1.Iconimage_right_Selected = Nothing
        Me.AddManagerBtn1.Iconimage_Selected = Nothing
        Me.AddManagerBtn1.IconMarginLeft = 0
        Me.AddManagerBtn1.IconMarginRight = 0
        Me.AddManagerBtn1.IconRightVisible = False
        Me.AddManagerBtn1.IconRightZoom = 0R
        Me.AddManagerBtn1.IconVisible = False
        Me.AddManagerBtn1.IconZoom = 90.0R
        Me.AddManagerBtn1.IsTab = False
        Me.AddManagerBtn1.Location = New System.Drawing.Point(0, 0)
        Me.AddManagerBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.AddManagerBtn1.Name = "AddManagerBtn1"
        Me.AddManagerBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddManagerBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.AddManagerBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.AddManagerBtn1.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.AddManagerBtn1.selected = False
        Me.AddManagerBtn1.Size = New System.Drawing.Size(199, 63)
        Me.AddManagerBtn1.TabIndex = 20
        Me.AddManagerBtn1.Text = "Add Manager"
        Me.AddManagerBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddManagerBtn1.Textcolor = System.Drawing.Color.White
        Me.AddManagerBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'CashierAccounts
        '
        Me.CashierAccounts.Controls.Add(Me.TableLayoutPanel1)
        Me.CashierAccounts.Location = New System.Drawing.Point(4, 44)
        Me.CashierAccounts.Margin = New System.Windows.Forms.Padding(0)
        Me.CashierAccounts.Name = "CashierAccounts"
        Me.CashierAccounts.Padding = New System.Windows.Forms.Padding(0, 5, 0, 0)
        Me.CashierAccounts.Size = New System.Drawing.Size(1311, 589)
        Me.CashierAccounts.TabIndex = 1
        Me.CashierAccounts.Text = "Cashier Account"
        Me.CashierAccounts.UseVisualStyleBackColor = True
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 86.11987!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 195.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.SearchCashierBtn, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.CashierList, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.Panel2, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 5)
        Me.TableLayoutPanel1.Margin = New System.Windows.Forms.Padding(0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 533.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(1311, 584)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'SearchCashierBtn
        '
        Me.SearchCashierBtn.Active = False
        Me.SearchCashierBtn.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchCashierBtn.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchCashierBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.SearchCashierBtn.BorderRadius = 0
        Me.SearchCashierBtn.ButtonText = "Search"
        Me.SearchCashierBtn.Cursor = System.Windows.Forms.Cursors.Hand
        Me.SearchCashierBtn.DisabledColor = System.Drawing.Color.Gray
        Me.SearchCashierBtn.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SearchCashierBtn.Iconcolor = System.Drawing.Color.Transparent
        Me.SearchCashierBtn.Iconimage = CType(resources.GetObject("SearchCashierBtn.Iconimage"), System.Drawing.Image)
        Me.SearchCashierBtn.Iconimage_right = Nothing
        Me.SearchCashierBtn.Iconimage_right_Selected = Nothing
        Me.SearchCashierBtn.Iconimage_Selected = Nothing
        Me.SearchCashierBtn.IconMarginLeft = 0
        Me.SearchCashierBtn.IconMarginRight = 0
        Me.SearchCashierBtn.IconRightVisible = False
        Me.SearchCashierBtn.IconRightZoom = 0R
        Me.SearchCashierBtn.IconVisible = False
        Me.SearchCashierBtn.IconZoom = 90.0R
        Me.SearchCashierBtn.IsTab = False
        Me.SearchCashierBtn.Location = New System.Drawing.Point(1116, 0)
        Me.SearchCashierBtn.Margin = New System.Windows.Forms.Padding(0)
        Me.SearchCashierBtn.Name = "SearchCashierBtn"
        Me.SearchCashierBtn.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.SearchCashierBtn.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.SearchCashierBtn.OnHoverTextColor = System.Drawing.Color.White
        Me.SearchCashierBtn.Padding = New System.Windows.Forms.Padding(7, 6, 7, 6)
        Me.SearchCashierBtn.selected = False
        Me.SearchCashierBtn.Size = New System.Drawing.Size(195, 51)
        Me.SearchCashierBtn.TabIndex = 18
        Me.SearchCashierBtn.Text = "Search"
        Me.SearchCashierBtn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.SearchCashierBtn.Textcolor = System.Drawing.Color.White
        Me.SearchCashierBtn.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.SearchCashier)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel1.Location = New System.Drawing.Point(0, 0)
        Me.Panel1.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(1116, 51)
        Me.Panel1.TabIndex = 19
        '
        'SearchCashier
        '
        Me.SearchCashier.Dock = System.Windows.Forms.DockStyle.Fill
        Me.SearchCashier.Location = New System.Drawing.Point(0, 0)
        Me.SearchCashier.Margin = New System.Windows.Forms.Padding(0)
        Me.SearchCashier.Multiline = True
        Me.SearchCashier.Name = "SearchCashier"
        Me.SearchCashier.Size = New System.Drawing.Size(1116, 51)
        Me.SearchCashier.TabIndex = 19
        Me.SearchCashier.Text = "Search Cashier"
        '
        'CashierList
        '
        Me.CashierList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5})
        Me.CashierList.Dock = System.Windows.Forms.DockStyle.Fill
        Me.CashierList.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CashierList.FullRowSelect = True
        Me.CashierList.GridLines = True
        Me.CashierList.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable
        Me.CashierList.HideSelection = False
        Me.CashierList.Location = New System.Drawing.Point(0, 51)
        Me.CashierList.Margin = New System.Windows.Forms.Padding(0)
        Me.CashierList.Name = "CashierList"
        Me.CashierList.Size = New System.Drawing.Size(1116, 533)
        Me.CashierList.TabIndex = 14
        Me.CashierList.UseCompatibleStateImageBehavior = False
        Me.CashierList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "Employee ID"
        Me.ColumnHeader1.Width = 281
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "Firstname"
        Me.ColumnHeader2.Width = 228
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "Lastname"
        Me.ColumnHeader3.Width = 434
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "Position"
        Me.ColumnHeader4.Width = 444
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "Date Created"
        Me.ColumnHeader5.Width = 656
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.EditCashierBtn1)
        Me.Panel2.Controls.Add(Me.RefreshCashier1)
        Me.Panel2.Controls.Add(Me.DeleteCashierBtn1)
        Me.Panel2.Controls.Add(Me.AddCashierbtn1)
        Me.Panel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.Panel2.Location = New System.Drawing.Point(1116, 51)
        Me.Panel2.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(195, 533)
        Me.Panel2.TabIndex = 20
        '
        'EditCashierBtn1
        '
        Me.EditCashierBtn1.Active = False
        Me.EditCashierBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditCashierBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditCashierBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.EditCashierBtn1.BorderRadius = 0
        Me.EditCashierBtn1.ButtonText = "Edit Cashier"
        Me.EditCashierBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.EditCashierBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.EditCashierBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.EditCashierBtn1.Iconimage = CType(resources.GetObject("EditCashierBtn1.Iconimage"), System.Drawing.Image)
        Me.EditCashierBtn1.Iconimage_right = Nothing
        Me.EditCashierBtn1.Iconimage_right_Selected = Nothing
        Me.EditCashierBtn1.Iconimage_Selected = Nothing
        Me.EditCashierBtn1.IconMarginLeft = 0
        Me.EditCashierBtn1.IconMarginRight = 0
        Me.EditCashierBtn1.IconRightVisible = False
        Me.EditCashierBtn1.IconRightZoom = 0R
        Me.EditCashierBtn1.IconVisible = False
        Me.EditCashierBtn1.IconZoom = 90.0R
        Me.EditCashierBtn1.IsTab = False
        Me.EditCashierBtn1.Location = New System.Drawing.Point(0, 67)
        Me.EditCashierBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.EditCashierBtn1.Name = "EditCashierBtn1"
        Me.EditCashierBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.EditCashierBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.EditCashierBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.EditCashierBtn1.selected = False
        Me.EditCashierBtn1.Size = New System.Drawing.Size(198, 67)
        Me.EditCashierBtn1.TabIndex = 25
        Me.EditCashierBtn1.Text = "Edit Cashier"
        Me.EditCashierBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.EditCashierBtn1.Textcolor = System.Drawing.Color.White
        Me.EditCashierBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'RefreshCashier1
        '
        Me.RefreshCashier1.Active = False
        Me.RefreshCashier1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshCashier1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshCashier1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.RefreshCashier1.BorderRadius = 0
        Me.RefreshCashier1.ButtonText = "Refresh"
        Me.RefreshCashier1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.RefreshCashier1.DisabledColor = System.Drawing.Color.Gray
        Me.RefreshCashier1.Iconcolor = System.Drawing.Color.Transparent
        Me.RefreshCashier1.Iconimage = CType(resources.GetObject("RefreshCashier1.Iconimage"), System.Drawing.Image)
        Me.RefreshCashier1.Iconimage_right = Nothing
        Me.RefreshCashier1.Iconimage_right_Selected = Nothing
        Me.RefreshCashier1.Iconimage_Selected = Nothing
        Me.RefreshCashier1.IconMarginLeft = 0
        Me.RefreshCashier1.IconMarginRight = 0
        Me.RefreshCashier1.IconRightVisible = False
        Me.RefreshCashier1.IconRightZoom = 0R
        Me.RefreshCashier1.IconVisible = False
        Me.RefreshCashier1.IconZoom = 90.0R
        Me.RefreshCashier1.IsTab = False
        Me.RefreshCashier1.Location = New System.Drawing.Point(0, 201)
        Me.RefreshCashier1.Margin = New System.Windows.Forms.Padding(0)
        Me.RefreshCashier1.Name = "RefreshCashier1"
        Me.RefreshCashier1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.RefreshCashier1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.RefreshCashier1.OnHoverTextColor = System.Drawing.Color.White
        Me.RefreshCashier1.selected = False
        Me.RefreshCashier1.Size = New System.Drawing.Size(198, 67)
        Me.RefreshCashier1.TabIndex = 24
        Me.RefreshCashier1.Text = "Refresh"
        Me.RefreshCashier1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.RefreshCashier1.Textcolor = System.Drawing.Color.White
        Me.RefreshCashier1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'DeleteCashierBtn1
        '
        Me.DeleteCashierBtn1.Active = False
        Me.DeleteCashierBtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteCashierBtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteCashierBtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.DeleteCashierBtn1.BorderRadius = 0
        Me.DeleteCashierBtn1.ButtonText = "Delete Cashier"
        Me.DeleteCashierBtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.DeleteCashierBtn1.DisabledColor = System.Drawing.Color.Gray
        Me.DeleteCashierBtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.DeleteCashierBtn1.Iconimage = CType(resources.GetObject("DeleteCashierBtn1.Iconimage"), System.Drawing.Image)
        Me.DeleteCashierBtn1.Iconimage_right = Nothing
        Me.DeleteCashierBtn1.Iconimage_right_Selected = Nothing
        Me.DeleteCashierBtn1.Iconimage_Selected = Nothing
        Me.DeleteCashierBtn1.IconMarginLeft = 0
        Me.DeleteCashierBtn1.IconMarginRight = 0
        Me.DeleteCashierBtn1.IconRightVisible = False
        Me.DeleteCashierBtn1.IconRightZoom = 0R
        Me.DeleteCashierBtn1.IconVisible = False
        Me.DeleteCashierBtn1.IconZoom = 90.0R
        Me.DeleteCashierBtn1.IsTab = False
        Me.DeleteCashierBtn1.Location = New System.Drawing.Point(0, 134)
        Me.DeleteCashierBtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.DeleteCashierBtn1.Name = "DeleteCashierBtn1"
        Me.DeleteCashierBtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.DeleteCashierBtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.DeleteCashierBtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.DeleteCashierBtn1.selected = False
        Me.DeleteCashierBtn1.Size = New System.Drawing.Size(198, 67)
        Me.DeleteCashierBtn1.TabIndex = 23
        Me.DeleteCashierBtn1.Text = "Delete Cashier"
        Me.DeleteCashierBtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.DeleteCashierBtn1.Textcolor = System.Drawing.Color.White
        Me.DeleteCashierBtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'AddCashierbtn1
        '
        Me.AddCashierbtn1.Active = False
        Me.AddCashierbtn1.Activecolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddCashierbtn1.BackColor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddCashierbtn1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.AddCashierbtn1.BorderRadius = 0
        Me.AddCashierbtn1.ButtonText = "Add Cashier"
        Me.AddCashierbtn1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.AddCashierbtn1.DisabledColor = System.Drawing.Color.Gray
        Me.AddCashierbtn1.Iconcolor = System.Drawing.Color.Transparent
        Me.AddCashierbtn1.Iconimage = CType(resources.GetObject("AddCashierbtn1.Iconimage"), System.Drawing.Image)
        Me.AddCashierbtn1.Iconimage_right = Nothing
        Me.AddCashierbtn1.Iconimage_right_Selected = Nothing
        Me.AddCashierbtn1.Iconimage_Selected = Nothing
        Me.AddCashierbtn1.IconMarginLeft = 0
        Me.AddCashierbtn1.IconMarginRight = 0
        Me.AddCashierbtn1.IconRightVisible = False
        Me.AddCashierbtn1.IconRightZoom = 0R
        Me.AddCashierbtn1.IconVisible = False
        Me.AddCashierbtn1.IconZoom = 90.0R
        Me.AddCashierbtn1.IsTab = False
        Me.AddCashierbtn1.Location = New System.Drawing.Point(0, 0)
        Me.AddCashierbtn1.Margin = New System.Windows.Forms.Padding(0)
        Me.AddCashierbtn1.Name = "AddCashierbtn1"
        Me.AddCashierbtn1.Normalcolor = System.Drawing.Color.FromArgb(CType(CType(46, Byte), Integer), CType(CType(139, Byte), Integer), CType(CType(87, Byte), Integer))
        Me.AddCashierbtn1.OnHovercolor = System.Drawing.Color.FromArgb(CType(CType(36, Byte), Integer), CType(CType(129, Byte), Integer), CType(CType(77, Byte), Integer))
        Me.AddCashierbtn1.OnHoverTextColor = System.Drawing.Color.White
        Me.AddCashierbtn1.selected = False
        Me.AddCashierbtn1.Size = New System.Drawing.Size(198, 67)
        Me.AddCashierbtn1.TabIndex = 21
        Me.AddCashierbtn1.Text = "Add Cashier"
        Me.AddCashierbtn1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.AddCashierbtn1.Textcolor = System.Drawing.Color.White
        Me.AddCashierbtn1.TextFont = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        '
        'BunifuCards1
        '
        Me.BunifuCards1.BackColor = System.Drawing.Color.White
        Me.BunifuCards1.BorderRadius = 0
        Me.BunifuCards1.BottomSahddow = True
        Me.BunifuCards1.color = System.Drawing.Color.RoyalBlue
        Me.BunifuCards1.Controls.Add(Me.CashierAccount)
        Me.BunifuCards1.LeftSahddow = False
        Me.BunifuCards1.Location = New System.Drawing.Point(15, 16)
        Me.BunifuCards1.Name = "BunifuCards1"
        Me.BunifuCards1.RightSahddow = True
        Me.BunifuCards1.ShadowDepth = 20
        Me.BunifuCards1.Size = New System.Drawing.Size(1319, 637)
        Me.BunifuCards1.TabIndex = 15
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 676)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1351, 53)
        Me.Panel5.TabIndex = 16
        '
        'SoftwareDashboardComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.BunifuCards1)
        Me.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.Name = "SoftwareDashboardComponent"
        Me.Size = New System.Drawing.Size(1351, 729)
        Me.CashierAccount.ResumeLayout(False)
        Me.ManagerAccount.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.CashierAccounts.ResumeLayout(False)
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.BunifuCards1.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents LogList As ListView
    Friend WithEvents EmployeeId As ColumnHeader
    Friend WithEvents EmployeeFirstname As ColumnHeader
    Friend WithEvents EmployeeLastname As ColumnHeader
    Friend WithEvents EmployeePosition As ColumnHeader
    Friend WithEvents DateCreated As ColumnHeader
    Friend WithEvents SearchManager As TextBox
    Friend WithEvents CashierAccount As TabControl
    Friend WithEvents ManagerAccount As TabPage
    Friend WithEvents CashierAccounts As TabPage
    Friend WithEvents BunifuCards1 As Bunifu.Framework.UI.BunifuCards
    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents SearchCashierBtn As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel1 As Panel
    Friend WithEvents SearchCashier As TextBox
    Friend WithEvents CashierList As ListView
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents Panel2 As Panel
    Friend WithEvents RefreshCashier1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents DeleteCashierBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddCashierbtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents EditCashierBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents BunifuFlatButton1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel3 As Panel
    Friend WithEvents RefreshManagerBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents DeleteManagerBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents EditManagerBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents AddManagerBtn1 As Bunifu.Framework.UI.BunifuFlatButton
    Friend WithEvents Panel5 As Panel
End Class
