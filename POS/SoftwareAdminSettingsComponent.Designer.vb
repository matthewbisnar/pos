﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class SoftwareAdminSettingsComponent
    Inherits System.Windows.Forms.UserControl

    'UserControl overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(SoftwareAdminSettingsComponent))
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.EnableChangeIdBtn = New System.Windows.Forms.Button()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.softwareAdminIdInput = New System.Windows.Forms.TextBox()
        Me.ResetPasswordDefault = New System.Windows.Forms.Button()
        Me.ChangePassword = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SoftwareAdminConfirmPassword = New System.Windows.Forms.TextBox()
        Me.SoftwareAdminPasswordChangeInput = New System.Windows.Forms.TextBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.ExportTransactionsBtn = New System.Windows.Forms.Button()
        Me.BuckupDatabaseBtn = New System.Windows.Forms.Button()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.Panel1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Panel1
        '
        Me.Panel1.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel1.Controls.Add(Me.EnableChangeIdBtn)
        Me.Panel1.Controls.Add(Me.Label3)
        Me.Panel1.Controls.Add(Me.softwareAdminIdInput)
        Me.Panel1.Controls.Add(Me.ResetPasswordDefault)
        Me.Panel1.Controls.Add(Me.ChangePassword)
        Me.Panel1.Controls.Add(Me.Label1)
        Me.Panel1.Controls.Add(Me.Label2)
        Me.Panel1.Controls.Add(Me.SoftwareAdminConfirmPassword)
        Me.Panel1.Controls.Add(Me.SoftwareAdminPasswordChangeInput)
        Me.Panel1.Location = New System.Drawing.Point(20, 29)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(438, 625)
        Me.Panel1.TabIndex = 0
        '
        'EnableChangeIdBtn
        '
        Me.EnableChangeIdBtn.BackColor = System.Drawing.SystemColors.HotTrack
        Me.EnableChangeIdBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.EnableChangeIdBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.EnableChangeIdBtn.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.EnableChangeIdBtn.Location = New System.Drawing.Point(307, 76)
        Me.EnableChangeIdBtn.Name = "EnableChangeIdBtn"
        Me.EnableChangeIdBtn.Size = New System.Drawing.Size(108, 44)
        Me.EnableChangeIdBtn.TabIndex = 21
        Me.EnableChangeIdBtn.Text = "Enable Change ID"
        Me.EnableChangeIdBtn.UseVisualStyleBackColor = False
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(18, 32)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(75, 20)
        Me.Label3.TabIndex = 20
        Me.Label3.Text = "Admin ID"
        '
        'softwareAdminIdInput
        '
        Me.softwareAdminIdInput.Location = New System.Drawing.Point(21, 76)
        Me.softwareAdminIdInput.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.softwareAdminIdInput.Multiline = True
        Me.softwareAdminIdInput.Name = "softwareAdminIdInput"
        Me.softwareAdminIdInput.Size = New System.Drawing.Size(281, 44)
        Me.softwareAdminIdInput.TabIndex = 19
        '
        'ResetPasswordDefault
        '
        Me.ResetPasswordDefault.BackColor = System.Drawing.SystemColors.HotTrack
        Me.ResetPasswordDefault.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.ResetPasswordDefault.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ResetPasswordDefault.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ResetPasswordDefault.Location = New System.Drawing.Point(21, 466)
        Me.ResetPasswordDefault.Name = "ResetPasswordDefault"
        Me.ResetPasswordDefault.Size = New System.Drawing.Size(394, 42)
        Me.ResetPasswordDefault.TabIndex = 18
        Me.ResetPasswordDefault.Text = "Reset Default Password"
        Me.ResetPasswordDefault.UseVisualStyleBackColor = False
        '
        'ChangePassword
        '
        Me.ChangePassword.BackColor = System.Drawing.SystemColors.HotTrack
        Me.ChangePassword.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.ChangePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ChangePassword.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ChangePassword.Location = New System.Drawing.Point(21, 387)
        Me.ChangePassword.Name = "ChangePassword"
        Me.ChangePassword.Size = New System.Drawing.Size(394, 42)
        Me.ChangePassword.TabIndex = 17
        Me.ChangePassword.Text = "Change Password"
        Me.ChangePassword.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(18, 259)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(137, 20)
        Me.Label1.TabIndex = 16
        Me.Label1.Text = "Confirm Password"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(18, 144)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(138, 20)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Change Password"
        '
        'SoftwareAdminConfirmPassword
        '
        Me.SoftwareAdminConfirmPassword.Location = New System.Drawing.Point(21, 303)
        Me.SoftwareAdminConfirmPassword.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.SoftwareAdminConfirmPassword.Multiline = True
        Me.SoftwareAdminConfirmPassword.Name = "SoftwareAdminConfirmPassword"
        Me.SoftwareAdminConfirmPassword.Size = New System.Drawing.Size(395, 44)
        Me.SoftwareAdminConfirmPassword.TabIndex = 14
        '
        'SoftwareAdminPasswordChangeInput
        '
        Me.SoftwareAdminPasswordChangeInput.Location = New System.Drawing.Point(21, 188)
        Me.SoftwareAdminPasswordChangeInput.Margin = New System.Windows.Forms.Padding(2, 2, 2, 2)
        Me.SoftwareAdminPasswordChangeInput.Multiline = True
        Me.SoftwareAdminPasswordChangeInput.Name = "SoftwareAdminPasswordChangeInput"
        Me.SoftwareAdminPasswordChangeInput.Size = New System.Drawing.Size(395, 44)
        Me.SoftwareAdminPasswordChangeInput.TabIndex = 13
        '
        'Panel2
        '
        Me.Panel2.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Panel2.Controls.Add(Me.Label6)
        Me.Panel2.Controls.Add(Me.Button3)
        Me.Panel2.Controls.Add(Me.Label5)
        Me.Panel2.Controls.Add(Me.Label4)
        Me.Panel2.Controls.Add(Me.ExportTransactionsBtn)
        Me.Panel2.Controls.Add(Me.BuckupDatabaseBtn)
        Me.Panel2.Location = New System.Drawing.Point(482, 29)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(841, 625)
        Me.Panel2.TabIndex = 1
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(22, 383)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(803, 51)
        Me.Label6.TabIndex = 23
        Me.Label6.Text = resources.GetString("Label6.Text")
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.SystemColors.HotTrack
        Me.Button3.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.Button3.Location = New System.Drawing.Point(402, 458)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(349, 42)
        Me.Button3.TabIndex = 22
        Me.Button3.Text = "Change Password"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(22, 225)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(803, 51)
        Me.Label5.TabIndex = 21
        Me.Label5.Text = resources.GetString("Label5.Text")
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(22, 57)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(803, 51)
        Me.Label4.TabIndex = 20
        Me.Label4.Text = resources.GetString("Label4.Text")
        '
        'ExportTransactionsBtn
        '
        Me.ExportTransactionsBtn.BackColor = System.Drawing.SystemColors.HotTrack
        Me.ExportTransactionsBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.ExportTransactionsBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.ExportTransactionsBtn.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.ExportTransactionsBtn.Location = New System.Drawing.Point(402, 300)
        Me.ExportTransactionsBtn.Name = "ExportTransactionsBtn"
        Me.ExportTransactionsBtn.Size = New System.Drawing.Size(349, 42)
        Me.ExportTransactionsBtn.TabIndex = 19
        Me.ExportTransactionsBtn.Text = "Export Transactions"
        Me.ExportTransactionsBtn.UseVisualStyleBackColor = False
        '
        'BuckupDatabaseBtn
        '
        Me.BuckupDatabaseBtn.BackColor = System.Drawing.SystemColors.HotTrack
        Me.BuckupDatabaseBtn.FlatAppearance.BorderColor = System.Drawing.Color.Black
        Me.BuckupDatabaseBtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BuckupDatabaseBtn.ForeColor = System.Drawing.Color.WhiteSmoke
        Me.BuckupDatabaseBtn.Location = New System.Drawing.Point(402, 131)
        Me.BuckupDatabaseBtn.Name = "BuckupDatabaseBtn"
        Me.BuckupDatabaseBtn.Size = New System.Drawing.Size(349, 42)
        Me.BuckupDatabaseBtn.TabIndex = 18
        Me.BuckupDatabaseBtn.Text = "Backup Database"
        Me.BuckupDatabaseBtn.UseVisualStyleBackColor = False
        '
        'Panel5
        '
        Me.Panel5.BackColor = System.Drawing.Color.White
        Me.Panel5.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel5.Location = New System.Drawing.Point(0, 677)
        Me.Panel5.Margin = New System.Windows.Forms.Padding(0)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(1350, 53)
        Me.Panel5.TabIndex = 21
        '
        'SoftwareAdminSettingsComponent
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Lavender
        Me.Controls.Add(Me.Panel5)
        Me.Controls.Add(Me.Panel2)
        Me.Controls.Add(Me.Panel1)
        Me.Name = "SoftwareAdminSettingsComponent"
        Me.Size = New System.Drawing.Size(1350, 730)
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Panel1 As Panel
    Friend WithEvents Panel2 As Panel
    Friend WithEvents SoftwareAdminConfirmPassword As TextBox
    Friend WithEvents SoftwareAdminPasswordChangeInput As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents ResetPasswordDefault As Button
    Friend WithEvents ChangePassword As Button
    Friend WithEvents Label3 As Label
    Friend WithEvents softwareAdminIdInput As TextBox
    Friend WithEvents EnableChangeIdBtn As Button
    Friend WithEvents Label4 As Label
    Friend WithEvents ExportTransactionsBtn As Button
    Friend WithEvents BuckupDatabaseBtn As Button
    Friend WithEvents Label6 As Label
    Friend WithEvents Button3 As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents Panel5 As Panel
End Class
